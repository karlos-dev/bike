<?php

/**
 *   Mandatory Environment variables 
 * */

/*
 
echo "<pre>";

$subject = "www.milnomada.ocio.com";
$regex = '/^([w]{3}).?(.*\.[a-z]{2,10})(\/.*)?$/i';
preg_match( $regex, $subject, $matches);
print_r($matches);

die;

*/


require __DIR__ ."/protected/vendor/autoload.php";

$yii_home = getenv('YII_HOME');
error_reporting(E_ALL);


define('COMMIT','b5c648a144d5961bf7dc63f5710e1ea1995fd7db');

if( !$yii_home ) {
  echo 'No YII_HOME set in user environment.';
  exit;
}

/**
 * @version  bike 0.1
 *  
 *  Internacionalization:
 * 	http://www.yiiframework.com/wiki/243/how-to-translate-and-do-the-translations-the-easy-way/
 * 
 *   - ADD yiilite.php faster execution
 */

$yii = $yii_home . '/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);


require_once($yii);

Yii::createWebApplication($config)->run();

?>
