## DEMO Code ##

### BikeRent Ads ###

* Website for Bike business Ads
* The user can design its own ads in the website. 
* The user can have a site hosted website as a business end to show their services, available bikes, special offers or store location.
* Ads can be managed by users and by different right levels of site admin users.

* Version Beta 0.1

### Set Up ###

* Install [Composer](https://getcomposer.org/doc/01-basic-usage.md) for basic Php dependencies
* Once installed:  
  `cd path/of/project`  
  `php composer install`  

* It Requires as well some node modules for the interface (listed in package.json file)  
  `cd path/of/project`  
  `npm install`

* Configuration
  Set db dependencies in `/protected/config/main`


### Future Dev ###

* Professional Interface Design
* Production Paypal Payments
* Crash Recovery Backups
* Migration code

### Contact ###

* Developed by the community