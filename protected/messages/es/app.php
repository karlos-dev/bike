<?php

return array(
		"Features"=>"Opciones",
		"Create your Ad"=>"Crea tu anuncio",
		"Contact"=>"Contacto",
		"Flexible"=>"Flexible",
		"Size"=>"Tamaño",
		"Adverts"=>"Anuncios",
		//fake :)
		"You"=>"Puedes ",
		"Every"=>"Cada",
		"size in"=>"en",
		"Design"=>"Diseño",
		"Time"=>"Tiempo",
		"View Ads"=>"Ver Ads",
		"Plans"=>"Planes",
		"Plan Options"=>"Planes disponibles",
		"Read more"=>"Leer más",
		"Ad Products"=>"Anuncios ofrecidos",
		"Design"=>"Diseño",
		"More Information"=>"Más Información",
		"Payments"=>"Pagos",
		"Back to Top"=>"Volver arriba",
		"It's up to you"=>"Depende de ti",
		"Classic"=>"Clásicas",
		"Fixie"=>"Fixie",
		"Electric"=>"Eléctricas",
		"All"=>"Todos",
		"Cheapie"=>"Gangas",
		"Monitor"=>"Análisis",
		"Manage"=>"Gestiona",
		"Anticopyright Assets"=>"Bases Anticopyright",
		"Disclaimer"=>"Responsabilidad",
		"Name"=>"Nombre",
		"Email"=>"Email",
		"Phone"=>"Teléfono",
		"Comment"=>"Comentario",
		"Large"=>"Grande",
		"Medium"=>"Medio",
		"Regular"=>"Ancho",
		"Small"=>"Pequeño",
		"tiny"=>"Diminuto",

		"Size"=>"Tamaño",
		"Link"=>"Link",
		
		"Image"=>"Imagen",
		"Choose Ad Image"=>"Selecciona Imagen",

		"Background Image"=>"Imagen de fondo",
		"Ad web"=>"Ad web",
		"Map"=>"Mapa",
		"Analitics"=>"Estadísticas",
		"Text"=>"Texto",
		"Next"=>"Siguiente",
		"Font"=>"Letra",
		"Background"=>"Fondo",
		"Shadow"=>"Sombra",
		
		"Title"=>"Título",
		"Ad title"=>"Título del anuncio",
		
		"Website"=>"Página Web",
		"www.your-website.com"=>"www.tu-pagina-web.com",


		"Create Ad"=>"Crear Anuncio",
		"Create other Ad"=>"Crear otro Anuncio",
		"Design your ad"=>"Diseña tu anuncio",
		"Introduce your details"=>"Introduce tus datos",
		"Complete payment"=>"Finaliza el pago",
		"See how it looks"=>"Mira como ha quedado",
		"Proceed to create an awesome ad!"=>"Empieza a crear un increible anuncio!",
		'Choose your ad size and make good use of your creativity'=>'Elige el tamaño de tu anuncio y haz buen uso de tu creatividad',
		"Choose Ad size"=>"Elige el tamaño del Anuncio",

		// Features view
		"You can choose among 5 different sizes" => "Puedes elegir entre 5 tamaños diferentes",
		"has its own features. Choose the plan that better meet your needs." => "tiene sus propias características. Elige el plan que se ajuste mejor a tus necesidades.",
		"From just a title and a text, up to insert your own designed HTML code, include a google map or some other features."=>"Desde título y texto, hasta insertar tu propio código HTML, incluir un mapa google u otras opciones.",
		"the time the advert will be displayed in the "=> "el tiempo que el anuncio será mostrado en la ",
		"in the user area. No configuration nighmares"=>"en el área de usuario. Simple configuración",
		"See how your"=>"Observa como tu",
		"linking your website and "=>"apuntando tu web y ",
		"using the space"=>"usando el espacio",
		"Track the number of clicks on your advert, see how many visits has in the ad page or in the bikerent.barcelona home page"=>"Sigue el número de clicks de tus anuncios, comprueba cuantas visitas tienen en la página del anuncio o en la home de bikeremt.barcelona",
		"Watch live ad details in the"=>"Observa los detalles del anuncio en directo en ",
		//Contact
		"Flexible and simple payments with "=>"Métodos de pago flexibles y fáciles con ",

		//H3
		"Ad management"=>"Gestión de anuncios",
		"visualization statistics"=>"estadísticas",
		"business grows"=>"negocio crece",
		"bike"=>"bici",
		"business accessibility"=>"accesibilidad del negocio",
		"powerful domain"=>"dominio potente",
		"Easy"=>"Fácil",
		"free"=>"gratis",
		"choose"=>"elegir",
		"Ad"=>"Anuncio",
		"Advert"=>"Anuncio",
		"Cheap"=>"Económico",
		"Domain name"=>"Dominio estratégico",
		"Efficient"=>"Eficiente",
		"Time"=>"Tiempo",
		//H1
		"Plans for Advertisement"=>"Planes para publicidad",
		"the_name_you_want"=>"el_nombre_que_quieras",
		"the-name-you-want"=>"el-nombre-que-quieras",

		// advantages
		'<p>Increase your business </p><h3>SEO visibility</h3>'=>'<p>Mejora el </p><h3>SEO</h3><p> (</p><h3>Resultados de búsqueda</h3><p> ) de tu negocio</p>',

		//code d q 	 		
		'<h1>Adverts</h1><p class="big inline"> have plenty of customizable features.</p>'=>'<p class="big inline">Los </p><h1>Anuncios</h1><p class="big inline"> pueden ser editados con multitud de opciones.</p>',
		'Your space is yours and therefore can <a href="/terms-of-use">use it</a> as you like.'=>'Tu espacio es tuyo y por lo tanto, puedes <a href="/terms-of-use">usarlo</a> como quieras.',
		'<a href="/" target="_blank">home</a> page.</p>'=>'página <a href="/" target="_blank">home</a>.</p>',
		'The "Almost <h3>FREE</h3><p class="big inline"> ad solution", offers an interesting chance to get visits to your <i>site</i> or <i>business</i> for  <strong>1€/month</strong> <span class="small">( 0.0013€/hour )</span></p>'=>'La "Opción de anuncio casi <h3>GRATIS</h3><p class="big inline">", ofrece una interesante oportunidad de conseguir visitas en tu <i>sitio web</i> o <i>negocio</i> por <strong>1€/mes</strong> <span class="small">( 0.0013€/hora )</span></p>',
		'Every <span class="high">ad plan</span> can be <span class="high">Monthly</span> paid. <span class="_link">Flexibility</span> is a must for us.</p><p class="big"></p>'=>'Cada <span class="high">ad plan</span> puede ser abonado <span class="high">Mensualmente</span>. La <span class="_link">Flexibilidad</span> es una de nuestras bases.</p><p class="big"></p>',
		'<span class="high">Yearly</span> paid <span class="high">ad plans</span> are <h3>price reduced</h3></p>'=>'Los <span class="high">Planes de anuncios</span> pagados <span class="high">Anualmente</span> tienen <h3>precio reducido</h3></p>',
		'that a <h3>powerful domain</h3><p class="big inline"> as <span class="high">http://bikerent.barcelona</span> offers for you.</p></p>'=>'que un <h3>dominio estratégico</h3><p class="big inline"> como <span class="high">http://bikerent.barcelona</span> te ofrece.</p></p>',
		'<h3>visualization statistics</h3><p class="big inline"> section</p>'=>'<p class="big inline">la sección de </p><h3>visualización de estadísticas<h3>',
		'<p class="big inline">ads here are a efficient way to increase your <h3>bike</h3><p class="big inline"> related </p><h3>business accessibility</h3>'=>'<p class="big inline">Poner anuncios en esta web es una forma eficiente de aumentar la </p><h3>accesibilidad</h3><p class="big inline"> de tu </p><h3>negocio</h3><p class="big inline"> relacionado con la </p><h3>bici</h3>',
		// code contact
		'<p class="big inline">Place your </p><h1>bike rent bussines</h1><p class="big inline"> in this site</p>'=>'<p class="big inline">Coloca tu </p><h1>negocio de alquiler de bicicletas</h1><p class="big inline"> en esta web</p>',
		'<p class="big">Drop <strong>here</strong> a line, we will get back to you shortly.</p>'=>'<p class="big">Deja una nota <strong>aqui</strong>, te contactaremos enseguida.</p>',
		'<p class="big">Advert <strong>sizes</strong> you can use:</p>'=>'<p class="big"><strong>Tamaños</strong> de anuncio que puedes usar:</p>',
		'You select the time the advert will be displayed in the <a href="/" target="_blank">home</a> page'=>'Tu eliges el tiempo que el anuncio estarla en la página <a href="/" target="_blank">home</a> de <h3>bikerent</h3>.',
		'<p class="big inline"></p><h1>Adverts</h1><p class="big inline"> are categorized by size and feature</p>'=>'<p class="big inline">Los </p><h1>Anuncios</h1><p class="big inline"> están categorizados por tamaño y por sus características</p>',
		'<p class="big inline">Clicks and advert </p><h3>visualization statistics</h3><br><p class="big"></p>'=>'<h3>Visualización de estadísticas</h3><p class="big inline"> de clicks y visitas</p><br><p class="big"></p>',
		'<p class="big inline">Track your </p><h1>bike rent</h1><p class="big inline"> adverts, linked in a powerful domain name: <span class="_link">bikerent.barcelona</span></p><br><p class="big"></p>'=>
			'<p class="big inline">Sigue tus anuncios de </p><h1>alquiler de bicicletas</h1><p class="big inline">, posicionados en un potente nombre de dominio: <span class="_link">bikerent.barcelona</span></p><br><p class="big"></p>'

	);

?>