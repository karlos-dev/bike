<?php

/**
 * @version bike_1.0
 * 
 * Adapted contact from to meet the website requirements
 * 
 */
class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $comment;
	
	// public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, email, comment', 'required'),
			array('comment', 	'length', 'max'=>1024, 	'min'=>5, 'message'=>'Say something' ),
			array('name', 		'length', 'max'=>64, 	'min'=>2, 'message'=>'Name too long or too shor' ),
			// email has to be a valid email address
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
}

?>