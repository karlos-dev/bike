<?php
    class MyDB extends SQLite3 {

        function __construct() {
            $this->open('test.db');
        }

        function createTable(){
            $sql =  "CREATE TABLE ADDS".                    
                    "(ID INT PRIMARY KEY     NOT NULL,".
                    "NAME           TEXT    NOT NULL,".
                    "AGE            INT     NOT NULL,".
                    "IMG            INT     NOT NULL,".
                    "TXTC           CHAR(50),".
                    "TXTT           CHAR(50),".
                    "TXT1           CHAR(50),".
                    "TXT2           CHAR(50),".
                    "MAP            CHAR(50),".
                    "SALARY         REAL);";

            $ret = $this->exec($sql);

            if(!$ret){
                echo $this->lastErrorMsg();
            } else {
                echo "<pre>Table created successfully\n";
           }
           $this->close();
        }

        function insert( $table ){
            $sql = "INSERT" . " INTO". "{$table} (ID,NAME,AGE,ADDRESS,SALARY) VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";

            $ret = $this->exec($sql);

            if(!$ret){
                echo $this->lastErrorMsg();
            } else {
                echo "<pre>Table created successfully\n";
           }
           $this->close();
        }

        function findAll( $table ){
            $sql = "SELECT * " . " from {$table};";

            $ret = $this->query($sql);
            while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
                echo "ID = ". $row['ID'] . "\n";
                echo "NAME = ". $row['NAME'] ."\n";
                echo "ADDRESS = ". $row['ADDRESS'] ."\n";
                echo "SALARY =  ".$row['SALARY'] ."\n\n";
            }
            echo "Operation done successfully\n";
            $this->close();
        }
    }

    $db = new MyDB();

    if(!$db){
        echo $db->lastErrorMsg();
    } else {
        echo "Opened database successfully\n";
        $db->findAll("ADDS");

    }
?>