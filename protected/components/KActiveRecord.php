<?php

class KActiveRecord extends CActiveRecord {
    
    private static $mysql = null;

    
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

 
    protected static function getAdDbConnection() {
        if (self::$mysql !== null)
            return self::$mysql;
        else
        {
            self::$mysql = Yii::app()->mysql;
            if (self::$mysql instanceof CDbConnection)
            {
                self::$mysql->setActive(true);
                return self::$mysql;
            }
            else
                throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
        }
    }
}

?>