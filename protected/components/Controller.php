<?php
/**
 * BikeRent Base Controller
 * 
 * 
 * @author  Karlos Álvarez <info@milnomada.io>
 * @version bike_1.0
 * 
 * 	ADD mail function
 * 
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $sizeArr = array(
        "large" =>      "large",
        "medium" =>     "medium",
        "regular" =>    "regular",
        "small" =>      "small",
        "tiny"  =>      "tiny"
    );


	public static function stripAccents($str){
	  	return strtr( 
	  		utf8_decode($str), 
	  		utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 
	  		'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'
	  	);
	}

	/**
	 * @version  bike_1.1
	 * 
	 * Send Mail in this application!
	 * 
	 * @param array  $dest 	    Key value array username => email_address
	 * @param string $subject   The Email Subject
	 * @param string $mailBody 	the Email html body
	 * @param string $from      the sender email address 
	 *
	 */
	public static function sendMail( $dest, $subject, $mailBody, $from=null) {

		if(! is_array($dest) )
			return false;

		

		Yii::import('ext.phpmailer.JPhpMailer');
        $orig = Yii::app()->params['adminEmail'];
        
        $mail=new JPhpMailer;
        $mail->IsSMTP();
        // $mail->Host='smtp.zoho.com';
		$mail->Host='smtp.weird.is';
        // $mail->SMTPDebug = 4;  
        $mail->SMTPSecure = 'ssl';
        $mail->Port='465';
        $mail->SMTPAuth=true;
        //$mail->Username='admin@psychedelicspace.com';
        //$mail->Password='admin000';
        $mail->Username='info@weird.is';
        $mail->Password='Kawe00__';
        $mail->isHTML(true);

        if( $from )
        	$mail->SetFrom($from, $from);
        else
        	$mail->SetFrom('info@weird.is', Yii::app()->name );

        $mail->SetFrom('admin@psychedelicspace.com', Yii::app()->name );
        //$str = isset( $model->subject ) ? $model->subject : 'New Email from '.Yii::app()->name;
        $mail->Subject= $subject;
        $mail->AltBody='To view the message, please use an HTML compatible email viewer!';
        

        $time = date( "Y-m-d H:i:s", time() );

        if( empty($mailBody) )
        	$mailBody = "<h2>Hello from ".Yii::app()->name." </h2><a href=\"http://bikerent.barcelona\">Visit the great Ad website</a><p>Sent on: $time</p>";
        //echo "here 1"; die;
        //print_r($mail);

        $mail->MsgHTML( $mailBody );
        
        // $mail->AddBCC('admin@psychedelicspace.com', 'Info');
        $mail->AddBCC('info@weird.is', 'Weird Info');

        
        foreach ($dest as $key => $value) {
        	$mail->AddAddress( $value, $key );
        }
        // print_r($mail);
        // echo "here"; die;

        if(!$mail->send()) {
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
            Yii::log("Email was not sent","error");
            return false;
        } 

        return true;
	}
}