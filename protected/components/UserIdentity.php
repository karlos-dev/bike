<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/*
	const ERROR_NONE=0;
    const ERROR_USERNAME_INVALID=1;
    const ERROR_PASSWORD_INVALID=2;
    const ERROR_UNKNOWN_IDENTITY=100;
    */
	/**
	 * Authenticates a user.
	 * 
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 * 
	 * @version  bike 1.3.1
	 * 
	 * @abstract 
	 *  Enabled for HTTP_BASIC authentication on rest methods
	 * 
	 */
	public function authenticate()
	{

		$result = User::model()->findByAttributes( array(
			'username' => $this->username,
			'password' => UserModule::encrypting( $this->password )
		));

		if( isset($result) && $result->count() > 0 ) { 
			$this->errorCode=0;
		} else {
			$this->errorCode=2;
		}

		/*
			print_r($this); die;
			
			$this->password = md5($this->password);
		
			if( !isset( $users[$this->username] ) )

				$this->errorCode=self::ERROR_USERNAME_INVALID;

			elseif($users[$this->username]!==$this->password)

				$this->errorCode=self::ERROR_PASSWORD_INVALID;

			else
		*/

		return !$this->errorCode;
	}
}