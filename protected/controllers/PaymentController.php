<?php

/**
 * 	@author  Karlos Álvarez <info@milnomada.io>
 * 
 * 	PaymentController
 * 
 * 	Manages Paypal API Php Module
 *  https://developer.paypal.com/developer/applications/
 * 
 *  Implements the web checkout Create and Execute Payment actions
 * 
 *  Checkout flow:
 *  https://developer.paypal.com/docs/integration/web/web-checkout/
 * 
 * 	Actions:
 *  http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/CreatePaymentUsingPayPal.html
 *  http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/ExecutePayment.html 
 * 
 */

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\RedirectUrls;
use PayPal\Api\FlowConfig;
use PayPal\Api\Presentation;
use \PayPal\Api\InputFields;

class PaymentController extends Controller { 

	protected $apiContext;
	protected $sandBoxId;
	protected $sandBoxSecret;

	public function init() {

		//require __DIR__  .      '/protected/vendor/autoload.php';
		require_once Yii::app()->basePath . '/vendor/paypal/rest-api-sdk-php/sample/common.php';

		$this->sandBoxId = 'sandBoxId';
		$this->sandBoxSecret = 'sandBoxSecret';

		$sdkConfig = array(
		  	"mode" => "sandbox"
		);

		$this->apiContext = new \PayPal\Rest\ApiContext(
		    new \PayPal\Auth\OAuthTokenCredential(
		        $this->sandBoxId,     	// ClientID
		        $this->sandBoxSecret,     // ClientSecret
		        $sdkConfig
		    )
		);
	}

	protected function createWebProfile() {

		$flowConfig = new \PayPal\Api\FlowConfig();
		$flowConfig->setLandingPageType("Login");
		$flowConfig->setBankTxnPendingUrl("http://".$_SERVER['HTTP_HOST']."/ads");

		$presentation = new \PayPal\Api\Presentation();
		$presentation->setLogoImage("http://www.psychedelicspace.com/images/bikerent.png")
						->setBrandName(Yii::app()->name . " Ads Service")
						->setLocaleCode( "US" );

		$inputFields = new \PayPal\Api\InputFields();
		$inputFields->setAllowNote(true)
					->setNoShipping(1)
					->setAddressOverride(0);

		$uniqueId = substr(md5(rand()), 0, 16);
		$webProfile = new \PayPal\Api\WebProfile();	

		$webProfile->setName(Yii::app()->language." Web Space Service" . $uniqueId ) 
					->setFlowConfig($flowConfig)
					->setPresentation($presentation)
					->setInputFields($inputFields);

		$request = clone $webProfile;

		try {
			$createProfileResponse = $webProfile->create( $this->apiContext );
		} catch (\PayPal\Exception\PayPalConnectionException $ex) { 
			ResultPrinter::printError("Created Web Profile", "Web Profile", null, $request, $ex);
		    exit(1);
		}

		return $createProfileResponse;
	}

	public function actionCreatePayment() {

		//print_r($this->createWebProfile()); die;
		
		// I have a profile Id
		$profofileResponse = $this->createWebProfile();

		$payment_log = (array) json_decode($_POST['paylog']);

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");
		$payerInfo = new PayerInfo();

		$email = isset( $_POST['email'] ) ? $_POST['email'] : UserModule::user()->email;
		$payerInfo->setEmail( $email );

		$payerInfo->setFirstName($_POST['first_name'] );
		$payerInfo->setLastName($_POST['last_name'] );
		$itemList = new ItemList();
		$total=0;

		foreach ($payment_log as $item) {
			$item2 = new Item();
			$item2->setName( $item->size.' Ad')
				->setName( $item->title )
				->setDescription( "1 " . $item->period . " " . $item->size.' Ad')
			    ->setCurrency('EUR')
			    ->setQuantity(1)
			    ->setSku( $item->hash ) // Similar to `item_number` in Classic API
			    ->setPrice( $item->price );

			$total+=$item->price;
			$itemList->addItem( $item2 );
		}

		$details = new Details();
		//$details->setShipping(2.2)
		//    ->setTax(1.3)
		$details->setSubtotal($total);

		$amount = new Amount();
		$amount->setCurrency("EUR")
		    ->setTotal($total)
		    ->setDetails($details);

		// date('Y-m-d H:i:s', $_POST['timestamp']);
		$name_transaction = date("Ymd", $_POST['timestamp'])." Bikerent Barcelona Ads Purchase";

		$transaction = new Transaction();
		$transaction->setDescription($name_transaction);
		$transaction->setAmount($amount);
		$transaction->setItemList($itemList);

		// http://stackoverflow.com/a/11878014/5476782
		// $uniqueId =  date("ymd", $_POST['timestamp'])."_".substr(md5( $_POST['timestamp'] ), 0, 8);

		$transactionId =  date("dhi", $_POST['timestamp'])."".substr( UserModule::encrypting( $email ), 0, 10);

		$transaction->setInvoiceNumber( $transactionId );
		$baseUrl = "http://".$_SERVER['HTTP_HOST']."/payment/executepayment";

		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl("$baseUrl/ExecutePayment.php?success=true")
		    ->setCancelUrl("$baseUrl/ExecutePayment.php?success=false");

		$webProfileId = 'XP-75PP-HPDQ-WLAA-GDNS';
		$webProfileId = $profofileResponse->getId();
		
		$payment = new Payment();
		$payment->setIntent("sale");
		$payment->setExperienceProfileId( $webProfileId );

		$payment->setPayer($payer);
		$payment->setRedirectUrls($redirectUrls);
		$payment->setTransactions(array($transaction));
		$request = clone $payment;

		try {
		    $payment->create($this->apiContext);
		}
		catch (\PayPal\Exception\PayPalConnectionException $ex) {

		    ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
		    exit(1);

		    //REALLY HELPFUL FOR DEBUGGING
		    echo $ex->getData();
		}

		$approvalUrl = $payment->getApprovalLink();
		//ResultPrinter::printResult("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", "<a href='$approvalUrl' >$approvalUrl</a>", $request, $payment);
		
		//header("Location: $
		$u = Yii::app()->user;
		$u->setState( $transactionId, $_POST['paylog'] );
		

		echo json_encode( array( "status"=>"ok", "message"=>UserModule::t("Redirecting you to PayPal"), "redir" => $approvalUrl ) );
	}

	public function actionExecutePayment() {

		$u = Yii::app()->user;
		try {
			if( !isset( $_GET['paymentId'] ) )
				throw new Exception("User cancelled", 1);
				
			$paymentId = $_GET['paymentId'];
			$pay = new PaymentLog();

			$payment = Payment::get($paymentId, $this->apiContext);
			$transaction = $payment->getTransactions()[0];
			$pay->hash = $transaction->getInvoiceNumber();

			$payment_log_json = $u->getState( $pay->hash );
	        $arr = (array) json_decode($payment_log_json);
	        $hashArr = array();
	        
	        // Store created ads hashes to show then in case of error or success
	        if( $arr ) { foreach ($arr as $h) { $hashArr[]=$h->hash; } }
	        $u->setState( "adids", json_encode($hashArr) );

		} catch( Exception $e) {
			$payment=null;
		}

		if ( isset($_GET['success']) && $_GET['success'] == 'true') {
			try {

				if( !$payment ) {
					$payment = Payment::get($paymentId, $this->apiContext);
					$transaction = $payment->getTransactions()[0];
				}
				
				$execution = new PaymentExecution();
				$execution->setPayerId($_GET['PayerID']);

		    	try {

		    		$result = $payment->execute($execution, $this->apiContext);

		    		//print_r($result);

		            $elem = (array) $payment;
		            $pay->attributes = $elem;
		            
		            $amount = $transaction->getAmount();
		            $pay->hash = substr( $transaction->getInvoiceNumber(), 6 );
		            
		            $pay->paypalid = $payment->getId();
		            $pay->state = $payment->getState();
		            $pay->amount = $amount->getTotal();

		            // $pay->cart = $payment->getState();
		            $person = $payment->getPayer();
		            $pay->payment_method = $person->getPaymentMethod();
		            $pay->usr_status = $person->getStatus();
		            $personInfo = $person->getPayerInfo();
		            $pay->usr_id = $personInfo->getPayerId();
		            $pay->usr_first_name = $personInfo->getFirstName();
		            $pay->usr_last_name  = $personInfo->getLastName();
		            $pay->validate();

		            if( $pay->hasErrors() ) {
		            	print_r( $pay->errors );
		            	die;
		            }

		            Yii::log("Executed Payment $pay->paypalid", 'info');
		            // awe  --> awesome
		            // sok  --> save operation ok
		            // serr --> db operation save error
		            
		            if( $pay->save() ) {
		            	$u->setState( "payed", $payment->getId() );
		            	header("Location: http://".$_SERVER['HTTP_HOST']."/ads?p=awe&pr=".$transaction->getInvoiceNumber() );
		            } else {
		            	header("Location: http://".$_SERVER['HTTP_HOST']."/ads?p=awe&pr=serr" );
		            }

		        } catch (Exception $ex) {
		        	header("Location: http://".$_SERVER['HTTP_HOST']."/ads?p=err&pr=eep" );
		        }
		        
		    } catch (Exception $ex) {
		    	header("Location: http://".$_SERVER['HTTP_HOST']."/ads?p=err&pr=egp" );
		    }

		    // ResultPrinter::printResult("Get Payment", "Payment", $payment->getId(), null, $payment);
		    // return $payment;

		} else {
		    // uc -> user cancelled
		    header("Location: http://".$_SERVER['HTTP_HOST']."/ads?p=ok&pr=uc" );
		}
	}
}

?>