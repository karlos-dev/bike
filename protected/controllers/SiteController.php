<?php

// error reporting (this is a demo, after all!)
// ini_set('display_errors',1);
// error_reporting(E_ALL);

class SiteController extends Controller {

    public $layout='//layouts/adlayout';

    public $description = 'Bike rent services and touristic bike services in the pleasant city of Barcelona. Place your bike business add in this website.';
    

    /**
	 * Declares class-based actions.
	 */
    public function init(){

        parent::init();
    }

    public function filters()
    {
        return CMap::mergeArray( parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
        ));
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',  // allow all users
                'actions'=>array('index','map', 'ads', 'sads', 'contact','checkun','checkem', 'checkcard', 'checkav', 'soon', 'features', 'plans', 'use', 'error', 'login'),
                'users'=>array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','create','update','view', 'manager', 'logout'),
                'users'=>UserModule::getAdmins(),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
        // $start = microtime(true);
        
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {

        if( Yii::app()->user->isGuest ) {

            $c = new CDbCriteria();
            //$c->compare('expiredate','>'.date('Y-m-d H:i:s', time() ) );
            $set = Ad::model()->findAll($c);
            $json = [];

            foreach ($set as $ad) {
                $json[] = $ad->attributes;
            }

            $this->layout='//layouts/adlayout';

            $this->render('index', array( 'set' => $json ));
        }
		else {
            $this->layout='//layouts/main';
            $this->redirect("/user/profile");
        }
	}

    /**
     * This is the 'map' action 
     * Render Google map 
     */
    public function actionMap() {

        $this->layout='//layouts/adlayout';
        $this->render('map');
    }

    private function getAdsFromSession(){
        $total = isset($_SESSION['_ads_total']) ? $_SESSION['_ads_total'] : 0;
        $result = array();
        for ($i=0; $i < $total ; $i++) { 
            $ad = $_SESSION['_ad_'.($i+1)]; 
            $result[] = $ad;
        }
        return $result;
    }

    /** 
     *  actionSads
     * 
     *  REST: ads.js 
     * 
     * @param integer $i the ad position in session cache
     * 
     */
    public function actionSads($i=0) { 
        if( !empty( $_POST ) && isset( $_POST['str'] ) ) { 
            $found=false;

            $_SESSION['_ads_total']= isset($_SESSION['_ads_total']) ? $_SESSION['_ads_total'] : 0;

            if( $_SESSION['_ads_total'] > 0)
                for ($pos=1; $pos < $_SESSION['_ads_total'] ; $pos++) { 
                    if( $_SESSION["_ad_$pos"] == $_POST['str'] ) {
                        $found=true;
                    }
                }

            if( !$found ) {
                $_SESSION['_ads_total'] += 1;
                $_SESSION["_ad_{$_SESSION['_ads_total']}"] = $_POST['str'];
            }
            
            echo $_SESSION['_ads_total'];

        } else {
            $_SESSION['_ads_total']= isset($_SESSION['_ads_total']) ? $_SESSION['_ads_total'] : 0;

            if( !empty( $_POST ) && isset( $_POST['delete'] ) ) { 
                $n = ($_POST['delete'] == 0) ? 1 : $_POST['delete']-1;
                unset( $_SESSION["_ad_{$n}"] );
                $_SESSION['_ads_total']= ($_SESSION['_ads_total']>0) ? $_SESSION['_ads_total']-1 : 0;
            } elseif( $i==0 ) {
                echo $_SESSION['_ads_total'];
            } elseif( isset($_SESSION["_ad_$i"]) ) {
                echo $_SESSION["_ad_$i"];
            } else {
                echo "{}";
            }
        }
    }

    /** 
     *  actionAds
     * 
     *  Receives $_POST from ads.js  
     *  If not present renders the view: views/site/ads.php
     *  
     *  @param string|null $opt Unused
     *  @echoes json
     */
    public function actionAds( $opt=null ) {

        $targetFolder = '/data'; // Relative to the root
        
        if ( isset( $_POST['timestamp']) ) 
            $verifyToken = md5( Yii::app()->user->getStateKeyPrefix() . $_POST['timestamp']);

        // Upload image files
        if (! empty($_FILES) && $_POST['token'] == $verifyToken) {
            
            $tempFile = $_FILES['file-0']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            $targetFile = rtrim($targetPath,'/') . '/' . $_FILES['file-0']['name'];

            // echo $targetFile; die;
            // Validate the file type
            
            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($_FILES['file-0']['name']);
            
            if (in_array($fileParts['extension'],$fileTypes)) {
                move_uploaded_file($tempFile, $targetFile);
                echo "1{$_FILES['file-0']['name']}";
            } else {
                echo 'Invalid file type.';
            }
        } else if( !empty( $_POST ) ) {
            
            $user = new User();
            $newUser=true;
            $savedAll=false;

            if( Yii::app()->user->isGuest ) { 

                $profile= new Profile;
                $_userModule = Yii::app()->getModule('user');

                $profile->attributes = $_POST;
                $user->attributes = $_POST;
                $user->password = Yii::app()->getModule('user')->encrypting($_POST['password']) ;
                $user->superuser=0;
                $user->status=(( $_userModule->activeAfterRegister) ? User::STATUS_ACTIVE : User::STATUS_NOACTIVE);

                $nameArr = explode(" ", $_POST['name'] );
                $surname = isset( $nameArr[1] ) ? implode(" ", array_slice($nameArr, 1) ) : "";
                $user->username = strtolower( str_replace(" ", ".", self::stripAccents( trim($nameArr[0]) )) );

                $profile['first_name']= trim($nameArr[0]);
                $profile['last_name']=  trim( $surname );

            } else {
                $user=Yii::app()->user;
                $profile=UserModule::user()->profile;
                $newUser=false;
            }

            //$arr = isset($_POST['wax']) ? $_POST['wax'] : array();
            
            if( !isset( $_POST['update'] )) {
                $sessionAds = $this->getAdsFromSession();
                $adArr = array();
                foreach ($sessionAds as $sad) {
                    $obj = json_decode($sad, false);
                    if($obj)
                        $adArr[]=$obj;
                }

            } else {
                $adArr[]= json_decode( $_POST['str'], false);
            }

            $defaultTime = 60 * 15;
            $createdAds = array();
            foreach ($adArr as $d) {
                $data=(array) $d;
                $ad = new Ad();
                // unset $data attr;
                $ad->attributes = $data;
                //print_r($data); die;

                // Post names not meeting ad attributes:
                $ad->class =    $data['size'];
                $ad->textad =   $data['title'];
                $ad->createdate = date('Y-m-d H:i:s', $_POST['timestamp']);
                $ad->expiredate = date('Y-m-d H:i:s', $_POST['timestamp'] + $defaultTime);
                $ad->active='N';
                $ad->priority = '3';
                $ad->groupid = 0;
                $ad->uri = $ad->getUrl();


                // @note --> Then in ad::validate there are other overrides
                
                $createdAds[]=$ad;
            }
            
            // print_r($adArr); die;

            if( Yii::app()->user->isGuest && $user->validate() && $profile->validate() ) { 

                $user->activkey=UserModule::encrypting(microtime().$user->password);
                // encrypt password
                $user->password= $_userModule->encrypting( $user->password );

                if ( $user->save() ) {
                    $userId = $user->id;

                    $profile->user_id=$user->id;
                    foreach ($createdAds as $data) {
                        $data->uid=$user->id;
                    }

                    if( $profile->save() ) {
                        $savedAll = true;
                    }

                    Yii::log("New client registered $user->id", "audit");

                } else {
                    Yii::log("Trying to save user: $user->email", "error");

                    echo json_encode( array( "status"=>"error", "errors"=>$user->errors ) );
                    Yii::app()->end();
                }

                /*  FIX LOGIN automatically the user

                    $identity=new UserIdentity($model->username,$soucePassword);
                    $identity->authenticate();
                    Yii::app()->user->login($identity,0);
                    $this->redirect(Yii::app()->controller->module->returnUrl);
                 */
                
            } else {

                $user = UserModule::user( Yii::app()->user->id );
                $userId = $user->id;
                
                if( ! Yii::app()->user->isGuest && !isset($_POST['update']) ) {
                    
                    foreach ($createdAds as $data) {
                        $data->uid = $userId;
                    }
                    $savedAll = true;
                    Yii::log("Registered user {$user->profile->first_name} adding ads", "audit");

                    } 

                else {

                    if( !is_a($user, 'RWebUser') ) {
                        if( $user->hasErrors() ) {
                            echo json_encode( array( "status"=>"error", "errors"=>$user->errors ) );
                            Yii::app()->end();
                        }
                        else if( $profile->hasErrors() ) {
                            echo json_encode( array( "status"=>"error", "errors"=>$profile->errors ) );
                            Yii::app()->end();
                        }
                    }
                }

            }
            // print_r($createdAds); die;
            if( $savedAll ) { 
                $b = SpaceController::createBucket( $user );

                foreach ($createdAds as $data) {

                    $subject = $data->url;
                    $regex = '/^([w]{3}).?(.*\.[a-z]{2,10})(\/.*)?$/i';
                    preg_match( $regex, $subject, $matches);

                    if( isset( $matches[2] )) {
                        $domain = Domain::model()->findByAttributes( array( 'name' => $matches[2] ) );
                        if( ! $domain ) {
                            $domain = new Domain();
                            $domain->uri = $data->url;
                            $domain->uid = $userId;
                            $domain->name = $matches[2];
                            $domain->active = 'Y';
                            $domain->createdate = date('Y-m-d H:i:s', $_POST['timestamp']);
                            if( !$domain->save() ) 
                                throw new Exception("Error Saving Domain", 1);

                            // Error or Yii bug
                            $domain = Domain::model()->findByAttributes( array( 'name' => $matches[2] ) );
                        }
                    }

                    // echo "one ";
                    //print_r($matches);
                    $data->setDomainid( $domain->domainid );

                    if ( $data->save(true) ) { 
                        if( SpaceController::ad2Bucket( $b, $data ) ) {
                            Yii::log("New ad created $ad->adid", "audit");

                        } else {
                            // logged in Ad2Bucket
                            // Retry? Notify?
                        }
                    }
                    // TESTING
                    else if( $ad->hasErrors() ) {
                        echo json_encode( array( "status"=>"error", "errors"=>$ad->errors ) );
                        Yii::app()->end();
                    }
                }

                // TODO
                // Send ad review Email with $createdAds;
            } else if( isset($_POST['update']) ) {
                $newUser=false;
                $b = (new SpaceController(0))->createBucket( $user, md5( $user->email ) );

                foreach ($createdAds as $data) { 
                    if ( $data->save(true) ) { 
                        if( SpaceController::ad2Bucket( $b, $data ) ) {
                            Yii::log("Updated ad $ad->adid", "audit");
                            // Paypal redirect
                        } else {
                            echo json_encode( array( 
                                "status"=>"error", 
                                "message" => UserModule::t("Error saving image data")
                            ) );
                            Yii::app()->end();
                        }
                    }
                }

            }

            // TESTING
            $newUser=false;

            if ( $newUser && $_userModule->sendActivationMail
                
                /*self::sendMail( 
                    array( $profile->first_name => $user->email ), 
                    "Subject", 
                    "Message"
                    //UserModule::t( "<h5>Hello</h5><p>Register in <b>Bikerent</b> Barcelona</p><p>Please, <br>activate you account go to {activation_url}", array('{activation_url}'=>$activation_url) ) 
                )*/ 

            ) {
                $activation_url = $this->createAbsoluteUrl('/user/activation/activation',array("activkey" => $user->activkey, "email" => $user->email));

                try {
                    self::sendMail( array( $profile['first_name'] => $user->email ), "Info", "");
                } catch (Exception $e) {
                    $e->printStackTrace();
                }
                
                echo json_encode( array( 
                    "status"=>"ok", 
                    "message" => UserModule::t("Thank you for your registration. Please check your email."),
                    "redir" => $ad->getUrl()
                ) );

                //Yii::app()->end();

                // Send mail deactivated DEBUG
                // $activation_url = $this->createAbsoluteUrl('/user/activation/activation',array("activkey" => $model->activkey, "email" => $model->email));
                // UserModule::sendMail($model->email,UserModule::t("You registered from {site_name}", array('{site_name}'=>Yii::app()->name)),UserModule::t("Please activate you account go to {activation_url}",array('{activation_url}'=>$activation_url)));
            } 

            $_POST['first_name']= $profile['first_name'];
            $_POST['last_name'] = $profile['last_name'];
            $_POST['uid'] = $userId;

            // redirects the application to controller
            Yii::app()->runController('payment/createpayment');
        } else {
            // Back from PAYPAL transaction
            if( isset( $_GET['p'] ) && isset( $_GET['pr'] ) ) {
                if( $_GET['p'] == 'err') { 

                    $this->render('ads', array( 'status' => 'err' ));
                    Yii::log("Paypal Php Api Error", "error");

                    // TODO: Nofify Paypal status error

                } elseif( !in_array($_GET['pr'], [ 'serr', 'uc' ]  ) ) {

                    $adsJson = Yii::app()->user->getState( "adids" );
                    $hashArr = json_decode($adsJson);

                    // Exit if no ads
                    if( !$hashArr ) {
                        header("Location: http://".$_SERVER['HTTP_HOST']."/ads" );
                        Yii::app()->end();
                    }

                    $criteria = new CDbCriteria;
                    foreach ($hashArr as $val) {
                        $criteria->compare('hash', $val, false, 'OR');
                    }
                    // $criteria->compare('hash', '670981310', false, 'OR');
                    $res = Ad::model()->findAll($criteria);

                    //$uniqueId =  date("ymd", $_POST['timestamp'])."_".substr(md5( $_POST['timestamp'] ), 0, 8);
                    $uniqueId = $_GET['pr'];
                    $paylog = json_decode( Yii::app()->user->getState( $uniqueId ) );

                    // ------------------> Exit
                    if( !$paylog ) {

                        if( $res->count() > 0 ) {
                            
                            // TODO: Nofify system status error
                            
                            $this->render('ads', array( 'status' => 'err' ));
                            Yii::log("Unknown Error", "error");

                        } else { 
                            $this->render('ads');
                        }

                        Yii::app()->end();
                    }

                    // ------------------> Update Ads Expire date
                    $updated=array();
                    $myres = array();

                    foreach ($res as $obj) {
                        foreach ($paylog as $log) {
                            if( $log->hash == $obj->hash && !in_array( $obj->hash, $updated ) ) {

                                $days = ($log->period == 'year') ? 365 : 30;
                                $expireDate = time() + 3600 * 24 * $days;

                                $saveAd = Ad::model()->findByAttributes(array( 'hash' => $obj->hash ));
                                if( $saveAd ) {
                                    $updated[]=$obj->hash;
                                    // Fixed in c0c99e7dc54b216fdd5ae6ae0c8488038a7d4976
                                    $saveAd->activateAd( date('Y-m-d H:i:s', $expireDate ) );
                                    $myres[]=$saveAd;
                                }
                            }
                        }
                    }

                    $this->render('thanks', array( 'res' => $myres ) );

                } elseif( $_GET['pr'] == 'serr' ) {

                    // TODO: Nofify system status error

                    $this->render('ads', array( 'status' => 'err' ));
                    Yii::log("Unknown Error", "error");

                } elseif( $_GET['pr'] == 'uc' ) {

                    $this->render('ads', array( 'status' => 'cancelled' ));
                }
            } else
                $this->render('ads');
        }
    }

    protected function afterAction( $action ) {

        if( YII_DEBUG && false)
            $this->renderPartial('audit.views.request.__footer');
    }

    public function actionCheckUn() { 
        $result=false;
        $file = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'messages' . DIRECTORY_SEPARATOR . 'badwords' . DIRECTORY_SEPARATOR . 'badwords.txt';
        $badWordsFile = file($file,  FILE_IGNORE_NEW_LINES);

        if( isset( $_POST['name'] ) && !empty($_POST['name']) ) { 
            $result=true;

            $name = strtolower(trim($_POST['name']));
            if( in_array( $name , $badWordsFile ) ) {
                $result=false;
            }
        }

        echo json_encode($result);
    }

    public function actionCheckEm() { 
        $result=false;

        if( isset( $_POST['email'] ) && !empty($_POST['email']) ) { 
            $result=true;

            $email = strtolower(trim($_POST['email']));
            $usr = User::model()->findByAttributes(array('email'=>$email));
            if( isset($usr) && !empty($usr) )
                $result=false;
        }

        echo json_encode($result);
    }

    public function actionCheckcard() { 

        $result=false;

        if( isset( $_POST['cardnumber'] ) && !empty($_POST['cardnumber']) ) { 
            // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
            $number=preg_replace('/\D/', '', $_POST['cardnumber'] );

            // Set the string length and parity
            $number_length=strlen($number);
            $parity=$number_length % 2;

            // Loop through each digit and do the maths
            $total=0;
            for ($i=0; $i<$number_length; $i++) {
                $digit=$number[$i];
                // Multiply alternate digits by two
                if ($i % 2 == $parity) {
                    $digit*=2;
                    // If the sum is two digits, add them together (in effect)
                    if ($digit > 9) {
                        $digit-=9;
                    }
                }
                // Total up the digits
                $total+=$digit;
            }
            // If the total mod 10 equals 0, the number is valid
            $result = ($total % 10 == 0);
        }

        echo json_encode($result);
    }

    public function actionCheckAv() {
        // reserved names
        $more = array(
            "bike rental barcelona",
            "bikerental barcelona",
            "bikerent barcelona",
            "barcelona",
            "barcelona bikerent",
            "barcelona bikerental",
            "barcelona bike",
            "barcelona bike rent",
            "barcelona bike rental",
            "bicis",
            "bikes",
            "bikes rent",
            "bikes rent barcelona",
            "bikes rental",
            "bikes rental barcelona",
            "bike",
            "bike hire",
            "bike hire barcelona",
            "bike rent",
            "bike rent barcelona",
            "bike barcelona",
            "bike barcelona rent",
            "bike barcelona hire",
            "bike barcelona rental",
        );

        //forbiddeb parts of the message
        $forbidden = array(
            "sants",
            "gracia",
            "born",
            "barceloneta",
            "eixample",
            "poblenou",
            "poblesec",
            "xxx",
            "xx",
        );

        $file = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'messages' . DIRECTORY_SEPARATOR . 'badwords' . DIRECTORY_SEPARATOR . 'badwords.txt';
        $badWordsFile = file($file,  FILE_IGNORE_NEW_LINES);

        if( isset( $_POST['title'] ) && !empty($_POST['title']) ) { 
            $title = strtolower(trim($_POST['title']));
            if( in_array( $title , $badWordsFile ) ) {
                echo json_encode(false);
                return;
            }

            // Not valid terms
            foreach ($forbidden as $forb) {
                if( strpos($title, $forb ) !== false ) {
                    echo json_encode(false);
                    return;
                }
            }
            
            // Check reserved names
            if( in_array($title, $more) ) {
                echo json_encode(false);
                return;
            }

            if( !isset($_GET['edit']) )
                // Check unique title
                $f = Ad::model()->findByAttributes(array('title'=>trim($_POST['title'])));
            else {
                $c = new CDbCriteria();
                $c->compare('adid', '<>'.$_GET['edit']);
                $c->compare('title', $_POST['title']);
                $f = Ad::model()->find($c);
            }

            if( $f ) {
                echo json_encode(false);
            } else
                echo json_encode(true);
            
            return;
        } 

        

        echo json_encode(false);
    }

    public function actionPlans() {
        
        $this->render('plans');
    }

    /**
     * This is the 'soon' action that is invoked
     * when a user clicks in any add shown in index view
     * 
     */
    public function actionSoon() {
     
        $this->render('index');
        
    }

    /**
     * This is the 'soon' action that is invoked
     * when a user clicks in any add shown in index view
     * 
     */
    public function actionManager() {

        $this->pageTitle="Ads Manager";
        $this->layout = '//layouts/main';
        $this->render('manager');
    }

    /**
     * This is the 'soon' action that is invoked
     * when a user clicks in any add shown in index view
     * 
     */
    public function actionFeatures($elem=null) {
     
        $this->render('features', array( "elem" => $elem ));
        
        //$start = round(microtime(true) * 1000);        
        //$controller = Yii::app()->controller->id;
        //$action = isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : 'init';
    }

    /**
     * This is the 'soon' action that is invoked
     * when a user clicks in any add shown in index view
     * 
     */
    public function actionUse() {
     
        $this->render('use', array() );

    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
        //echo "SITE is here!"; die;
        
		if( $error=Yii::app()->errorHandler->error ) {
			if( Yii::app()->request->isAjaxRequest )
				echo $error['message'];
			else {
                echo "<pre>";
                print_r( $error );
                die;
            }
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {

        if( !empty( $_POST && Yii::app()->params['mail-enabled'] ) ) {
            $name = Yii::app()->name;
            //print_r($_POST); die;
            $model=new ContactForm;
            $model->attributes=$_POST;
            if( $model->validate()) {
                Yii::import('ext.phpmailer.JPhpMailer');
                $orig = Yii::app()->params['adminEmail'];
                
                $mail=new JPhpMailer;
                $mail->IsSMTP();
                $mail->Host='smtp.zoho.com';
                // $mail->SMTPDebug = 4;  
                $mail->SMTPSecure = 'ssl';
                $mail->Port='465';
                $mail->SMTPAuth=true;
                $mail->Username='admin@psychedelicspace.com';
                $mail->Password='admin000';
                $mail->isHTML(true);
                //$mail->SetFrom('admin@psychedelicspace.com','tattoo barcelona');
                $mail->SetFrom('admin@psychedelicspace.com', Yii::app()->name);
                $str = isset( $model->subject ) ? $model->subject : 'New contact from tattoo barcelona';
                $mail->Subject= $str;
                $mail->AltBody='To view the message, please use an HTML compatible email viewer!';
                $time = date( "Y-m-d H:i:s", time() );
                $mail->MsgHTML("<h3>Contacto recibido</h3><h2>$name</h2><h1>{$model->name}</h1><h3>{$model->email}</h3><p>{$model->comment}</p><p>Recibido:<br><b>{$time}</b></p><p>Contactale pronto!</p><h3>Abrazo</h3>");
                $mail->AddBCC('admin@psychedelicspace.com', 'Info');
                $mail->AddBCC('milnomada@gmail.com', 'Karlos');

                if(!$mail->send()) {
                    //echo 'Message could not be sent.';
                    //echo 'Mailer Error: ' . $mail->ErrorInfo;
                    Yii::log("Email was not sent","error");
                } 

                $mail=new JPhpMailer;
                $mail->IsSMTP();
                $mail->Host='smtp.zoho.com';
                // $mail->SMTPDebug = 4;  
                $mail->SMTPSecure = 'ssl';
                $mail->Port='465';
                $mail->SMTPAuth=true;
                $mail->Username='admin@psychedelicspace.com';
                $mail->Password='admin000';
                $mail->isHTML(true);
                //$mail->SetFrom('admin@psychedelicspace.com','tattoo barcelona');
                $mail->SetFrom('admin@psychedelicspace.com', Yii::app()->name);
                $str = 'Contact from ' . Yii::app()->name;
                $mail->Subject= $str;
                $mail->AltBody='To view the message, please use an HTML compatible email viewer!';
                $time = date( "F j, Y, H:i", time() );
                $mail->MsgHTML("<h3>Hello!</h3><h4>We've received your message!</h4><a href='http://bikerent.barcelona'>$name</a><hr /><h3>Message from:</h3><h1>{$model->name}</h1><h3>{$model->email}</h3><p><b>Message:</b><br>{$model->comment}</p><p>Recibido:<br><b>{$time}</b></p><hr /><br><p>We'll contact you <b>shortly</b></p><h3>Best regards,</h3>");
                
                $mail->AddAddress( $model->email, $model->name );
                $mail->AddAddress('node1@psychedelicspace.com', 'Anne');

                if(!$mail->send()) {
                    // echo 'Message could not be sent.';
                    // echo 'Mailer Error: ' . $mail->ErrorInfo;
                    Yii::log("Email was not sent", "error");
                } else {
                    // echo 'Message has been sent';
                    Yii::log("A user was succesfully notified", "info");
                }

                $res=array('status' => 'sent');
                echo json_encode($res);
                die;

            } elseif( $model->hasErrors() ) {

                $err=$model->getErrors();
                $err['status']='invalid';
                echo json_encode($err);
                die;

            } else { 
                $err=array('status' => 'error');
                echo json_encode($err);
                die;
            }
        }

		$this->render('contact', array() );
        //$start = round(microtime(true) * 1000);        
        //$controller = Yii::app()->controller->id;
        //$action = isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : 'init';
	}
    
	/**
	 * Displays the login page
	 */
	public function actionLogin() {
        $this->redirect("/user/login");
	}
    
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
        
        /*
        
        $start = round(microtime(true) * 1000);        
        $controller = Yii::app()->controller->id;
        $action = isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : 'init';
        //$stringStart =  date("M d Y H:i:s", time());
        //Yii::log("**********> EXECUTION [{$controller}/{$action}] [{$stringStart}] **** Ms: {$start}");
        */
		$this->redirect('index');
        
	}
    
    
    public function actionResetPassword( $id ){
        $form  = new ResetForm();
        /**
         * @abstract When the website would be placed within another differenct domain or server
         * the communication cannot use models within oauth module which is the REST API itself.
         * 
         * We should modify all oauth2 methods to run using different server
         */
        $c = new EMongoCriteria;        
        $c->addCondition( 'code', $id );
        $resetCode = PassCode::model()->findOne($c);

        if( !$resetCode )
            $mode = 'alreadyused';
        
        else {    
            if( time() < $resetCode->expires )
                $mode = 'active';
            else
                $mode = 'expired';
            
            $form->email = $resetCode->user;
        }
        
        if( isset( $_POST['ResetForm'] ) ){
            
            $c = new EMongoCriteria;
            $c->addCondition( 'email', $_POST['ResetForm']['email'] );
            $user = Oauth2User::model()->findOne($c);

            if( !$user ) 
                $mode = 'error';
            
            $user->password = openssl_digest($_POST['ResetForm']['password'], 'sha512');
            if ( $user->update(array( 'password', true )) ){
                
                $c = new EMongoCriteria;
                $c->addCondition( 'user', $_POST['ResetForm']['email'] );

                $resetCode = PassCode::model()->deleteAll($c);
                $mode = 'updated';
            }
        }
        
        $this->render('reset', array(
            'mode'=>$mode,
            'model'=>$form, 
            'resetCode'=>$id,
        ));
    }
}