<?php

/**
 * 	@author  Karlos Álvarez <info@milnomada.io>
 * 
 *  @version  1.3
 * 
 * 	SpaceController
 * 
 * 	Manages RackSpace Cloud Storage API
 *  https://developer.rackspace.com/docs/cloud-servers/quickstart/
 * 
 * 	Github
 *  https://github.com/milnomada/php-opencloud
 * 
 * 	Docs:
 *  http://docs.php-opencloud.com/en/latest/services/object-store/objects.html#list-objects-in-a-container
 * 
 */

use OpenCloud\Rackspace;
use OpenCloud\ObjectStore\Resource\DataObject;

class SpaceController extends Controller { 

	private static $client;
	private static $region='IAD';
	private static $username='milnomada';
	private static $key='7170f6f195c74121a7bcfb62f167dcad';

	public function init(){
		if( !self::$client ) {
			self::$client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
			    'username' => self::$username,
			    'apiKey'   => self::$key
			));
		}
	}

	public static function getOS() {
		if( !self::$client )
			self::$client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
			    'username' => self::$username,
			    'apiKey'   => self::$key
			));

		$oss=self::$client->objectStoreService(null, self::$region);

		$account = $oss->getAccount();
		$account->setTempUrlSecret('697f424cc4a9657115d2144aa483eb169b998a7b');

		return $oss;
	}

	// test stuff
	public function actionIndex() {
		try {
			$oss = self::getOS();
			$name = '0f9f306f8e694c3843bc8602c6dd47e2';
			$container = $oss->getContainer($name);
			if( !$container )
				$container = $oss->createContainer($name);

			echo "<pre>";
			$container->enableCdn();
			if( $container->isCdnEnabled() ) {
				$cdn=$container->getCdn();
				echo "CDB is enabled\n";
				echo $cdn->getCdnUri()."\n";
			}

			$saveAd = Ad::model()->findByAttributes(array( 'uri' => 'my-images-are-uploaded-to-rackspace-automatically' ));
			$files = $container->objectList();
			$head="ad" . ltrim( $saveAd->adid, '0'). "";

			$cadena = $saveAd->htmlad;
			foreach ($files as $file) {
			    if( substr( $file->getName(), 0 ,5) == $head ) {

			    	$size = $file->getContentLength();
				    $type = $file->getContentType();

				    echo $cdn->getCdnUri()."/".$file->getName() . " $size $type\n";
			    }
			}
			echo $bgOn;
			echo count($files). " objects\n";

		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * Create Bucket
	 * @param  User 									  $user 	The user model
	 * @return OpenCloud\ObjectStore\Resource\DataObject  			The created container
	 */
	public static function createBucket( $user, $name=null ) {
		try {
			if( !$name || empty($name) ) {
				$oss = self::getOS();
				$name = Yii::app()->getModule('user')->encrypting($user->email);
			} else {
				// Beacuse we do not load the application in ajax stuff
				$client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
				    'username' => self::$username,
				    'apiKey'   => self::$key
				));
				$oss=$client->objectStoreService(null, self::$region);
			}

			$container = $oss->getContainer($name);

			return $container;

		} catch (Exception $e) {
			echo $e->getMessage();
			die;
			$container = $oss->createContainer($name);
			return $container;
		}
	}

	/**
	 * file2Bucket
	 * Add a single file to a bucket/container
	 * 
	 * @param  OpenCloud\ObjectStore\Resource\Container $b The container object
	 * @param  String 	$name 	filename in bucket
	 * @param  String 	$path 	path to file in absolute SO relative path
	 * @param  Array 	$meta  	Metadata about the file
	 */
	public static function file2Bucket( $b, $name, $path, $meta=null  ) {
		try {

			$oss = self::getOS();
			// $name = Yii::app()->controller->module->encrypting( $user->email );
			// $b = $oss->getContainer($name);

			if( $meta ) {
				$metadata = $meta;
			} else {
				$metadata = array( "author" => "bike", "tag" => "test");
			}

			$b->uploadObject($name, fopen($path, 'r+'), DataObject::stockHeaders($metadata) );

		} catch (Exception $e) {
			echo $e->getMessage();
			echo $e->printStackTrace();
		}
	}

	/**
	 * deleteAdFromBucket
	 * Deletes files belonging to adid
	 * 
	 * @param  User 	$user  	The user deleting
	 * @param  Integer 	$adid 	The deleted ad id
	 * @return boolean  If the deletion was successful
	 */
	public static function deleteAdFromBucket( $user, $adid ) {
		try {

			$oss = self::getOS();
			$bucketName = Yii::app()->getModule('user')->encrypting($user->email);

			$container = $oss->getContainer($bucketName);
			$files = $container->objectList();
			
			// So delete json file as well			
            $head="ad" . ltrim( $adid, '0');
			foreach ($files as $file) {
				// Less string though
				if( substr( $file->getName(), 0 ,5) == $head ) { 
					$container->deleteObject( $file->getName() );
				}
			}

			return true;

		} catch (Exception $e) {

			Yii::log("Fail writing to Rackspace", "error");
			return false;
		}
	}

	/**
	 * Ad2Bucket
	 * Adds ad images and json data to bucket/container
	 * 
	 * @param  OpenCloud\ObjectStore\Resource\Container $b The container belonging to the ad user 
	 * @param  Ad 	$ad  	The ad model
	 * @return boolean  	If the operation succeded
	 */
	public static function ad2Bucket( $b, $ad ) {
		try {

			$oss = self::getOS();
			$targetPath = $_SERVER['DOCUMENT_ROOT'];
			$metadata = array( "type" => $ad->adclass );

            if( in_array( $ad->class, array( 'large','medium','regular' ) ) && !empty($ad->banner) ) {
            	$imgExt = substr( $ad->banner, -3);
            	$origImgName = str_replace("/data/", "", $ad->banner);

                $img = "ad".ltrim($ad->adid, '0')."_img_". Yii::app()->getModule('user')->encrypting( $origImgName ).".".$imgExt;
                $b->uploadObject($img, fopen($targetPath.$ad->banner, 'r+'), DataObject::stockHeaders($metadata) );
            }

            if( in_array( $ad->class, array( 'large','medium','regular','small' ) ) && !empty($ad->bgimage) ) { 
            	$imgExt = substr( $ad->bgimage, -3);
            	$origImgName = str_replace("/data/", "", $ad->bgimage);
            	
                $img = "ad".ltrim($ad->adid, '0')."_bgi_". Yii::app()->getModule('user')->encrypting( $origImgName ).".".$imgExt;
                $b->uploadObject($img, fopen($targetPath.$ad->bgimage, 'r+'), DataObject::stockHeaders($metadata) );
            }

            if( !$b->isCdnEnabled() ) 
            	$b->enableCdn();

            $cdn=$b->getCdn();

            $files = $b->objectList();
            $cadena = $ad->htmlad;
            $head="ad" . ltrim( $ad->adid, '0'). "_";

			foreach ($files as $file) {
			    if( substr( $file->getName(), 0 ,6) == $head ) {

			    	$size = $file->getContentLength();
				    $type = $file->getContentType();

				    if( strpos($file->getName(), 'bgi') !== false ) {
				    	if( isset($bgOn) )
				    		$cadena=$bgOn;
				    	$patron = '/background-image: url\(.*\);/i';
						$sustitución = 'background-image: url(' . $cdn->getCdnUri()."/".$file->getName() . ');';
						$bgOn= preg_replace($patron, $sustitución, $cadena);

				    } else if( strpos($file->getName(), 'img') !== false ) {
				    	if( isset($bgOn) )
				    		$cadena=$bgOn;
				    	$patron = '/ src=".*" /i';
						$sustitución = ' src="' . $cdn->getCdnUri()."/".$file->getName() . '" ';
						$bgOn = preg_replace($patron, $sustitución, $cadena);
				    } 
			    }
			}

			$ad->htmlad=( isset($bgOn) ) ? $bgOn : $ad->htmlad;
			
			if( $ad->update( array('htmlad') ) ) {
				$path = $ad->generateAdFile();
				if( $path ) {
	                // $spaceTools->addToBucket($b, "ad".$ad->adid.".json", $path, array( "type" => $ad->adclass ) );
	                $metadata = array( "type" => "admeta" );
					$b->uploadObject("ad".ltrim($ad->adid, '0').".json", fopen($path, 'r+'), DataObject::stockHeaders($metadata) );
				}
			}

			return true;

		} catch (Exception $e) {

			Yii::log("Fail writing to Rackspace", "error");
			return false;
		}
	}
}

?>