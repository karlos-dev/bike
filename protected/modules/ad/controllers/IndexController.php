<?php

/**
 * 	@author  	Karlos Álvarez <info@weird.is>
 *  @package  	Yii Ads
 * 
 * 	IndexController
 * 
 * 	Manages Ad Module View actions
 * 
 */

class IndexController extends Controller { 

	public $description = 'Bike rent services and touristic bike services in the pleasant city of Barcelona. Place your bike business add in this website.';
	public $defaultAction = 'index';
	public $layout = '//layouts/column2';
	
	public $sizeArr = array(
            "large" =>      "large",
            "medium" =>     "medium",
            "regular" =>    "regular",
            "small" =>      "small",
            "tiny"  =>      "tiny"
        );


	public function actionIndex() {
		
		if( UserModule::isAdmin() )
			$dataProvider=new CActiveDataProvider('Ad');
		else {
			//Get the key
			$user = UserModule::user(Yii::app()->user->id);
			$id = $user->id;

			$criteria=new CDbCriteria( array(                    
	            'order'=>'createdate desc',
	            // Can do an alias for the table
	            // 'with' => array('userToProject'=>array('alias'=>'user')),
	            'condition'=>"uid='".$id."'"
	        ));

			$dataProvider=new CActiveDataProvider('Ad', array(
			    'criteria'=>$criteria,
			) );
		}

		$this->render('ad.views.ad.index',array(
			'dataProvider'=>$dataProvider,
		));
		
	}

}

?>