<?php

/**
 * 	@author  Karlos Álvarez <info@weird.is>
 * 
 * 	AdController
 * 
 * 	Manages PHP Ad Manager modified to meet this project.
 * 
 *  Original PHP Ad Manager sources:
 *  http://admgr.sourceforge.net/
 * 
 *  Contact:
 *  Email: development@digitekdesign.com
 * 
 *  Some commnads:
 * 
 *  ALTER TABLE ads ADD cssad  VARCHAR(4096) NOT NULL DEFAULT '' AFTER htmlad;
 *  ALTER TABLE ads ADD uri VARCHAR(256) NOT NULL DEFAULT '' AFTER title;
 *  ALTER TABLE ads ADD domainid INT(6) ZEROFILL DEFAULT 0 AFTER groupid;
 *  ALTER TABLE ads ADD uid SMALLINT ZEROFILL AFTER adid;
 *  ALTER TABLE ads MODIFY title VARCHAR(192) DEFAULT NULL;
 * 
 * 	ALTER TABLE ads DROP COLUMN classid;
 * 
 */

Yii::import("application.helpers.*");

class AdController extends Controller { 

	protected $apiContext;
	protected $sandBoxId;
	protected $sandBoxSecret;

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2b';

	public $defaultAction = 'index';

	public function init() {

	}

	public static function renderAd( $ad ) {

		// print_r($ad); die;
		$txt= ( isset($ad->textad) ) ? $ad->textad : $ad->title;
		$w=   ( isset($ad->width) )  ? $ad->width."px" : '500px';
		$h=   ( isset($ad->height) ) ? $ad->height."px" : '300px';

		if( isset( $ad->htmlad ) ) {
			//$adstr = '<link href="/css/styles.css" rel="stylesheet" type="text/css" media="screen" />'.
			$adstr = '<div style="width:500px; height:300px; display:inline-block; vertical-align: top;">'.  $ad->htmlad .'</div>';
			$css = (array) json_decode($ad->cssad);
			//echo $ad->htmlad;
			//echo $adstr;
			//echo $css['img']->src;
			//
			if( isset( $css['img']->src )  )
				$adstr = str_replace("src", "src=\"". $css['img']->src . "\"", $adstr);
			
			echo $adstr; 
			return;
		}
		else
			$adstr = "ERROR";

        return $adstr;
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			/*
			
			array( 
				'HttpAuthFilter - rest',
			),
			*/
		
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('rest'),
				'expression' => array('HttpAuthController','auth')
				
			),
			array('allow', // allow authenticated user to perform 'create', 'update' actions, 'admin' is secure also
				'actions'=>array('create','update','stats','activate','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform and 'delete' actions
				'actions'=>array('view', 'admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionStats($id) {

		$criteria = new CDbCriteria;
		$criteria->condition = 'adid='.$id;

		$res = Stat::model()->find($criteria);
		//echo "hello";
		//print_r($res); die;

		$this->render('ad.views.stat.index',array(
			//'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionActivate($id) {

		$this->render('activate',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('index',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Ad;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ad'])) {

			$model->attributes=$_POST['Ad'];
			if( $model->save() )
				$this->redirect(array('view','id'=>$model->adid));
		}

		$this->render('create',array(
			'model'=>$model,
		));

	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ad']))
		{
			$model->attributes=$_POST['Ad'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->adid));
		}
		if(UserModule::isAdmin())
			$this->render('update',array(
				'model'=>$model,
			));
		else
			$this->render('edit',array(
				'model'=>$model,
			));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Rest
	 * 
	 * Accepts requests from js actions
	 * HTTP_BASIC_AUTH needed to access this method
	 * @see HttpAuthController
	 * 
	 * Rest action identifieds
	 * [all]
	 * 	Retrieve data of non expired ads
	 * [geo]
	 * 	Appends geolocated data from user's map requests
	 * [click]
	 *  Validates unique click and Updates click data 
	 * 
	 *  @version 1.3.2
	 */
	public function actionRest($com) { 
		
		/**
		 * @abstract 1.3.1
		 * Errors executing model Ad::model()->findAll() with many criteria elements
		 * Used as --> Yii::app()->mysql->createCommand(
		 * 
		 * @abstract 1.3.2
		 *  - Can update it using Ad::model(). Code is using command as model actions no really needed.
		 *  - Code is working perfect
		 */
		switch( strtolower($com) ) {
			
			// Get all active ads
			case 'all': { 
				$names=[];
				$command= Yii::app()->mysql->createCommand("select uri,title,address,zip,lat,lng,htmlad,hash from ads where expiredate > '".date('Y-m-d H:i:s', time() )."'");
				$reader=$command->query();
				foreach($reader as $row) {
					$names[]=$row;
				}
				echo json_encode($names);
			} break;

			// Request to save Geolocate address data 
			case 'geo': { 
				if( !isset($_POST['data']) )
					return;

				$count=0;
				$arr = json_decode( $_POST['data'] );
				if( !$arr ) {
					echo json_encode(array('error'=>'decoding json error'));
					return;
				}
				foreach ($arr as $obj) {
					// print_r($obj); die;
					if( isset($obj->hash) && isset($obj->lat) && isset($obj->lng) ) {
						$command= Yii::app()->mysql->createCommand("update ads set lat='".$obj->lat."', lng='".$obj->lng."' where hash='".$obj->hash."'");
						$result = $command->execute();
						if( $result >= 1 ) {
							Yii::log('Updated lat lng in '.$obj->address, 'info');
							$count+=1;
						} //elseif( $result == 0) {
						//Yii::log('Already Updated lat lng in '.$obj->address, 'info');
					}
				}

				echo json_encode(array('updated'=>$count));
			} break;

			// Track ad click
			// One click per sessionId, ad
			// One click per user agent, ip, day, ad
			
			case 'click': { 
				if( !isset($_POST['hash']) )
					return;

				$h =  $_POST['hash'];
				$ua = $_POST['ua'];
				$ip = $_POST['i'];
				$sessionId = $_POST['s'];

				if( $sessionId !== session_id() ) {
					Yii::log("Possible xss in $ip","info");
					return;
				}

				$command= Yii::app()->mysql->createCommand("select * from dayclicks where sui='".$sessionId."' and adhash='".$h."'");
				$n=$command->execute();
				if( $n > 0) 
					return;

				$command= Yii::app()->mysql->createCommand("select * from dayclicks where agent='".$ua."' and day='".date("Ymd", time())."' and ip='".$ip."' and adhash='".$h."'");
				$n=$command->execute();
				if( $n > 0 ) 
					return;

				$command= Yii::app()->mysql->createCommand("insert into dayclicks set sui='".$sessionId."', day='".date("Ymd", time())."', agent='".$ua."', ip='".$ip."', adhash='".$h."' ");
				$command->execute();

				$command= Yii::app()->mysql->createCommand("update ads set clicks=clicks+1 where hash=".$h."");
				$result = $command->execute();

				echo json_encode(array('updated'=> ($result>=1) ? true:false ));
			} break;
		}
		
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() {

		/*		
		if( UserModule::isAdmin() )
			$dataProvider=new CActiveDataProvider('Ad');
		else {
			//Get the key
			$user = UserModule::user(Yii::app()->user->id);
			$id = $user->id;

			$criteria=new CDbCriteria( array(                    
	            'order'=>'createdate desc',
	            // Can do an alias for the table
	            // 'with' => array('userToProject'=>array('alias'=>'user')),
	            'condition'=>"uid='".$id."'"
	        ));

			$dataProvider=new CActiveDataProvider('Ad', array(
			    'criteria'=>$criteria,
			) );
		}
		
		$this->render('ad.views.ad.index',array(
			'dataProvider'=>$dataProvider,
		));
		*/
	
		$model=new Ad('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Ad']))
			$model->attributes=$_GET['Ad'];

		$this->render('index',array(
			'model'=>$model,
			//'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Ad('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Ad']))
			$model->attributes=$_GET['Ad'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Ad the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Ad::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Ad $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ad-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}

?>