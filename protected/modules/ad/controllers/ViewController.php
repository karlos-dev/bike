<?php

/**
 * 	@author  Karlos Álvarez <info@weird.is>
 *  @package  Yii-ads
 * 
 * 	ViewController
 * 
 * 	Manages Ad Module View actions
 * 
 */

class ViewController extends Controller { 

	public $description = 'Bike rent services and touristic bike services in the pleasant city of Barcelona. Place your bike business add in this website.';
	public $defaultAction = 'index';
	public $layout = '//layouts/adlayout';
	
	public function actionIndex($url) {
		
		$parsed = strtolower( str_replace("-", " ", $url ) );
		//echo $parsed; die;
		$model = Ad::model()->findByAttributes( array( 'title' => $parsed ) );
		if( $model ) {
			//echo "<pre>Ad $parsed\n"; print_r($model); die; 	
			$this->render('index', array('ad'=>$model));
		}
		else {
			//echo "<pre>Not found add with title '$parsed'\n"; die; 	
			$this->render('index');
		}
		
	}

}

?>