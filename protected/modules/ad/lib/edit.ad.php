<?php

/////////////////////////////////////////////////////////////////////////////////
//
//  PHP Ad Manager
//  Copyright (c) 2001, Digitek Design/Chris Allen
//  http://www.digitekdesign.com/software/
//  http://admgr.sourceforge.net/
//  Email: development@digitekdesign.com
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
/////////////////////////////////////////////////////////////////////////////////


//error_reporting(E_ALL);
//echo " Add\n$edit\n"; print_r( $_POST ); 
//

$_url =         "http://bikerent.barcelona";
$_trackingurl = "http://".$_SERVER['HTTP_HOST']."/tracking.php";
$_textadurl   = "http://".$_SERVER['HTTP_HOST'];


$ad_classes = AdModule::adClasses();
$sizes = array(
    0 => array( 540, 320),
    1 => array( 340, 180),
    2 => array( 270, 160),
    3 => array( 135, 90),
    4 => array( 75.5, 45)
);

if ( isset($_POST['B1']) ) { 
    $B1= $_POST['B1']; 

    /**
     * @abstract 
     *     Hacky Model Validation 1000-01-01 00:00:01
     */
    $filters = array(
        "edit" =>           array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[0-9]{1,32}/") ),
        "notes" =>          array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,1024}/") ),
        "htmlad" =>          array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,4024}/") ),
        "class" =>          array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/(large)|(medium)|(regular)|(small)|(tiny)/i") ),
        "textad" =>         array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,1024}/") ),
        "textadurl" =>      array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,256}/") ),
        "trackingurl" =>    array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,256}/") ),
        "banner" =>         array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,256}/") ),
        "logfile" =>        array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\w+-]{5,50}\.[\w+]{1,3}/i") ),  
        "time" =>           array("filter" => FILTER_VALIDATE_INT),
        "title" =>          array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z \-]{1,128}/i") ),
        "url" =>            array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^www\.(.)*\.[a-z]{2,10}/i") ),
        "active" =>         array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/(Y)|(N)/i") ),
        "size" =>           array("filter" => FILTER_VALIDATE_INT ),
        "hits" =>           array("filter" => FILTER_VALIDATE_INT ),
        "clicks" =>         array("filter" => FILTER_VALIDATE_INT ),
        "domains" =>        array("filter" => FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY ),
        "logtype" =>        array("filter" => FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY ), 
        "adclass" =>        array("filter" => FILTER_VALIDATE_INT ),
        "updmodifydate" =>  array("filter" => FILTER_VALIDATE_REGEXP,  "options"=>array("regexp"=>"/[a-z]{2,4}/i") ),
        "createdate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
        "modifydate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
        "expiredate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
        "lastdisplay" =>    array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
        "statsreset" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
    );

    $input = filter_input_array( INPUT_POST, $filters);    

    $updmodifydate= isset($input['updmodifydate'])?$input['updmodifydate']:$input['updmodifydate']; 
    $title=         isset($input['title']) ? $input['title']:"";
    $textad=        isset($input['textad']) ? $input['textad']:"";  
    $name=          isset($input['name']) ? $input['name']:"";  
    $edit=          isset($input['edit']) ? $input['edit']:"";  
    $logpath=       isset($input['logpath']) ? $input['logpath']:"";  
    $logfile=       isset($input['logfile']) ? $input['logfile']: dirname(__FILE__)."/logs/default.log";  
    $active=        strtoupper( $input['active'] );  
    $notes=         isset($input['notes']) ? $input['notes']:""; 
    $domains=       isset($input['domains']) ? $input['domains']:""; 
    $redir=         isset($input['redir']) ? $input['redir']:""; 
    $banner=        isset($input['banner']) ? $input['banner']:""; 
    $sz=            isset($input['size']) ? $input['size']: 5; 
    $hits=          (int) !empty($input['hits']) ? $input['hits']: 0; 
    $clicks=        (int) !empty($input['clicks']) ? $input['clicks']:0; 

    $htmlad =       isset( $input['htmlad']) ? $input['htmlad']: ''; 

    $classid=       isset($input['adclass']) ? $input['adclass']: 0; 
    $class=         isset($input['class']) ? $input['class']:"tiny";  

    $url =          isset( $input['url'] ) ? $input['url']:$_url;  
    $trackingurl =  isset( $input['trackingurl'] ) ? $input['trackingurl']:$_trackingurl;  
    $textadurl =    isset( $input['textadurl'] ) ? $input['textadurl']:$_textadurl;  
    $logtype =      isset( $input['logtype'] ) ? $input['logtype'] : array();  

    $createdate=    isset($input['createdate']) ? $input['createdate']:'1000-01-01 00:00:01'; 
    $modifydate=    isset($input['modifydate']) ? $input['modifydate']:'1000-01-01 00:00:01'; 
    $expiredate=    isset($input['expiredate']) ? $input['expiredate']:'1000-01-01 00:00:01'; 
    $lastdisplay=   isset($input['lastdisplay']) ? $input['lastdisplay']:'1000-01-01 00:00:01'; 
    $statsreset=    isset($input['statsreset']) ? $input['statsreset']:'1000-01-01 00:00:01'; 

    //echo "<pre>"; print_r( $_POST ); print_r($input); die;
}

if (empty($edit)) { return; }

$db = ad_connect_db();

if (isset($B1) && $B1 == "Cancel") {
    if ($new) {
        ad_query("delete from ads where adid = '$edit'");
        $new = FALSE;
    }
    $edit = "";
    require dirname(__FILE__)."/admin.php"; return;
}

if ( empty($B1) ) {
    $qh = ad_query("select * from ads where adid = '$edit'", $db);
    if (ad_queryrows($qh) > 0)
        $r = ad_fetch($qh);
    else
        return;
} else {

    $width= $sizes[$sz][0];
    $height=$sizes[$sz][1];
    qpe($title, $banner, $url, $trackingurl, $logfile, $textad, $textadurl, $notes);

    if( !empty($domains) )
        $domains = implode("|",$domains);

    if ($logtype != NULL)
        $logtype = implode(",", $logtype);
    else
        $logtype = "";

    if (strtoupper($updmodifydate) == 'ON')
        $modifydate = strftime("%Y-%m-%d %H:%M:%S",time());

    $iq = "update ads set ".
        "domains='$domains',".
        "title='$title',".
        "banner='$banner',".
        "width='$width',".
        "height='$height',".
        "url='$url',".
        "logfile='$logfile',".
        "trackingurl='$trackingurl',".
        "textad='$textad',".
        "textadurl='$textadurl',".
        "hits='$hits',".
        "htmlad='$htmlad',".
        "clicks='$clicks',".
        "class='$class',".
        "adclass='$classid',".
        "logtype='$logtype',".
        "createdate='$createdate',".
        "modifydate='$modifydate',".
        "expiredate='$expiredate',".
        "lastdisplay='$lastdisplay',".
        "statsreset='$statsreset',".
        "active='$active',".
        "notes='$notes'".
        " where adid='$edit'";

    $qh = ad_query($iq, $db);
    
    unset($_POST['B1']);
    unset($_POST['edit']);

    $edit = null;

    $new = FALSE;
    require dirname(__FILE__)."/admin.php";
    return;
}

?>

<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--

function getimagesize() {
    window.open("admin.php?view=imgsize&img=" + editad.banner.value, "getimagesize",
           "height=70,width=250,resizable=1");
}
//-->
</SCRIPT>
<B>Edit Advertising Item</B>
<form method="POST" action="/site/manager" name="editad">
    <table width=600 border=0 cellspacing=1 cellpadding=1 class='edittop'>
    	<tr>
    		<td width="100" class="desc">AD ID</td>
    		<td width="320">
            <input type="text" name="edit" value="<?php print $r['adid'];?>" readonly>
            <input type="hidden" name="view" value="editad">
            <input type="hidden" name="new" value="<?php print (isset($new))?$new:""; ?>">
    		</td>
            <td width="80" class="desc">Class</td>
            <td width="80">
                <select name="adclass">
                <?php       
                    $ct = 0; 
                    while ($ct < count($ad_classes)) {
                        if ( isset($r['class']) && $r['class'] == $ad_classes[ $r['adclass'] ] )  {
                            print "<option value=\"$ct\" ";
                            print " selected>{$ad_classes[$ct]}</option>\n";  
                        }
                        else {
                            print "<option value=\"$ct\" ";
                            print " >{$ad_classes[$ct]}</option>\n";  
                        }
                        $ct++;
                    } 
                ?>
                </select>
            </td>
    	</tr>
        <tr>
            <td width="100" class="desc">User ID</td>
            <td width="320">
                <input type="text"   name="uid" value="<?php print $r['uid'];?>" readonly>
            </td>
        </tr>
        <tr>
            <td width="100" class="desc">Hashed</td>
            <td width="320">
                <input type="text"   name="uid" value="<?php print $r['hash'];?>" readonly>
            </td>
        </tr>
    	<tr>
    		<td class="desc">Title</td>
    		<td colspan=3><input type="text" name="title" size="60" value="<?php print $r['title'];?>"></td>
    	</tr>
        <tr>
        </tr>
    	<tr>
    		<td class="desc">Banner</td>
        	<td><input type="text" name="banner" size="60" value="<?php print $r['banner'];?>"></td>
    		<td class="desc"><a href="">Dimensions</a></td>
        	<td>
                <select name="size">
                <?php       
                    $ct = 0; 
                    while ($ct < count($ad_classes)) {
                        if ( isset($r['class']) && $r['class'] == $ad_classes[ $r['adclass'] ] )  {
                            print "<option value=\"$ct\" ";
                            print " selected>{$ad_classes[$ct]} {$sizes[$ct][0]}x{$sizes[$ct][1]}</option>\n";  
                        }
                        else {
                            print "<option value=\"$ct\" ";
                            print " >{$ad_classes[ $ct ]} {$sizes[$ct][0]}x{$sizes[$ct][1]}</option>\n";  
                        }
                        $ct++;
                    } 
                ?>
                </select>
                <!--
                    <input type="text" name="width" size="4" value="<?php //print $r['width'];?>"> x
                    <input type="text" name="height" size="4" value="<?php //print $r['height'];?>">
                -->
        	</td>
    	</tr>
    	<tr>
    		<td class="desc">URL</td>
    		<td><input type="text" name="url" size="60" value="<?php print $r['url'];?>"></td>
    	</tr>
    	<tr>
    		<td class="desc">Tracking URL</td>
    		<td><input type="text" name="trackingurl" size="60" value="<?php print $r['trackingurl'];?>"></td>
    	</tr>
    	<tr>
            <td class="desc">Title</td>
    		<td><input type="text" name="textad" size="60" value="<?php print $r['textad'];?>"></td>
    	</tr>
        <tr><td class="desc">Css</td><td colspan="1"><textarea rows="8" cols="75" class="prettyprint"><?php /*echo $r['cssad'];*/ echo str_replace("}", "}\n\r", str_replace("\",\"", "\",&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"", str_replace(":{\"", ":{\n\t\"", $r['cssad'] )));  ?></textarea></td></tr>
    	<tr><td class="desc">HTML</td><td colspan="1"><textarea name="htmlad" rows="8" cols="75">
            <?php print $r['htmlad'];?>
        </textarea></td></tr>
        <tr>
    		<td class="desc">Text Ad URL</td>
    		<td><input type="text" name="textadurl" size="60" value="<?php print $r['textadurl'];?>"><br>
    		<font class="small"><i>If not defined, will use main URL<i></font></td>
    	</tr>
    	<tr>
    		<td class="desc">Logfile</td>
    		<td><input type="text" name="logfile" size="60" value="<?php print $r['logfile'];?>"></td>
    		<td colspan="2"><input type="checkbox" name="logtype[]" value="hits" <?php ischk("hits", $r['logtype']);?>> Hits &nbsp;
    		<input type="checkbox" name="logtype[]" value="clicks" <?php ischk("clicks",$r['logtype']);?>> Clicks
    		</td>
    	</tr>
        <tr>
            <td class="desc">Domains</td>
            <td><?php $domains=explode("|",$r['domains']); listdomains( array( $r['domainid'] ) ); ?></td>
        </tr>
    </table>

    <table width=600 border=0 cellspacing=1 cellpadding=1 class='editbottom'>
    	<tr>
    		<td width="100" class="desc">Hits</td>
    		<td width="190"><input type="text" name="hits" size="10" value="<?php print $r['hits'];?>"></td>
    		<td width="100" class="desc">Clicks</td>
    		<td width="190"><input type="text" name="clicks" size="10" value="<?php print $r['clicks'];?>"></td>
    	</tr>
    	<tr>
    		<td class="desc">Create Date</td>
    		<td><input type="text" name="createdate" size="20" value="<?php print $r['createdate'];?>"></td>
    		<td class="desc">Modify Date</td>
    		<td><input type="text" name="modifydate" size="20" value="<?php print $r['modifydate'];?>"></td>
    	</tr>
    	<tr>
    		<td class="desc">Expire Date</td>
    		<td><input type="text" name="expiredate" size="20" value="<?php print $r['expiredate'];?>"></td>
    		<td class="desc">Last Display</td>
    		<td><input type="text" name="lastdisplay" size="20" value="<?php print $r['lastdisplay'];?>"></td>
    	</tr>
    	<tr>
    		<td class="desc">Active</td>
            <td width="80">
                <select name="active">
                    <option value="Y" <?php echo ( strtolower( $r['active'] ) == 'y') ? "selected" : "";?> >Yes</option>
                    <option value="N" <?php echo ( strtolower( $r['active'] ) == 'n') ? "selected" : "";?> >No</option>
                </select>
            <!--
            <input type="text" name="active" size="2" value="<?php print $r['active'];?>">
    		<font class="small"><i>[Y]es [N]o</i></font>
            -->
            </td>
    		<td class="desc">Stats Reset</td>
    		<td><input type="text" name="statsreset" size="20" value="<?php print $r['statsreset'];?>"></td>
    	</tr>
    	<tr>
    		<td class="desc">Notes</td>
    		<td colspan="3"><textarea name="notes" rows=4 cols=60><?php print $r['notes'];?></textarea></td>
    	</tr>
    	<tr>
    		<td colspan="4" align=center>
                <input type="checkbox" checked <?php ischk("clicks", $r['logtype']);?> name="updmodifydate"> Update Modify Date<br>
    			<input type="submit" value="Update" name="B1" /> 
                <input type="submit" value="Cancel" name="B1" />
    		</td>
    	</tr>
    </table>
</form>

