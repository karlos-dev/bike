<?php

/////////////////////////////////////////////////////////////////////////////////
//
//  PHP Ad Manager
//  Copyright (c) 2001, Digitek Design/Chris Allen
//  http://www.digitekdesign.com/software/
//  http://admgr.sourceforge.net/
//  Email: development@digitekdesign.com
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
/////////////////////////////////////////////////////////////////////////////////
error_reporting(E_ALL);

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."ads.cfg");
//echo dirname(__FILE__).DIRECTORY_SEPARATOR."ads.cfg"; die;
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."ads.lib");
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."admin.lib");

// echo "<pre>"; print_r( $_SERVER ); print_r( $_POST ); die;
// echo "host $ad_db_host - user $ad_db_user"; die;

/**
 *  @abstract
 *      In case of Update Opertaion from edit.domain.php
 *      Redirect to update 
 * 
 */
if ( isset($_POST['B1']) && $_POST['B1'] == "Update") {
    $edit=$_POST['B1'];
    if( array_key_exists( 'hits', $_POST ) || array_key_exists( 'clicks', $_POST) )
        include dirname(__FILE__)."/edit.ad.php";
    else
        include dirname(__FILE__)."/edit.domain.php";
    
    return;
}


if( !isset($id) && isset($_GET['id']) ) {
    // print_r($_GET); die;
    $id = $_GET['id'];
}

if( !isset($view) && isset($_GET['view']) )
    $view = $_GET['view'];

if( !isset($edit) && isset($_GET['edit']) )
    $edit = $_GET['edit'];

if( !isset($delete) && isset($_GET['delete']) )
    $delete = $_GET['delete'];

if( !isset($activate) && isset($_GET['activate']) )
    $activate = $_GET['activate'];

if (!isset($sentheader) ) {

?>
<!-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
    <TITLE>PHP Ad Manager</TITLE>
    <link rel="stylesheet" href="admin.css">
</HEAD>
<BODY BGCOLOR="black" TEXT="gray" LINK="white" VLINK="white">
-->
<style type="text/css">
    table.ads {
        background-color: #eee;
        color: #444;
    }
    table.ads tr.header { background-color: #ddd; color: #aaa; }

    a           { color: #888; }
    a:visited   { color: #111; }
    a:hover     { color: #00e009; }

</style>

<h1>Advertising Administration</h1>

<table class="ads base" border="0"><tr><td width="620" align="center">
<!-- End of Header -->
<?php
    $sentheader = TRUE;
    }
    try {
    if ( isset( $view ) ) {
        switch($view) {
            case "":
                show_menu();
                break;
            case "newad":
                $db=ad_connect_db();

                $dt = strftime("%Y-%m-%d %H:%M:%S",time());
                $zero = "1000-01-01 00:00:01";// strftime("%Y-%m-%d %H:%M:%S",time());
                $ex = strftime("%Y-%m-%d %H:%M:%S",strtotime($ad_expire_default));
                ad_query("insert into ads set active='N', createdate='$dt',".
                    "expiredate='$ex', modifydate='$dt', lastdisplay='$zero', statsreset='$zero', title='-New Undefined Ad-', groupid='0', class='00'", $db);
                $edit = mysqli_insert_id($db);
                $new = TRUE;
                
                require dirname(__FILE__).DIRECTORY_SEPARATOR."edit.ad.php";
                break;
            case "editad":
                if (!empty($delete)) {
                    delete("ads","adid", $delete);

                    //
                    // FIX ??? Trigger RackSpace delete of ad items
                    // 
                    $user = UserModule::user( Yii::app()->user->id );
                    SpaceController::deleteAdFromBucket( $user, $delete );

                }
                if (!empty($activate)) {
                    toggle_active("ads","adid",$activate);
                }
                if (!empty($edit)) {
                    require dirname(__FILE__)."/edit.ad.php";
                } else {
                    show_ads();
                }
                break;
            case "newdomain":
                $db=ad_connect_db();
                $log = $ad_logpath_default.'/default';
                ad_query("insert into domains set active = 'N', name='', redir='', logpath='$log'", $db);
                $edit = mysqli_insert_id($db);
                $new = TRUE;

                require dirname(__FILE__)."/edit.domain.php";
                break;
            case "editdomain":
                if (!empty($delete)) {
                    // echo "deleting..."; die;
                    delete("domains","domainid", $delete);
                }
                if (!empty($activate)) {
                    toggle_active("domains","domainid", $activate);
                }
                if (!empty($edit)) {
                    // echo "Editing...";
                    require dirname(__FILE__)."/edit.domain.php";
                } else {
                    show_domains();
                }
                break;
            case "log":
                viewlog($id, "");
                break;
            case "add":
                // echo "Add $id";
                showad($id);
                break;
            case "resetstats":
                $dt = strftime("%Y-%m-%d %H:%M:%S",time());
                $db = ad_connect_db();
                if (!empty($id))
                    ad_query("update ads set hits=0,clicks=0,statsreset='$dt' where adid = '$id'", $db);
                else
                    ad_query("update ads set hits=0,clicks=0,statsreset='$dt'", $db);

                if (!empty($rp))
                    require "$rp";
                else
                    require dirname(__FILE__)."/admin.php";
                break;
            case "optdb":
                optdb();
                break;
            case "buildhc":
                buildhc();
                break;
            case "imgsize":
                getimgsize($img);
                return;
                break;
            default:
                print "<p><font class=\"large\">Module '$view' not implemented</font></p>";
                print "<FORM ACTION=\"/site/manager\"><DIV ALIGN=\"CENTER\"><INPUT TYPE=\"SUBMIT\" VALUE=\"Main Menu\"></DIV></FORM>";
                break;
        }
    } else {
        show_menu();
    }

    } catch (Exception $e) {
        echo "<pre>";
        echo $e->getMessage();
        echo $e->getTraceAsString();
        die;
    }
    
if (! isset( $sentfooter) ) {
?>
<!-- Footer -->
</td></tr></TABLE>
<TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0" WIDTH="620"><TR>
	<TD ALIGN="CENTER">
      <BR>
		<FONT CLASS="small"><I>Copyright &copy; 2016, Weird IT</I></FONT>
	</TD>
</TR></TABLE>

<!-- End of Footer 
</BODY>
</HTML>
-->
<?php 
    $sentfooter = TRUE;
} ?>
