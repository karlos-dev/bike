<?php

/////////////////////////////////////////////////////////////////////////////////
//
//  PHP Ad Manager
//  Copyright (c) 2001, Digitek Design/Chris Allen
//  http://www.digitekdesign.com/software/
//  http://admgr.sourceforge.net/
//  Email: development@digitekdesign.com
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
/////////////////////////////////////////////////////////////////////////////////
error_reporting(E_ALL);
// echo "<pre> FIle!\n"; print_r( $_POST ); die;

if ( isset($_POST['B1']) ) { 
    $B1= $_POST['B1']; 

    /**
     * @abstract 
     *     Hacky Model Validation
     */
    $filters = array(
        "edit" =>           array("filter" => FILTER_VALIDATE_INT),
        "notes" =>          array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1, 1024}/") ),
        "logpath" =>        array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/^(\w+/){1,2}\w+\.\w+$/i") ),  
        "time" =>           array("filter" => FILTER_VALIDATE_INT),
        "name" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z \-\.\/]{1,128}/i") ),
        "uri" =>            array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z \-\.]{1,256}/i") ),
        "redir" =>          array("filter" => FILTER_VALIDATE_URL),
        "active" =>         array("filter" => FILTER_VALIDATE_REGEXP,  "options"=>array("regexp"=>"/(Y)(N)/i") ),
        "createdate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
        "modifydate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
        "updmodifydate" =>  array("filter" => FILTER_VALIDATE_REGEXP,  "options"=>array("regexp"=>"/[a-z]{2,4}/i") ),
    );

    $input = filter_input_array(INPUT_POST, $_POST);    

    $updmodifydate= isset($input['updmodifydate'])?$input['updmodifydate']:$input['updmodifydate']; 
    $notes=         isset($input['notes']) ? $input['notes']:"";  
    $name=         isset($input['name']) ? $input['name']:"";  
    $edit=         isset($input['edit']) ? $input['edit']:"";  
    $logpath=      isset($input['logpath']) ? $input['logpath']:"";  
    $active=       strtoupper( $input['active'] );  
    $notes=        isset($input['notes']) ? $input['notes']:""; 
    $redir=        isset($input['redir']) ? $input['redir']:""; 

}

if ( empty($edit) ) { return; }

$db =ad_connect_db();

if ( isset($B1) && $B1 == "Cancel") {
    if ($new) {
        ad_query("delete from domains where domainid = '$edit'");
        $new = FALSE;
    }
    $edit = "";
    require dirname(__FILE__)."/admin.php"; return;
}

if (empty($B1)) {
    $qh =  ad_query("select * from domains where domainid = '$edit'", $db);
    
    // Number of ads in current domain
    $qh2 = ad_query("select adid from ads where domainid = '$edit'", $db);
    $nads = ad_queryrows($qh2);

    if (ad_queryrows($qh) > 0)
        $r = ad_fetch($qh);
    else
        return;
} else {
    qpe($name, $redir, $logpath, $notes);

    if ( strtoupper($updmodifydate) == 'ON' )
        $modifydate = "modifydate='".strftime("%Y-%m-%d %H:%M:%S", time())."'," ;
    else {

    }

    $iq = "update domains set ".
        "name='$name',".
        "redir='$redir',".
        "logpath='$logpath',".
        //"modifydate='$modifydate',".
        "$modifydate".
        "notes='$notes',".
        "active='$active'".

        " where domainid='$edit'";

    $qh = ad_query($iq);

    //echo "<pre>$iq"; print_r( $qh ); die;

    print mysql_error();
    $new = FALSE;

    unset($_POST['B1']);
    unset($_POST['edit']);

    $edit = null;

    require dirname(__FILE__)."/admin.php";
    return;
}

?>

<B>Edit Domain</B>
<form method="POST" action="/site/manager">
<table width=600 border=0 cellspacing=1 cellpadding=1 class="edit dom">
	<tr>
		<td class="desc">Domain ID</td>
		<td>
        <input type="text" name="edit" value="<?php print $r['domainid'];?>" readonly>
        <input type="hidden" name="view" value="editdomain">
        <input type="hidden" name="new" value="<?php //print $new; ?>">
		</td>
	</tr>
	<tr>
		<td class="desc">Name</td>
		<td><input type="text" readonly name="name" size="50" value="<?php print $r['name'];?>"></td>
	</tr>
    <tr>
        <td class="desc">Uri</td>
        <td><input type="text" name="uri" size="50" value="<?php print $r['uri'];?>"></td>
    </tr>
	<tr>
		<td class="desc">Redir Script</td>
    	<td><input type="text" name="redir" size="60" value="<?php print $r['redir'];?>"><br>
    	<font class="small"><i>Leave blank to use system default (<?php print $ad_redir_default;?>)</i></font></td>
	</tr>
	<tr>
		<td class="desc">Log Path</td>
		<td><input type="text" name="logpath" size="60" value="<?php print $r['logpath'];?>"><br>
    	<font class="small"><i>Leave blank to use system default (<?php print $ad_logpath_default;?>)</i></font></td>
	</tr>
    <tr>
        <td class="desc">Registered Ads</td>
        <td><input readonly type="text" name="createdate" size="20" value="<?php print $nads;?>">
        </td>
    </tr>
    <tr>
        <td class="desc">Create Date</td>
        <td><input readonly type="text" name="createdate" size="20" value="<?php print $r['createdate'];?>">
        </td>
    </tr>
	<tr>
		<td class="desc">Modify Date</td>
		<td><input type="text" name="modifydate" size="20" value="<?php print $r['modifydate'];?>">
		<font class="small"><i>2001-01-01 12:00:00</i></font></td>
	</tr>
	<tr>
		<td class="desc">Active</td>
		<td>
        <!-- <input type="text" name="active" size="2" value="<?php print $r['active'];?>"><font class="small"><i>[Y]es [N]o</i></font>-->
        <select name="active">
            <option value="Y">Yes</option>
            <option value="N">No</option>
        </select>
        </td>
	</tr>
	<tr>
		<td class="desc">Notes</td>
		<td><textarea name="notes" rows=4 cols=50><?php print $r['notes'];?></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
            <input type="checkbox" checked name="updmodifydate"> Update Modify Date<br>
			<input type="submit" value="Update" name="B1"> <input type="submit" value="Cancel" name="B1">
		</td>
	</tr>
</table>
</form>

