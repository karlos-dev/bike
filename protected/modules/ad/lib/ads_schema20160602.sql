-- MySQL dump 10.13  Distrib 5.7.11, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: ads
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adlog`
--

DROP TABLE IF EXISTS `adlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adlog` (
  `adid` int(6) unsigned zerofill NOT NULL,
  `type` enum('hit','click') DEFAULT NULL,
  `remotehost` varchar(64) NOT NULL,
  `remoteaddr` varchar(20) NOT NULL,
  `entrydate` datetime DEFAULT NULL,
  `site` varchar(35) DEFAULT NULL,
  KEY `adid` (`adid`),
  KEY `type` (`type`),
  KEY `site` (`site`),
  KEY `entrydate` (`entrydate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ads` (
  `adid` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uid` smallint(5) unsigned zerofill DEFAULT NULL,
  `groupid` int(6) unsigned NOT NULL DEFAULT '0',
  `domainid` int(6) unsigned zerofill DEFAULT '000000',
  `adclass` tinyint(1) unsigned zerofill DEFAULT '4',
  `class` varchar(16) DEFAULT NULL,
  `title` varchar(192) DEFAULT NULL,
  `hash` varchar(20) NOT NULL,
  `uri` varchar(256) NOT NULL DEFAULT '',
  `banner` varchar(255) DEFAULT NULL,
  `bgimage` varchar(255) NOT NULL DEFAULT '',
  `htmlad` varchar(4096) NOT NULL DEFAULT '',
  `cssad` varchar(4096) NOT NULL DEFAULT '',
  `address` varchar(128) NOT NULL DEFAULT '',
  `zip` int(5) unsigned zerofill DEFAULT '00000',
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `tags` tinytext,
  `services` tinytext,
  `width` int(4) DEFAULT NULL,
  `height` int(4) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `textad` varchar(255) DEFAULT NULL,
  `textadurl` varchar(255) DEFAULT NULL,
  `trackingurl` varchar(255) DEFAULT NULL,
  `logfile` varchar(255) DEFAULT NULL,
  `logtype` set('hits','clicks') DEFAULT NULL,
  `hits` int(8) unsigned DEFAULT NULL,
  `clicks` int(8) unsigned DEFAULT NULL,
  `priority` enum('1','2','3') DEFAULT '2',
  `domains` tinytext,
  `createdate` datetime NOT NULL,
  `modifydate` datetime NOT NULL,
  `expiredate` datetime NOT NULL,
  `lastdisplay` datetime NOT NULL,
  `statsreset` datetime NOT NULL,
  `notes` text,
  `active` char(1) DEFAULT NULL,
  PRIMARY KEY (`adid`),
  KEY `adid` (`adid`)
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains` (
  `domainid` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `uri` varchar(256) DEFAULT '',
  `redir` varchar(128) NOT NULL,
  `logpath` varchar(128) NOT NULL,
  `createdate` datetime DEFAULT NULL,
  `modifydate` datetime DEFAULT NULL,
  `notes` text,
  `active` enum('Y','N','P','D') DEFAULT NULL,
  KEY `domainid` (`domainid`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `paymentid` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `paypalid` varchar(64) NOT NULL,
  `state` varchar(32) DEFAULT '',
  `payment_method` varchar(32) DEFAULT '',
  `amount` smallint(6) NOT NULL,
  `hash` varchar(20) NOT NULL,
  `usr_status` varchar(32) DEFAULT '',
  `usr_email` varchar(128) DEFAULT '',
  `usr_id` varchar(64) NOT NULL,
  `usr_first_name` varchar(128) DEFAULT '',
  `usr_last_name` varchar(128) DEFAULT '',
  `createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`paymentid`),
  KEY `paymentid` (`paymentid`),
  KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stat`
--

DROP TABLE IF EXISTS `stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stat` (
  `adid` int(6) unsigned zerofill NOT NULL,
  `date` datetime DEFAULT NULL,
  `hits` int(8) unsigned DEFAULT NULL,
  `clicks` int(8) unsigned DEFAULT NULL,
  `likes` int(8) unsigned DEFAULT NULL,
  `statid` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`statid`),
  KEY `adid` (`adid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-02 21:53:11
