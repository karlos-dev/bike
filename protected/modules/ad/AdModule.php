<?php

/**
* Ads module class file.
*
* @author Karlos Álvarez <karlos.alvan@gmail.com>
* @copyright Copyright &copy; 2016 Karlos Álvarez
* @version 1.0.0
* 
* Append this to the main.php config file:
* 		....
* 
* 		'modules'=>array(
*     		'ads'=>array(
* 
*     		),
*       ),
*       ....
*/

class AdModule extends CWebModule {

	private 		$_assetsUrl;
	public 			$adClasses;
	public static 	$availableClasses = array(
		                'large',
		                'medium',
		                'regular',
		                'small',
		                'tiny'
		            );

	public function init() {

		// import the module-level models and components
		$this->setImport(array(
			'ad.models.*',
			'ad.components.*',
			'ad.controllers.*',
		));
		$this->defaultController = 'ad';
		parent::init();
	}

	public function beforeControllerAction($controller, $action) {

		if(parent::beforeControllerAction($controller, $action)) {

			return true;
			
		} else
			return false;
	}

	public static function adClasses(){ return self::$availableClasses; }

	/**
	* Registers the necessary scripts.
	*/
	public function registerScripts() {
		// Get the url to the module assets
		$assetsUrl = $this->getAssetsUrl();

		// Register the necessary scripts
		$cs = Yii::app()->getClientScript();

		//$cs->registerCoreScript('jquery');
		//$cs->registerCoreScript('jquery.ui');
		//$cs->registerScriptFile($assetsUrl.'/js/rights.js');
		//$cs->registerCssFile($assetsUrl.'/css/core.css');

		// Make sure we want to register a style sheet.
		
		if( $this->cssFile!==false ) {
			// Default style sheet is used unless one is provided.
			if( $this->cssFile===null )
				$this->cssFile = $assetsUrl.'/css/default.css';
			else
				$this->cssFile = Yii::app()->request->baseUrl.$this->cssFile;

			// Register the style sheet
			$cs->registerCssFile($this->cssFile);
		}
	}

	/**
	* Publishes the module assets path.
	* @return string the base URL that contains all published asset files of Rights.
	*/
	public function getAssetsUrl() {

		if( $this->_assetsUrl===null )
		{
			$assetsPath = Yii::getPathOfAlias('ads.assets');

			// We need to republish the assets if debug mode is enabled.
			if( $this->debug===true )
				$this->_assetsUrl = Yii::app()->getAssetManager()->publish($assetsPath, false, -1, true);
			else
				$this->_assetsUrl = Yii::app()->getAssetManager()->publish($assetsPath);
		}

		return $this->_assetsUrl;
	}

	/**
	* @return the current version.
	*/
	public function getVersion() {
		return '1.0.0';
	}
}
