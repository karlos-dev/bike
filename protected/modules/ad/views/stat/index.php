<?php
/* @var $this StatController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Stats',
);

$this->menu=array(
	//array('label'=>'Create Stat', 'url'=>array('create')),
	//array('label'=>'Manage Stat', 'url'=>array('admin')),
	array('label'=>'Back', 'url'=>array('ad/index'))
);



?>


<h3>Stats</h3>

<?php 
/*

 $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */ ?>

<div class='options'> <span class="option">Hits</span> | <span class="option">Clicks</span> | <span class="option">Likes</span> </div>
<canvas id="myChart" width="300" height="180"></canvas>
<script src="/js/jquery-1.12.3.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<script>
var myChart;
var ctx = document.getElementById("myChart");
var json = '[{"hits":3,"clicks":1,"likes":0,"ts":1462827540000},{"hits":5,"clicks":3,"likes":0,"ts":1463227540000},{"hits":8,"clicks":4,"likes":1,"ts":1464227540000},{"hits":9,"clicks":5,"likes":2,"ts":1464827540000},{"hits":10,"clicks":7,"likes":3,"ts":1465227540000},{"hits":15,"clicks":8,"likes":4,"ts":1465827540000}]';

var obj = JSON.parse(json);
console.log(new Date().getTime());



function drawSelection(selection) {
	var data=[], labels=[], dt, hits=[], likes=[];
	for (var i in obj ) {

		hits.push(obj[i].hits);
		likes.push(obj[i].likes);
		data.push(obj[i].clicks);
		dt=new Date(obj[i].ts);

		labels.push( dt.toDateString() );
	}

	myChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: labels,
	        datasets: [
	        	{
		            label: ' Hits',
		            data: hits,
		            backgroundColor: "rgba(220,20,220,0.2)",
	    			strokeColor: "rgba(220,20,220,1)",
	    			fontColor: "rgba(100,120,100,1)",
	    			borderColor: "rgba(100,120,100,1)",

		            /*backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(153, 102, 255, 0.2)',
		                'rgba(255, 159, 64, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255,99,132,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],*/
		            borderWidth: 1
		        },
		        {
		            label: ' Clicks',
		            data: data,
		            backgroundColor: "rgba(54, 162, 235, 0.2)",
	    			strokeColor: "rgba(54, 162, 235, 0.2)",
	    			fontColor: "rgba(54, 162, 235, 1)",
	    			borderColor: "rgba(54, 162, 235, 1)",
		            borderWidth: 1
		        },
		        {
		            label: ' Likes',
		            data: likes,
		            backgroundColor: 'rgba(255, 159, 64, 0.6)',
		            borderColor: 'rgba(255, 159, 64, 1)',
		            borderWidth: 1
		        }
	        ]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        },
	        /*
	        legend: {
	            display: true,
	            labels: {
	                fontColor: 'rgb(255, 99, 132)'
	            }
	        }*/
	    }
	});
}

$(document).ready(function(){ 

	console.log("Document ready");
	
	var defaultMode='hits';
	drawSelection(defaultMode);

	$('.option').on('click', function(){
		drawSelection( $(this).html().toLowerCase() );
	});
});

</script>