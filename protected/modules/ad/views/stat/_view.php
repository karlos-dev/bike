<?php
/* @var $this StatController */
/* @var $data Stat */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('statid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->statid), array('view', 'id'=>$data->statid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adid')); ?>:</b>
	<?php echo CHtml::encode($data->adid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hits')); ?>:</b>
	<?php echo CHtml::encode($data->hits); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clicks')); ?>:</b>
	<?php echo CHtml::encode($data->clicks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('likes')); ?>:</b>
	<?php echo CHtml::encode($data->likes); ?>
	<br />


</div>