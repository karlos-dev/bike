<?php
/* @var $this StatController */
/* @var $model Stat */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stat-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'statid'); ?>
		<?php echo $form->textField($model,'statid',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'statid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adid'); ?>
		<?php echo $form->textField($model,'adid',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'adid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hits'); ?>
		<?php echo $form->textField($model,'hits',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'hits'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clicks'); ?>
		<?php echo $form->textField($model,'clicks',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'clicks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'likes'); ?>
		<?php echo $form->textField($model,'likes',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'likes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->