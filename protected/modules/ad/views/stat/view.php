<?php
/* @var $this StatController */
/* @var $model Stat */

$this->breadcrumbs=array(
	'Stats'=>array('index'),
	$model->statid,
);

$this->menu=array(
	array('label'=>'List Stat', 'url'=>array('index')),
	array('label'=>'Create Stat', 'url'=>array('create')),
	array('label'=>'Update Stat', 'url'=>array('update', 'id'=>$model->statid)),
	array('label'=>'Delete Stat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->statid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Stat', 'url'=>array('admin')),
);
?>

<h1>View Stat #<?php echo $model->statid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'statid',
		'adid',
		'date',
		'hits',
		'clicks',
		'likes',
	),
)); ?>
