<?php
/* @var $this AdController */
/* @var $data Ad */
?>
<link href="/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,600,800' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="/js/forms/css/jquery.idealforms.css">
<link rel="stylesheet" href="/js/sb/jquery.switchButton.css">
<link rel="stylesheet" href="/css/colorPicker.css">
<style type="text/css">
	.view-item { width: 50%; /* float: left; */ display: inline-block; position: relative; }
	.view-item.mapbox { position: relative; max-width: 500px; max-height: 300px; }
</style>
<div class="view">
	<div class="view-item">
		<b><?php echo CHtml::encode($data->getAttributeLabel('adid')); ?>:</b>
		<?php echo CHtml::link(CHtml::encode($data->adid), array('view', 'id'=>$data->adid)); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
		<?php echo CHtml::encode($data->uid); ?>
		<br />

		<b><?php echo Yii::t('app','Stats'); ?>:</b>
		<?php echo CHtml::link(Yii::t('app','Ad ' .$data->adid. ' Stats'), array('ad/stats', 'id'=>$data->adid)); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('groupid')); ?>:</b>
		<?php echo CHtml::encode($data->groupid); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('domainid')); ?>:</b>
		<?php echo CHtml::encode($data->domainid); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('adclass')); ?>:</b>
		<?php echo CHtml::encode($data->adclass); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('class')); ?>:</b>
		<?php echo CHtml::encode($data->class); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
		<?php echo CHtml::encode($data->title); ?>
		<br />
	</div>
	<div class="view-item mapbox">
	<?php echo $data->htmlad ?>
	</div>
</div>