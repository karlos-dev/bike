<?php
/* @var $this AdController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ads',
);

$this->menu=array(
	array('label'=>'Create Ad', 'url'=>array('create')),
	array('label'=>'Manage Ad', 'url'=>array('admin'), 'visible'=>UserModule::isAdmin() ),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ad-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<h3>Ads</h3>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'ad.views.ad._view',
)); */?>

<?php 

	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ad-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'adid',
		//'hash',
		/*array(
			'name' => 'title',
			'type' => 'raw',
			'value' => '$data->title',
			'filter' => false,
			'htmlOptions' => array(
		        'style' => 'text-align: left;',
		    ),
		),*/
		array(
			'name' => 'uri',
			'header'=>'Title',
			'type' => 'raw',
			'value' => 'CHtml::link( "$data->uri", Yii::app()->createUrl($data->uri) )',
			'htmlOptions' => array(
		        'style' => 'width: 220px; text-align:left; overflow:hidden;',
		    ),
		),
		array(
			'name' => 'uri',
			'filter' => false,
            'sortable'=>false,
            'header'=>'Stats',
			'type' => 'raw',
			'value' => 'CHtml::link( "stats", Yii::app()->createUrl("myads/ad/stats", array("id"=>$data->adid)) )',
			'htmlOptions' => array(
		        'style' => 'text-align: left;',
		    ),
		),
		array(
            'name'=>'class',
            'filter'=>CHtml::dropDownList('Ad[class]', $model->class, $this->sizeArr, array('empty'=>'All') ),
            'value'=>'$data->class',
            'htmlOptions' => array(
		        'style' => 'width: 55px; text-align: center;',
		    ),
        ),
		
		// 'address',
		// 'zip',
		
		'url',
		'hits',
		'clicks',
		/*
		'priority',
		'domains',
		*/
		array(
            'name'=>'expiredate',
            //'filter'=>CHtml::dropDownList('Ad[class]', $model->class, $this->sizeArr, array('empty'=>'All') ),
            'value'=>' (new DateTime($data->expiredate))->format("j F Y") ',
            'htmlOptions' => array(
		        'style' => 'width: 70px; text-align: left; font-size:11px;',
		    ),
        ),
		/*
		'modifydate',
		'createdate',
		'lastdisplay',
		'statsreset',
		'notes',
		*/
		//'active',
		array(
            'name'=>'active',
            'type' => 'raw',
            'filter'=>CHtml::dropDownList('Ad[active]', $model->active, array( 'Y'=>'Yes', 'N'=>'No' ), array('empty'=>'All') ),
            'value'=>'( $data->active == "Y") ? "Yes" : CHtml::link( "Activate", Yii::app()->createUrl("myads/ad/activate", array("id"=>$data->adid)) )',
            'htmlOptions' => array(
		        'style' => 'width: 40px; text-align: center;',
		    ),
        ),
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
