<?php
/* @var $this AdController */
/* @var $model Ad */

$this->breadcrumbs=array(
	'Ads'=>array('index'),
	$model->title=>array('view','id'=>$model->adid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ad', 'url'=>array('index')),
	array('label'=>'Create Ad', 'url'=>array('create')),
	array('label'=>'View Ad', 'url'=>array('view', 'id'=>$model->adid)),
	array('label'=>'Manage Ad', 'url'=>array('admin')),
);
?>

<h1>Update Ad <?php echo $model->adid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>