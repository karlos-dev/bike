<?php
/* @var $this AdController */
/* @var $model Ad */

$this->breadcrumbs=array(
	'Ads'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Ad', 'url'=>array('index')),
	array('label'=>'Create Ad', 'url'=>array('create')),
	array('label'=>'Update Ad', 'url'=>array('update', 'id'=>$model->adid)),
	array('label'=>'Delete Ad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->adid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ad', 'url'=>array('admin')),
);
?>

<h1>View Ad #<?php echo $model->adid; ?></h1>



<?php 

	

	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'adid',
		'uid',
		'groupid',
		'domainid',
		'adclass',
		'class',
		'title',
		'hash',
		'uri',
		'banner',
		'bgimage',
		'htmlad',
		'cssad',
		'address',
		'zip',
		'tags',
		'services',
		'width',
		'height',
		'url',
		'textad',
		'textadurl',
		'trackingurl',
		'logfile',
		'logtype',
		'hits',
		'clicks',
		'priority',
		'domains',
		'createdate',
		'modifydate',
		'expiredate',
		'lastdisplay',
		'statsreset',
		'notes',
		'active',
	),
)); ?>
