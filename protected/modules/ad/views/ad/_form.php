<?php
/* @var $this AdController */
/* @var $model Ad */
/* @var $form CActiveForm */
?>
<script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<style type="text/css">
	.row-elem { display: inline-block; margin-right: 5px; }
	option:checked { background-color: #eee; } 
	input:read-only { background-color: #ddd; }
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ad-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="row-elem">
			<?php echo $form->labelEx($model,'uid'); ?>
			<?php echo $form->textField($model,'uid',array('readonly'=>true)); ?>
			<?php echo $form->error($model,'uid'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'groupid'); ?>
			<?php echo $form->textField($model,'groupid',array('size'=>6,'maxlength'=>6, 'readonly'=>true)); ?>
			<?php echo $form->error($model,'groupid'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'domainid'); ?>
			<?php echo $form->textField($model,'domainid',array('size'=>6,'maxlength'=>6, 'readonly'=>true)); ?>
			<?php echo $form->error($model,'domainid'); ?>
		</div>

		

		<div class="row-elem">
			<?php echo $form->labelEx($model,'adclass'); ?>
			<?php echo $form->textField($model,'adclass',array('size'=>6,'maxlength'=>16)); ?>
			<?php echo $form->error($model,'adclass'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'active'); ?>
			<?php echo $form->dropDownList($model,'active',array('Y'=>'Yes','N'=>'No')); ?>
			<?php echo $form->error($model,'active'); ?>
		</div>

	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>192)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hash'); ?>
		<?php echo $form->textField($model,'hash',array('size'=>20,'maxlength'=>20,'readonly'=>true)); ?>
		<?php echo $form->error($model,'hash'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uri'); ?>
		<?php echo $form->textField($model,'uri',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'uri'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'banner'); ?>
		<?php echo $form->textField($model,'banner',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'banner'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bgimage'); ?>
		<?php echo $form->textField($model,'bgimage',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bgimage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'htmlad'); ?>
		<?php echo $form->textField($model,'htmlad',array('size'=>60,'maxlength'=>4096,'readonly'=>true)); ?>
		<?php echo $form->error($model,'htmlad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cssad'); ?>
		<?php echo $form->textField($model,'cssad',array('size'=>60,'maxlength'=>4096,'readonly'=>true)); ?>
		<?php echo $form->error($model,'cssad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zip'); ?>
		<?php echo $form->textField($model,'zip',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'zip'); ?>
	</div>

	<div class="row">
		
		<script type="text/javascript">
	        $(window).on('load', function(){
	        	//$('option').attr('selected').css({'background-color': '#555'});
	        	//
	        	// $('option[selected=selected]').css({'background-color': '#555'});

	        	$('option').click( function(e){ 
	        		//alert('click');
	        		var attr = $(this).attr('selected');
	        		console.log(attr);
	        		if( typeof attr !== typeof undefined ) 
	        			{  $(this).attr('selected', 'selected'); }
	        		else 
	        			{ $(this).removeAttr('selected'); } 
	        		});
	        } );
        </script>
		<div class="row-elem">
			<?php echo $form->labelEx($model,'tags'); ?>
			<?php
	        if(isset($model->tags))
	        {
	        		$tags = array( 'classic','electric','fixie', 'cheapie' );
	                // snipe product list and build the option array
	                $m = explode(",", $model->tags);
	                $x = 0;
	                $pp=[];
	                foreach ($m as $p)
	                {
						$pp[$p] = array('selected'=>'selected');
						$x++;
	                }
	        }
	        echo $form->listBox($model,
	        		'tags', 
	         		$tags,
		         	array(
		         		'multiple'=>'multiple',
		         		'size'=> count($tags), 
		         		'options'=>$pp 
		         	)
	         	);
	        ?>
			<?php // echo $form->textArea($model,'tags',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'tags'); ?>
		</div>
		<div class="row-elem">
			<?php echo $form->labelEx($model,'services'); ?>
			<?php
	        if(isset($model->services))
	        {
	        		$services = array( 'tours','shop', 'repair' );
	        		$servicesKV = array( 'tours'=>'Tours','shop'=>'Shop', 'repair'=>'Repair' );
	                // snipe product list and build the option array
	                $m = explode(",", $model->services);
	                $x = 0;
	                $pp=[];
	                foreach ($m as $p)
	                {
						$pp[$p] = array('selected'=>'selected');
						$x++;
	                }
	        }

	        ?>
	        
	        <?php echo CHtml::dropDownList('Ad[services][]', $m, $servicesKV, array('prompt'=> 'Select services', 'multiple' => 'multiple')); ?>

			<?php //echo $form->textArea($model,'services',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'services'); ?>
		</div>
	</div>
	<div class="row">
		<div class="row-elem">
			<?php echo $form->labelEx($model,'class'); ?>
			<?php echo $form->textField($model,'class'); ?>
			<?php echo $form->error($model,'class'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'width'); ?>
			<?php echo $form->textField($model,'width',array('size'=>4,'maxlength'=>6,'readonly'=>true)); ?>
			<?php echo $form->error($model,'width'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'height'); ?>
			<?php echo $form->textField($model,'height',array('size'=>4,'maxlength'=>6,'readonly'=>true)); ?>
			<?php echo $form->error($model,'height'); ?>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'textad'); ?>
		<?php echo $form->textField($model,'textad',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'textad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'textadurl'); ?>
		<?php echo $form->textField($model,'textadurl',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'textadurl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trackingurl'); ?>
		<?php echo $form->textField($model,'trackingurl',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'trackingurl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'logfile'); ?>
		<?php echo $form->textField($model,'logfile',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'logfile'); ?>
	</div>

	
	<div class="row">
		<div class="row-elem">
			<?php echo $form->labelEx($model,'logtype'); ?>
			<?php echo $form->textField($model,'logtype',array('size'=>8,'maxlength'=>32,'readonly'=>true)); ?>
			<?php echo $form->error($model,'logtype'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'hits'); ?>
			<?php echo $form->textField($model,'hits',array('size'=>8,'maxlength'=>8,'readonly'=>true)); ?>
			<?php echo $form->error($model,'hits'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'clicks'); ?>
			<?php echo $form->textField($model,'clicks',array('size'=>8,'maxlength'=>8,'readonly'=>true)); ?>
			<?php echo $form->error($model,'clicks'); ?>
		</div>

		<div class="row-elem">
			<?php echo $form->labelEx($model,'priority'); ?>
			<?php echo $form->textField($model,'priority',array('size'=>1,'maxlength'=>1)); ?>
			<?php echo $form->error($model,'priority'); ?>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'domains'); ?>
		<?php echo $form->textArea($model,'domains',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'domains'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate', array('readonly'=>true)); ?>
		<?php echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modifydate'); ?>
		<?php echo $form->textField($model,'modifydate',array('readonly'=>true) ); ?>
		<?php echo $form->error($model,'modifydate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expiredate'); ?>
		<?php echo $form->textField($model,'expiredate'); ?>
		<?php echo $form->error($model,'expiredate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastdisplay'); ?>
		<?php echo $form->textField($model,'lastdisplay',array('readonly'=>true)); ?>
		<?php echo $form->error($model,'lastdisplay'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'statsreset'); ?>
		<?php echo $form->textField($model,'statsreset',array('readonly'=>true)); ?>
		<?php echo $form->error($model,'statsreset'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

