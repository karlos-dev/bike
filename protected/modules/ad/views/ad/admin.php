


<?php

/**
 * SEE
 * http://www.yiiframework.com/forum/index.php/topic/9644-searching-using-a-dropdown-in-the-admin-view-quick-search/
 * 
 */

/* @var $this AdController */
/* @var $model Ad */

$this->breadcrumbs=array(
	'Ads'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Ad', 'url'=>array('index')),
	array('label'=>'Create Ad', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ad-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Ads</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ad-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'adid',
		'hash',
		/*
		'uid',
		 'groupid',
		'domainid',
		'adclass',
		'class',
		*/
		'title',
		
		'uri',
		/*
		'banner',
		'bgimage',
		'htmlad',
		'cssad',
		*/
		'address',
		//'zip',
		/*
		'tags',
		'services',
		'width',
		'height',
		*/
		'url',
		/*
		'textad',
		'textadurl',
		'trackingurl',
		'logfile',
		'logtype',
		*/
		'hits',
		'clicks',
		/*
		'priority',
		'domains',
		*/
		'createdate',
		/*
		'modifydate',
		'expiredate',
		'lastdisplay',
		'statsreset',
		'notes',
		*/
		'active',
		/*array(
            'name'=>'active',
            'filter'=>CHtml::dropDownList('ad[active]', $data->active, array( 'Y'=>'Yes', 'N'=>'No' ) ),
            'value'=>'CHtml::encode( ad::model()->active )',
        ),*/
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
