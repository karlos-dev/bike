<?php
/* @var $this AdController */
/* @var $model Ad */

$this->breadcrumbs=array(
	'Ads'=>array('index'),
	'Create',
);

$this->menu=array(
  array('label'=>'Back', 'url'=>array('index')),
  array('label'=>'Manage Ad', 'url'=>array('admin'), 'visible'=>UserModule::isAdmin() ),
);

// The name I want to show in the main h1 html tag
$name='bikerent';

// Provide sizes from server to client
$jsonObj = json_encode( $this->sizeArr );
$line = "<script type=\"text/javascript\">\n\n\tvar _sizes='$jsonObj';\n\n</script>\n";

// Ease current user access
$logeduser = UserModule::user( Yii::app()->user->id );

// Form security
$unique_salt=Yii::app()->user->getStateKeyPrefix();


?>
<link href="/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if gt IE 8]><!--><link href="/css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /><!--<![endif]-->
<!--[if !IE]> <link href="/css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<!--[if gt IE 6]> <link href="/css/styles-ie.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,600,800' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="/js/forms/css/jquery.idealforms.css">
<link rel="stylesheet" href="/js/sb/jquery.switchButton.css">
<link rel="stylesheet" href="/css/colorPicker.css">
<style type="text/css">
	#content { padding: 0; }
  ul { padding: 0; }

  #content hr { margin-left: 20px; width: 90%; } 
  ul.operations { padding: 20px; padding-bottom: 0; }


	.span-5.last { margin-left: 10px; }
	.col1-1.last {
	    margin-bottom: 0;
	}
</style>


<div id="wrap">
  <?php // echo $this->renderPartial('header'); ?>
  <div id="content">
    <div class="container clearfix">
      <div id="container">
        <div class="col1-1 home element display last">
          <div class="board">
            <div class="ad-select">
              <label>Select Ad size</label>
              <select class="adclass">

                <option value="large"><?php echo Yii::t('app','Large'); ?></option>
                <option value="medium"><?php echo Yii::t('app','Medium'); ?></option>
                <option value="regular"><?php echo Yii::t('app','Regular'); ?></option>
                <option value="small"><?php echo Yii::t('app','Small'); ?></option>
                <option value="tiny"><?php echo Yii::t('app','Tiny'); ?></option>
              </select>
            </div>
            <div class="pre">
              <div class="ad-box large">
                <a class="adlnk" href="" target="_blank">
                  <div class="elem-ad"></div>
                  <div class="add-str shadow">
                    <h2>great electric bikes for a great day</h2>
                  </div>
                </a>
              </div>
            </div>
            <div class="status">
              <div class="span-6">
                <div class="title">Ad title</div>
                <div class=""><a href="" class="website" target="_blank">www.your-website.com</a></div>
                <div class="address"></div>
                <div class="zip"></div>
                <ul class="tags">
                </ul>
                <ul class="services">
                </ul>
                <div class="url-resume">
                  <div class="res-title">Your Ad urls<span class="help"></span></div>
                  <ul>
                    <li class="url"></li>
                  </ul>
                </div>
              </div>                
              <div class="span-4">
                <div class="map"></div>
              </div>
            </div>
          </div>
          <div class="tools">
          <!--
            <div class="idealsteps-container">
              <form action="" novalidate autocomplete="off" class="idealforms ad-data">
                <div class="idealsteps-wrap">
                  <section class="idealsteps-step">
                      <div class="field">
                        <label class="main"><?php echo Yii::t('app','Title'); ?>:</label>
                        <input name="title" type="text" data-idealforms-ajax="/site/checkav" placeholder="<?php echo Yii::t('app','Ad title'); ?>">
                        <span class="error"></span>
                      </div>
                      <div class="switch-wrapper txt">
                        <input class="sb" type="checkbox" value="1" checked>
                        <input type="hidden" name='wax' value="">
                        <label for="font-co"><?php echo Yii::t('app','Font'); ?></label> <input id="font-co"   type="text" name="colorfo" value="#fff" />
                        <label for="bg-co"><?php echo Yii::t('app','Background'); ?></label> <input id="bg-co" type="text" name="colorbg" value="#535353" />
                        <label for="sh-co"><?php echo Yii::t('app','Shadow'); ?></label> <input id="sh-co"     type="text" name="colorsh" value="transparent" />
                      </div>

                      <div class="field">
                        <label class="main"><?php echo Yii::t('app','Website'); ?>:</label>
                        <input name="website" type="text" placeholder="<?php echo Yii::t('app','www.your-website.com'); ?>">
                        <span class="error"></span>
                      </div>

                      <?php $timestamp = time();?>
                      <input type="hidden" name='timestamp' value="<?php echo $timestamp;?>">
                      <input type="hidden" name='token' value="<?php echo md5( $unique_salt . $timestamp);?>">

                      <div class="field s">
                        <label class="main"><?php echo Yii::t('app','Background Image'); ?>:</label>
                        <input id="bg_picture" name="image" type="file" multiple="false" placeholder="<?php echo Yii::t('app','Choose Ad Image'); ?>">
                        <span class="error"></span>
                      </div>

                      <div class="field s radio">
                        <div class="switch-wrapper img s">
                          <input class="sb" type="checkbox" value>
                        </div>

                        <div class="switch-wrapper img-side hidden s">
                          <input class="sb" type="checkbox" value>
                        </div>
                        <p class="group position img s">
                          <label><input name="pos" type="radio" value="bgr">Background</label>
                          <label><input name="pos" type="radio" value="prt">Portrait</label>
                        </p>
                        <span class="error"></span>
                      </div>

                      <div class="field s s2">
                        <label class="main"><?php echo Yii::t('app','Image'); ?>:</label>
                        <input id="picture" name="top-image" type="file" multiple="false" placeholder="<?php echo Yii::t('app','Choose Ad Image'); ?>">
                        <span class="error"></span>
                      </div>

                      <div class="field radio prescale s s2">
                        <p class="group position top-img s s2">
                          <label><input name="pos" type="radio" value="frt">Front</label>
                          <label><input name="pos" type="radio" value="beh">Behind</label>
                        </p>
                        <span class="error"></span>
                      </div>

                      <span class="s s2" style="vertical-align: top; padding-right: 0.35rem; margin-top: 3px; display: inline-block; font-size: 1rem;">|</span>
                      <div class="field scale s s2">
                        <label class="main"><?php echo Yii::t('app','Scale'); ?>:</label>
                        <select>
                            <option value="1" selected="selected">1:1</option>
                            <option value="2">1:2</option>
                            <option value="3">1:4</option>
                            <option value="4">1:8</option>
                          </select>
                        <span class="error"></span>
                      </div>

                      <div class="field buttons">
                        <label class="main">&nbsp;</label>
                        <button type="button" class="next step-one"><?php echo Yii::t('app','Next'); ?> > </button>
                      </div>
                    </form>
                  </section>

                  <section class="idealsteps-step">

                      <div class="field">
                        <label class="main">Bike Rent Shop Address:</label>
                        <input name="address" type="text" placeholder="Mallorca 342, Barcelona" class="">
                        <span class="error"></span>
                      </div>
                      <div class="field">
                        <label class="main">Área Code</label>
                        <input name="zip" type="text" placeholder="08018" class="">
                        <span class="error"></span>
                      </div>

                      <div class="field">
                        <label class="main">Singular Bike Types:</label>
                        <p class="group position">
                          <label><input value="classic" type="checkbox" name="tags[]">Classic</label>
                          <label><input value="electric" type="checkbox" name="tags[]">Electric</label>
                          <label><input value="fixie"   type="checkbox" name="tags[]">Fixie</label>
                          <label><input value="cheapie" type="checkbox" name="tags[]">Cheapie</label>
                        </p>
                        <span class="error"></span>
                      </div>

                      <div class="field">
                        <label class="main">Other services:</label>
                        <p class="group position">
                          <label><input value="tours"   type="checkbox" name="services[]">Tours</label>
                          <label><input value="shop"  type="checkbox" name="services[]">Shop</label>
                          <label><input value="repair"  type="checkbox" name="services[]">Repair</label>
                        </p>
                        <span class="error"></span>
                      </div>

                      <div class="field buttons">
                        <label class="main">&nbsp;</label>
                        <button type="button" class="prev"> < Prev</button>
                        <button type="submit" class="submit">Next > </button>
                      </div>
                  </section>
                </div>
              </form>
            </div>
          -->
            <div class="idealsteps-container user-data"> 
              <div class="idealsteps-wrap">
                <section class="idealsteps-step">
                  <form action="" novalidate autocomplete="off" class="idealforms user-data">
                    <div class="field">
                      <label class="main">Name:</label>
                      <input name="name" type="text" readonly data-idealforms-ajax="/site/checkun" value="<?php echo ( Yii::app()->user->isGuest ) ? "" : $logeduser->profile->first_name." ".$logeduser->profile->last_name; ?>">
                      <span class="error"></span>
                    </div>
                      <?php if( Yii::app()->user->isGuest ) { ?>

                        <div class="field">
                          <label class="main">E-Mail:</label>
                          <input name="email" type="email" autocomplete="off" data-idealforms-ajax="/site/checkem">
                          <span class="error"></span>
                        </div>

                        <div class="field">
                          <label class="main">Password:</label>
                          <input name="password" type="password" autocomplete="off">
                          <span class="error"></span>
                        </div>
                      <?php } ?>

                      <div class="field resume">
                        <input name="paylog" type="hidden"/>
                        
                        <?php $timestamp = time();?>
                        <input type="hidden" name='timestamp' value="<?php echo $timestamp;?>" />
                        <label class="main">Your Ads:</label>

                        <div class="ad-resume">
                          <ul>
                          </ul>
                        </div>
                        <span class="error"></span>
                      </div>
                      <!--
                      <div class="field radio pmnt">
                        <label class="main">Payment Period:</label>
                        <p class="group position temp">
                          <label><input name="pos" type="radio" value="month">1 Month</label>
                          <label><input name="pos" type="radio" value="year">1 Year</label>
                        </p>
                        <span class="error"></span>
                      </div>
                      -->
                      <div class="field radio pmnt">
                        <label class="main">Payment Method:</label>
                        <p class="group position pay">
                          <label><input name="pos" type="radio" value="ppal">PayPal</label>
                          <label><input name="pos" type="radio" value="ccard">Credit Card</label>
                        </p>
                        <span class="error"></span>
                      </div>
             
                      <div class="field pmnt ppal">
                        <span class="error"></span>
                         <a class="move" data-paypal-button="true">
                          <img width="145" src="https://www.paypalobjects.com/webstatic/en_US/mktg/merchant/product-selection/ec-button.png" alt="Check out with PayPal" />
                        </a>
                        <p>You will be redirected to Paypal.com<br>Follow the instructions to accept the payment.</p>
                      </div>
                      
                      <div class="field pmnt ccard">
                        <!-- <label class="main"></label> <input name="username" type="text" data-idealforms-ajax="ajax.php"> -->
                        <span class="error"></span>
                          <a class="move" data-paypal-button="true">
                            <img width="145" src="http://www.interigual.com/wp-content/uploads/2014/11/paypal_footer.png" alt="Check out with PayPal" />
                          </a>
                      </div>

                      <div class="field pmnt ccard">
                        <label class="main">Credit Card Holder</label>
                        <input name="cardholder" type="name" autocomplete="off">
                        <span class="error"></span>
                      </div>

                      <div class="field pmnt ccard">
                        <label class="main">Credit Card Number</label>
                        <input name="cardnumber" type="name" autocomplete="off" placeholder="4200 2021 4534 4522" data-idealforms-ajax="/site/checkcard">
                        <span class="error"></span>
                      </div>

                      <div class="field pmnt ccard fdate exp">
                        <label class="main">Expire Date</label><span class="calen"></span>
                        <input name="expire" type="text" class="mystuff" autocomplete="off" placeholder="09/2021">
                        <span class="error"></span>
                      </div>

                      <div class="field pmnt ccard fdate csv">
                        <label class="main">CSV</label>
                        <input name="cardcsv" type="name" placeholder="761" autocomplete="off">
                        <span class="error"></span>
                      </div>
                 
                      <div class="field fback">
                        <!-- <label class="main">Paypal:</label>
                        <input name="username" type="text" data-idealforms-ajax="ajax.php">
                        <span class="error"></span> -->
                      </div>

                      <div class="field buttons">
                        <label class="main">&nbsp;</label>
                        <button type="button" class="back"> < Back</button>
                        <!-- <button type="button" class="other"> Create other </button> -->
                        <button type="submit" class="submit">Proceed</button>
                      </div>
                  </form>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php // echo $this->renderPartial('footer'); ?>
</div>
<div id="backtotop">
  <ul>
    <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
  </ul>
</div>
<!--<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
<script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.9.2.min.js" type="text/javascript"></script>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script src="/js/mustache.js" type="text/javascript"></script>
<script src="/js/forms/js/out/jquery.idealforms.js"></script>
<script src="/js/preloader.js" type="text/javascript"></script>
<!--
https://github.com/olance/jQuery-switchButton
-->
<script type="text/javascript" src="/js/sb/jquery.switchButton.js"></script>
<script type="text/javascript" src="/js/jquery.colorPicker.js"/></script>

<script type="text/javascript">
  var _noInit = 'activate';
  var _adobj='<?php echo json_encode($model->attributes);?>';
  var _timestamp= '<?php echo $timestamp;?>',
      _token='<?php echo md5($unique_salt . $timestamp);?>';

      $(document).ready(function(){
        /*setTimeout( function(){
          $('html, body').animate({
                scrollTop: $(".mine.one").offset().top
              }, 2000);
          }, 1500 );
          */
      });
</script>

<script src="/node_modules/bounce.js/bounce.min.js" type="text/javascript"></script>
<script src="/js/ads.js" type="text/javascript"></script>

<?php echo $line;?>

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
