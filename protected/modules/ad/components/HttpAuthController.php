<?php
/**
 * HttpAuthFilter class
 * 
 * from: https://github.com/DaSourcerer/yii-httpauth
 */
class HttpAuthController extends Controller {
	/**
	 * The model handling authentication
	 *
	 * In a new, bootstrapped Yii application, this will be 'LoginModel' (which also happens to be the default).
	 * @var string
	 */
	public static $authModel='UserLogin';

	/**
	 * The login model's attribute carrying the username
	 * @var string
	 */
	public static $usernameAttribute='username';

	/**
	 * The login model's attribute carrying the password
	 * @var string
	 */
	public static $passwordAttribute='password';

	/**
	 * The 'realm' advertised to the http client
	 *
	 * This can be some descriptive text regarding the resource you are trying to protect. If set to <kbd>null</kbd>,
	 * the value of Yii::app()->name will be taken. Please see to it that no characters outside iso-8859-1 make it here
	 * as this could seriously cripple http responses. Also note that this value will be turned into a quoted string
	 * which mandates the escaping of double-quotes (") and backslashes (\). This seems to cause problems with some
	 * browsers like <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=676358">Firefox</a>.
	 * @var string|null
	 */
	public $realm;

	public static function auth()
	{
		if(!Yii::app()->user->isGuest)
			return true;

		if(!array_key_exists('PHP_AUTH_USER', $_SERVER))
			$this->sendAuthHeaders();

		$model=new self::$authModel;
		$model->{self::$usernameAttribute}=$_SERVER['PHP_AUTH_USER'];
		$model->{self::$passwordAttribute}=$_SERVER['PHP_AUTH_PW'];

		// HERE 
		// append rights module, ads-map communicator auth
		// 
		if(!$model->validate()) {
			self::sendAuthHeaders();
			return false;
		}
		//else
		//	Yii::log("HTTP_BASIC_AUTH OK", 'info');

		return true;
	}

	/**
	 * Send out the headers demanding authentication by the client among a 401 (unauthorized) status code.
	 * @throws CHttpException
	 */
	protected static function sendAuthHeaders()
	{
		if(self::$realm===null)
			self::$realm=Yii::app()->name;
		self::$realm=addcslashes(self::$realm, '"\\');
		header(sprintf('WWW-Authenticate: Basic realm="%s"', self::$realm));
		throw new CHttpException(401);
	}
}