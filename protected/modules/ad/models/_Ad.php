<?php

/**
 * Ad class
 * 
 * @see  http://www.yiiframework.com/doc/api/1.1/CActiveRecord
 */

class _Ad extends KActiveRecord {

	public $adid;
	public $groupid;
	public $adclass;
	public $priority;

	public $uid;
	public $domainid;
    public $title;
    public $hash;
    public $uri;
    public $textad;
    public $logfile;

    public $class;
    public $htmlad;
    public $cssad;
    public $address;
    public $zip;

    public $tags;
    public $services;

    public $active;
    public $notes;
    public $domains;
    public $redir;
    public $banner;
    public $bgimage;

    public $hits;
    public $clicks;

    public $width;
    public $height;
    public $url;

    public $trackingurl;
    public $textadurl;
    public $logtype;

    public $createdate;
    public $modifydate;
    public $expiredate;
    public $lastdisplay;
    public $statsreset;


    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }    
    
    public function getDbConnection() {

        return $this->getAdDbConnection();
    }

	/**
	 * Declares the validation rules.
	 */
	public function rules() {

		return array(
			array('title, priority, textad, class, active, url, uri', 'required'),
			array('hash, groupid, domainid, uid, notes, htmlad, cssad, domains, redir, banner, bgimage, hits, clicks, url, trackingurl, textadurl, logtype', 'safe'),
			array('logfile, address, zip, tags, services, class, priority, adclass', 'safe'),
			
			array('domainid, expiredate, active, title', 'safe' ),
			array('title', 'unique', 'attributeName'=>'title', 'caseSensitive'=>false ),

			array('groupid, hits, clicks', 'numerical'),

			// defauls
			array('banner', 				 'default', 'value' => "", 							 'setOnEmpty' => true ),
			array('createdate',  'default', 'value' => date('Y-m-d H:i:s', time() ), 'setOnEmpty' => false ),
			array('modifydate',  'default', 'value' => date('Y-m-d H:i:s', time() ), 'setOnEmpty' => true ),
            array('lastdisplay, statsreset', 'default', 'value' => '1000-01-01 00:00:01',		 'setOnEmpty' => true, 'on' => 'insert'),
            array('expiredate', 			 'default', 'value' => date('Y-m-d H:i:s', time() + 60*20 ), 'setOnEmpty' => true, 'on' => 'insert'),
            array('active', 			 	 'default', 'value' => 'N', 								 'setOnEmpty' => true, 'on' => 'insert'),
            array('logfile', 			 	 'default', 'value' => 'my-default-log-file.log', 			 'setOnEmpty' => true, 'on' => 'insert'),
            
            array('hits', 	'default', 'value' => 0, 		 					 'setOnEmpty' => false, 'on' => 'insert'),
            array('clicks', 'default', 'value' => 0,		 					 'setOnEmpty' => false, 'on' => 'insert'),
			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	public function tableName() {
		return 'ads';
	}

	public function primaryKey() {
	    return 'adid';
	}

	public function activateAd( $expire=null ) {
		if( $expire )
			$this->expiredate = $expire;

		$this->active = 'Y';
		$this->save(false);
	}

	/**
	 * getUrl
	 * 
	 * Formats current ad name in form of ad url
	 * 
	 * @return string
	 */
	public function getUrl(){
		return Controller::stripAccents( str_replace(" ", "-", strtolower( $this->title ) ) );
	}

	public function beforeSave(){
	   if(parent::beforeSave()) {
	   
	   		if( is_string($this->tags) ) {
	         // for example
	   		} else 
	   			if( is_array($this->tags) )
	        		$this->tags = ( !empty($this->tags) ) ? implode(",", $this->tags ) : "";

	        if( is_string($this->services) ) {
	         // for example
	   		} else 
	        	if( is_array($this->services) )
	        		$this->services = ( !empty($this->services) ) ? implode(",", $this->services ) : "";

	        return true;
	   }
	   return false;
	}

	/** 
	 * @override
	 * 
	 * @param  [type]  $attributes  [description]
	 * @param  boolean $clearErrors [description]
	 * @return [type]               [description]
	 */
	
	/*
	public function validate( $attributes=NULL, $clearErrors=true) {

		$filters = array(
	        "notes" =>          array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,1024}/") ),
	        "htmlad" =>         array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,4096}/") ),
	        "class" =>          array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/(large)|(medium)|(regular)|(small)|(tiny)/i") ),
	        "textad" =>         array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,1024}/") ),
	        "textadurl" =>      array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[a-zA-Z -\/\.\?]{1,256}/") ),
	        "trackingurl" =>    array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,256}/") ),
	        "banner" =>         array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,256}/") ),
	        "address" =>        array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,256}/") ),
	        "zip" =>            array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[0-9]{4,5}/i") ),	
	        "hash" =>           array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[0-9a-z]{8,20}/i") ),	
	        "banner" =>         array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,256}/") ),
	        "logfile" =>        array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[a-z \-\.\/]{1,64}\.[\w+]{1,3}/i") ),  
	        //"logpath" =>        array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z\/ \-]{1,128}/i") ),
	        "title" =>          array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z \-]{1,128}/i") ),
	        "url" =>            array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^[a-z]{2,10}\.(.)*\.[a-z]{2,10}/i") ),
	        "active" =>         array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/(Y)|(N)/i") ),
	        "size" =>           array("filter" => FILTER_VALIDATE_INT ),
	        "hits" =>           array("filter" => FILTER_VALIDATE_INT ),
	        "clicks" =>         array("filter" => FILTER_VALIDATE_INT ),
	        "domains" =>        array("filter" => FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY ),
	        "services" =>       array("filter" => FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY ),
	        "tags" =>        	array("filter" => FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY ),
	        "logtype" =>        array("filter" => FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY ), 
	        "adclass" =>        array("filter" => FILTER_VALIDATE_INT ),
	        "createdate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
	        "modifydate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
	        "expiredate" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
	        "lastdisplay" =>    array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
	        "statsreset" =>     array("filter" => FILTER_VALIDATE_REGEXP,  "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
	    );

		
	    $input = filter_var_array( $this->attributes, $filters );   
	    $this->attributes = array_filter($input);

	    // $this->logpath = Yii::app()->basePath . '/extensions/admgr/logs/default/';
	    //$this->logfile = 'my-default-log-file.log';
	    
	    //
	    $this->uri = $this->getUrl();
	    $this->tags = 	  ( !empty($input['tags']) ) ? implode(",", $input['tags'] ) : $this->tags;
	    $this->services = ( !empty($input['services']) ) ? implode(",", $input['services'] ) : $this->services;

	    

	    $this->priority = '3';
	    $this->groupid = 0;
	    $this->adclass = ( !empty($input['adclass']) ) ? implode(",", $input['adclass'] ) : $this->adclass;
	    
	    // $this->modifydate = '1000-01-01 00:00:01'; 
	    // $this->expiredate =  date('Y-m-d H:i:s', time() + 60*20 ); 
	    // $this->lastdisplay = '1000-01-01 00:00:01'; 
	    // $this->statsreset =  '1000-01-01 00:00:01'; 
	    
	    //$this->hits=0;
	    //$this->clicks=0;
	    
	    $sizes = array(
	        "large" => array( 540, 320),
	        "medium" => array( 340, 180),
	        "regular" => array( 270, 160),
	        "small" => array( 135, 90),
	        "tiny" => array( 75.5, 45)
	    );

	    $this->width= $sizes[$input['class']][0];
	    $this->height=$sizes[$input['class']][1];

	    $result = parent::validate($this->attributes);

	    return ( true && $result );
	}
	*/

	public function generateAdFile() {
		$json = json_encode($this->attributes);
		$fname =  Yii::app()->basePath . DIRECTORY_SEPARATOR.'..'. "/data/"."ad{$this->adid}.json";
		if( file_put_contents( $fname, $json ) !== false ) {
			return $fname;
		}
		return null;
	}

	public function getAdid() {
		return $this->adid;
	}

	public function setDomainid( $did ) {
		$this->domainid=$did;
	}


}

