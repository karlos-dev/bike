<?php

/**
 * Domain class
 * 
 * @see  http://www.yiiframework.com/doc/api/1.1/CActiveRecord
 */

class Domain extends KActiveRecord {

	/**
	 * The followings are the available columns in table 'users':
	 * @var integer $domainid
	 */
	
	public $uid;
	public $logpath;
	public $uri;
    public $name;
    public $tags;

    public $active;
    public $notes;
    public $redir;

    public $createdate;
    public $modifydate;


    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }    
    
    public function getDbConnection() {

        return self::getAdDbConnection();
    }

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('name, active, uri, uid', 'required'),
			array('domainid, uid, notes, redir, logtpath, uri, tags', 'safe'),
			array('name', 'unique', 'attributeName'=>'name', 'caseSensitive'=>false ),
			array('createdate, modifydate', 'default', 'value' => date('Y-m-d H:i:s', time() ), 'setOnEmpty' => true ),
            //array('expiredate, lastdisplay, statsreset', 'default', 'value' => 0,      						 'setOnEmpty' => true, 'on' => 'insert'),
			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	public function tableName() {
		return 'domains';
	}

	public function primaryKey() {
	    return 'domainid';
	}

	/**
	 * getUrl
	 * 
	 * Formats current ad name in form of ad url
	 * 
	 * @return string
	 */
	public function getUrl(){
		//return Controller::stripAccents( str_replace(" ", "-", strtolower( $this->title ) ) );
	}

	/** 
	 * @override
	 * 
	 * @param  [type]  $attributes  [description]
	 * @param  boolean $clearErrors [description]
	 * @return [type]               [description]
	 */
	public function validate( $attributes=NULL, $clearErrors=true) {

		$filters = array(
	        "notes" =>          array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,1024}/") ),
	        "logpath" =>        array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z \-\.\/]{1,64}\.[\w+]{1,3}/i") ),  
	        "time" =>           array("filter" => FILTER_VALIDATE_INT),
	        "name" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z \-\.]{1,128}/i") ),
	        "uri" =>            array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^[a-z]{2,10}\.(.)*\.[a-z]{2,10}/i") ),
	        "active" =>         array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/(Y)|(N)/i") ),
	        "tags" =>        	array("filter" => FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY ),

	        "createdate" =>     array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
	        "modifydate" =>     array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,19}/") ),
	    );

	    $input = filter_var_array( $this->attributes, $filters );    
	    $this->attributes = $input;

	    if( empty($this->logpath) )
	    	$this->logpath = '/usr/local/www/musicloader/protected/modules/ads/lib/logs/default';

	    $this->tags = 	  implode(",", ( empty($input['tags']) ) ? array() : $input['tags'] );
	    $this->redir = 	  $this->uri."/redir.php";
	    $this->modifydate = '1000-01-01 00:00:01'; 

	    return ( true && parent::validate( null, true) );
	}

}
