<?php

/**
 * @author  Karlos Alvarez <karlos.alvan@gmail.com>
 * 
 * 
 * Payment class
 * 
 * @see  http://www.yiiframework.com/doc/api/1.1/CActiveRecord
 * @see  http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/ExecutePayment.html
 * 
 * @class
 * @see  http://paypal.github.io/PayPal-PHP-SDK/docs/class-PayPal.Api.Payment.html
 * 
 * @version  1.2.4
 */

class PaymentLog extends KActiveRecord {

	/**
	 * The followings are the available columns in table 'payment'
	 * 
	 * Table Primary Key
	 * @var integer $paymentid
	 */
		public $paymentid;

	/**
	 * Other Atributes
	 * @see  PayPal\Api\Payment
	 */
	
	public $paypalid;
	public $state;


    /**
     * @see  PayPal\Api\Payer 
     *       PayPal\Api\PayerInfo
     */
    public $payment_method;
    public $amount;
	public $hash;

    public $usr_status;

    // PayerInfo
    public $usr_email;
    public $usr_id;
    public $usr_first_name;
    public $usr_last_name;

    public $createdate;


    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }    
    
    public function getDbConnection() {

        return self::getAdDbConnection();
    }

	public function rules() {
		return array(
			array('paypalid, state, usr_id, amount, hash', 'required'),
			array('payment_method, amount, hash, paypalid, state, usr_status, usr_email, usr_id, usr_first_name, usr_last_name', 'safe'),

			array(
				'createdate', 
				'default', 
				'value' => date('Y-m-d H:i:s', time() ), 
				'setOnEmpty' => true 
			),
		);
	}

	public function tableName() {
		return 'payments';
	}

	public function primaryKey() {
	    return 'paymentid';
	}
}
