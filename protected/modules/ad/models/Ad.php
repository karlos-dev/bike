<?php

/**
 * This is the model class for table "ads".
 *
 * The followings are the available columns in table 'ads':
 * @property string $adid
 * @property integer $uid
 * @property string $groupid
 * @property string $domainid
 * @property integer $adclass
 * @property string $class
 * @property string $title
 * @property string $hash
 * @property string $uri
 * @property string $banner
 * @property string $bgimage
 * @property string $htmlad
 * @property string $cssad
 * @property string $address
 * @property string $zip
 * @property string $tags
 * @property string $services
 * @property integer $width
 * @property integer $height
 * @property string $url
 * @property string $textad
 * @property string $textadurl
 * @property string $trackingurl
 * @property string $logfile
 * @property string $logtype
 * @property string $hits
 * @property string $clicks
 * @property string $priority
 * @property string $domains
 * @property string $createdate
 * @property string $modifydate
 * @property string $expiredate
 * @property string $lastdisplay
 * @property string $statsreset
 * @property string $notes
 * @property string $active
 */
class Ad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ads';
	}

	/**
	 * @return string the associated model Ad primary key column name
	 */
	public function primaryKey() {
	    return 'adid';
	}

	/**
	 * getUrl
	 * Formats current ad name in form of ad url
	 * @return string
	 */
	public function getUrl(){
		return Controller::stripAccents( str_replace(" ", "-", strtolower( $this->title ) ) );
	}

	/**
	 * Generates a json file with the data structure of this model
	 * @return String|null The path to the generated file or null if error
	 */
	public function generateAdFile() {
		$json = json_encode($this->attributes);
		$fname =  Yii::app()->basePath . DIRECTORY_SEPARATOR.'..'. "/data/"."ad{$this->adid}.json";
		if( file_put_contents( $fname, $json ) !== false ) {
			return $fname;
		}
		return null;
	}

	/**
	 * Get this Ad database id
	 * @return Integer The column ad primary key field
	 */
	public function getAdid() {
		return $this->adid;
	}

	/**
	 * Set the domain id of this Ad
	 * @param Integer $did The database primary key of table domains
	 */
	public function setDomainid( $did ) {
		$this->domainid=$did;
	}

	/**
	 * Set Ad as active
	 * @param  Mysql DateTime $expire The expiration date to be set
	 */
	public function activateAd( $expire=null ) {
		if( $expire )
			$this->expiredate = $expire;

		$this->active = 'Y';
		$this->save(false);
	}


	public function beforeSave(){
	   if(parent::beforeSave()) {
	   
	   		if( is_string($this->tags) ) {
	         // for example
	   		} else 
	   			if( is_array($this->tags) )
	        		$this->tags = ( !empty($this->tags) ) ? implode(",", $this->tags ) : "";

	        if( is_string($this->services) ) {
	         // for example
	   		} else 
	        	if( is_array($this->services) )
	        		$this->services = ( !empty($this->services) ) ? implode(",", $this->services ) : "";

	        return true;
	   }
	   return false;
	}


	public function defaultScope() {

		/**
		 * @abstract
		 * 
		 * Be very careful with the order, on AUTH isAdmin() might give an exception
		 * 
		 */
        if( array_key_exists('PHP_AUTH_USER', $_SERVER) || UserModule::isAdmin() ) {
			$dataProvider=new CActiveDataProvider('Payment');

        } elseif ( !Yii::app()->user->isGuest ) {
	        return array(
	            'condition'=>'uid='.Yii::app()->user->id,
	        );    
	    } 

	    return array();
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		/**
		 * @abstract 
		 * 
		 *  As mentioned in: 
		 *  http://www.yiiframework.com/forum/index.php/topic/48742-default-value-validator-problem/
		 * 
		 *  Rules are applied as they come, so first rule, validates the existing model and does not wait for all the rules 
		 *  to be completed and rematch the object afterwards (logical)
		 * 
		 * 	BE CAREFUL when defining rules related each other. i.e.
		 * 
		 * 	Required --> before default set :)
		 * 
		 */
		return array(
			array('uid, adclass, width, height', 'numerical', 'integerOnly'=>true),
			array('groupid, domainid', 'length', 'max'=>6),
			array('class', 'length', 'max'=>16),
			array('title', 'length', 'max'=>192),
			array('hash', 'length', 'max'=>20),
			array('uri', 'length', 'max'=>256),
			array('banner, bgimage, url, textad, textadurl, trackingurl, logfile', 'length', 'max'=>255),
			array('htmlad, cssad', 'length', 'max'=>4096),
			array('address', 'length', 'max'=>128),
			array('zip', 'length', 'max'=>5),
			array('hits, clicks', 'length', 'max'=>8),
			array('priority, active', 'length', 'max'=>1),
			array('tags, services, logtype, domains, notes', 'safe'),
			
			// defauls
			array('banner', 				 'default', 'value' => "", 							 'setOnEmpty' => true ),
			array('createdate',  'default', 'value' => date('Y-m-d H:i:s', time() ), 'setOnEmpty' => false ),
			array('modifydate',  'default', 'value' => date('Y-m-d H:i:s', time() ), 'setOnEmpty' => true ),
            array('lastdisplay, statsreset', 'default', 'value' => '1000-01-01 00:00:01',		 'setOnEmpty' => true, 'on' => 'insert'),
            array('expiredate', 			 'default', 'value' => date('Y-m-d H:i:s', time() + 60*20 ), 'setOnEmpty' => true, 'on' => 'insert'),
            array('active', 			 	 'default', 'value' => 'N', 								 'setOnEmpty' => true, 'on' => 'insert'),
            array('logfile', 			 	 'default', 'value' => 'my-default-log-file.log', 			 'setOnEmpty' => true, 'on' => 'insert'),
            
            array('hits', 	'default', 'value' => 0, 		 					 'setOnEmpty' => false, 'on' => 'insert'),
            array('clicks', 'default', 'value' => 0,		 					 'setOnEmpty' => false, 'on' => 'insert'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('adid, uid, groupid, domainid, adclass, class, title, hash, uri, banner, bgimage, htmlad, cssad, address, zip, tags, services, width, height, url, textad, textadurl, trackingurl, logfile, logtype, hits, clicks, priority, domains, createdate, modifydate, expiredate, lastdisplay, statsreset, notes, active', 'safe', 'on'=>'search'),

			array('hash, createdate, modifydate, expiredate, lastdisplay, statsreset', 'required'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'adid' => 'Adid',
			'uid' => 'Uid',
			'groupid' => 'Groupid',
			'domainid' => 'Domainid',
			'adclass' => 'Adclass',
			'class' => 'Class',
			'title' => 'Title',
			'hash' => 'Hash',
			'uri' => 'Uri',
			'banner' => 'Banner',
			'bgimage' => 'Bgimage',
			'htmlad' => 'Htmlad',
			'cssad' => 'Cssad',
			'address' => 'Address',
			'zip' => 'Zip',
			'tags' => 'Tags',
			'services' => 'Services',
			'width' => 'Width',
			'height' => 'Height',
			'url' => 'Link',
			'textad' => 'Textad',
			'textadurl' => 'Textadurl',
			'trackingurl' => 'Trackingurl',
			'logfile' => 'Logfile',
			'logtype' => 'Logtype',
			'hits' => 'Hits',
			'clicks' => 'Clicks',
			'priority' => 'Priority',
			'domains' => 'Domains',
			'createdate' => 'Created on',
			'modifydate' => 'Modifydate',
			'expiredate' => 'Expires',
			'lastdisplay' => 'Lastdisplay',
			'statsreset' => 'Statsreset',
			'notes' => 'Notes',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;



		$criteria->compare('adid',$this->adid,true);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('groupid',$this->groupid,true);
		$criteria->compare('domainid',$this->domainid,true);
		$criteria->compare('adclass',$this->adclass);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('hash',$this->hash,true);
		$criteria->compare('uri',$this->uri,true);
		$criteria->compare('banner',$this->banner,true);
		$criteria->compare('bgimage',$this->bgimage,true);
		$criteria->compare('htmlad',$this->htmlad,true);
		$criteria->compare('cssad',$this->cssad,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('services',$this->services,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('textad',$this->textad,true);
		$criteria->compare('textadurl',$this->textadurl,true);
		$criteria->compare('trackingurl',$this->trackingurl,true);
		$criteria->compare('logfile',$this->logfile,true);
		$criteria->compare('logtype',$this->logtype,true);
		$criteria->compare('hits',$this->hits,true);
		$criteria->compare('clicks',$this->clicks,true);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('domains',$this->domains,true);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('modifydate',$this->modifydate,true);
		$criteria->compare('expiredate',$this->expiredate,true);
		$criteria->compare('lastdisplay',$this->lastdisplay,true);
		$criteria->compare('statsreset',$this->statsreset,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('active',$this->active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysql;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
