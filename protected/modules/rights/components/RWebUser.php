<?php
/**
* Rights web user class file.
*
* @author Christoffer Niska <cniska@live.com>
* @copyright Copyright &copy; 2010 Christoffer Niska
* @since 0.5
* 
* @modified 
*  
* @version  bike 1.2.1
*/
class RWebUser extends CWebUser
{
	private static $userSession;
	private static $keys;

	public function init(){

		parent::init();
		if( !empty( Yii::app()->session->toArray() ) && !isset( self::$keys ) ) {

			self::$userSession=Yii::app()->session->toArray();
			self::$keys=array_keys( Yii::app()->session->toArray() );
		}
	}

	/**
	* Actions to be taken after logging in.
	* Overloads the parent method in order to mark superusers.
	* @param boolean $fromCookie whether the login is based on cookie.
	*/
	public function afterLogin($fromCookie)
	{
		
		$_sec= Yii::app()->getSecurityManager();
		
		self::$userSession=Yii::app()->session->toArray();
		self::$keys=array_keys( Yii::app()->session->toArray() );

		$encSession = $_sec->encrypt( json_encode( self::$userSession ) );
		Yii::app()->session['enc'] = $encSession;

		parent::afterLogin($fromCookie);

		// Mark the user as a superuser if necessary.
		if( Rights::getAuthorizer()->isSuperuser($this->getId())===true )
			$this->isSuperuser = true;

		return true;
	}

	/**
	 * This method is invoked right after a user is logged out.
	 * You may override this method to do some extra cleanup work for the user.
	 * 
	 * @since bike 1.2.1 empty
	 */
	protected function afterLogout()
	{

	}

	/**
	 * @author Karlos Álvarez <karlos.alvan@gmail.com>
	 * 
	 * @override 
	 * @since bike 1.2.1
	 * @see https://github.com/yiisoft/yii/blob/1.1.17/framework/web/auth/CWebUser.php#L572
	 * 
	 * @param type $key 
	 * @param type|null $defaultValue 
	 * @return mixed
	 */
	public function getState($key, $defaultValue=null)
	{
		$_encKey= $this->getStateKeyPrefix().$key;
		// print_r( self::$keys ); die;
		if( isset(self::$keys) && in_array($_encKey, self::$keys) ) {
			// Yii::log("Using RWebUser getState(key $key) override on encoded $_encKey", "audit");
			return isset(self::$userSession[$_encKey]) ? self::$userSession[$_encKey] : $defaultValue;
		}

		//Yii::log("Using RWebUser getState(key $key) override [$_encKey]", "audit");
		$key=$this->getStateKeyPrefix().$key;
		return isset($_SESSION[$key]) ? $_SESSION[$key] : $defaultValue;
	}

	/**
	 * @author Karlos Álvarez <karlos.alvan@gmail.com>
	 * @override 
	 * @since bike 1.2.1
	 * @see https://github.com/yiisoft/yii/blob/1.1.17/framework/web/auth/CWebUser.php#L593 
	 * 
	 * @param type $key 
	 * @param type $value 
	 * @param type|null $defaultValue 
	 * @return mixed
	 */
	public function setState($key,$value,$defaultValue=null)
	{

		if ( !is_array($value) ) {
			$_encKey= $this->getStateKeyPrefix().$key;
			if( isset(self::$keys) && in_array($_encKey, self::$keys) ) {
				Yii::log("Using RWebUser setState(key $key, value $value) override on encoded $_encKey", "audit");
				$key=$this->getStateKeyPrefix().$key;

				if($value===$defaultValue)
					unset(self::$userSession[$key]);
				else
					self::$userSession[$key]=$value;
			}

			//Yii::log("Using RWebUser setState(key $key, value $value) override", "audit");
		} else {
			$json= json_encode($value);
			//Yii::log("Using RWebUser setState(key $key, value $json) override", "audit");
		}

		if( strpos($key, "audit" ) !== FALSE ) {
			// No audit stored in session
		} else {

			$key=$this->getStateKeyPrefix().$key;
			if($value===$defaultValue)
				unset($_SESSION[$key]);
			else
				$_SESSION[$key]=$value;
		}
		
	}

	/*
	public function getId(){
		Yii::log("Using RWebUser getId() override","info");

		$key=$this->getStateKeyPrefix().'__id';

		if( isset( self::$userSession ) ) {
			
			return self::$userSession[$key];
		}

		$_s = Yii::app()->session['enc'];
		$_security = Yii::app()->getSecurityManager();
		if( $_s ) {
		  	$_decstring = $_security->decrypt( $_s );
		  	$_decSession = json_decode($_decstring);
		  	return $_decSession[ $key ];

		} else return $this->getState('__id') ? $this->getState('__id') : 0;
	}
	*/

	/*public function getName(){

	}*/

	/**
	* Performs access check for this user.
	* Overloads the parent method in order to allow superusers access implicitly.
	* @param string $operation the name of the operation that need access check.
	* @param array $params name-value pairs that would be passed to business rules associated
	* with the tasks and roles assigned to the user.
	* @param boolean $allowCaching whether to allow caching the result of access checki.
	* This parameter has been available since version 1.0.5. When this parameter
	* is true (default), if the access check of an operation was performed before,
	* its result will be directly returned when calling this method to check the same operation.
	* If this parameter is false, this method will always call {@link CAuthManager::checkAccess}
	* to obtain the up-to-date access result. Note that this caching is effective
	* only within the same request.
	* @return boolean whether the operations can be performed by this user.
	*/
	public function checkAccess($operation, $params=array(), $allowCaching=true)
	{
		// Allow superusers access implicitly and do CWebUser::checkAccess for others.
		return $this->isSuperuser===true ? true : parent::checkAccess($operation, $params, $allowCaching);
	}

	/**
	* @param boolean $value whether the user is a superuser.
	*/
	public function setIsSuperuser($value)
	{
		$this->setState('Rights_isSuperuser', $value);
	}

	/**
	* @return boolean whether the user is a superuser.
	*/
	public function getIsSuperuser()
	{
		return $this->getState('Rights_isSuperuser');
	}
	
	/**
	 * @param array $value return url.
	 */
	public function setRightsReturnUrl($value)
	{
		$this->setState('Rights_returnUrl', $value);
	}
	
	/**
	 * Returns the URL that the user should be redirected to 
	 * after updating an authorization item.
	 * @param string $defaultUrl the default return URL in case it was not set previously. If this is null,
	 * the application entry URL will be considered as the default return URL.
	 * @return string the URL that the user should be redirected to 
	 * after updating an authorization item.
	 */
	public function getRightsReturnUrl($defaultUrl=null)
	{
		if( ($returnUrl = $this->getState('Rights_returnUrl'))!==null )
			$this->returnUrl = null;
		
		return $returnUrl!==null ? CHtml::normalizeUrl($returnUrl) : CHtml::normalizeUrl($defaultUrl);
	}
}
