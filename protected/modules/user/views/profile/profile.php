<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
	UserModule::t("Profile"),
);
$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'))
		:array()),
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('List User'), 'url'=>array('/user'))
		:array()),
    array('label'=>UserModule::t('Edit'), 'url'=>array('edit')),
    array('label'=>UserModule::t('Change password'), 'url'=>array('changepassword')),
    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout')),
);
?><h1><?php echo UserModule::t('Your profile'); ?></h1>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
	<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>
<style type="text/css">
	.dataGrid code 	{ font-size: 0.7rem; }
	.dataGrid hr 	{ margin: 2px 0px; }
</style>
<table class="dataGrid">
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('username')); ?></th>
	    <td><?php echo CHtml::encode($model->username); ?></td>
	</tr>
	<?php 

		/** 
		 * @abstract Retrieve db size data in admin user profile
		 *           Only if KDbConnection is used as a CDbConnection wrapper
		 */

		if( UserModule::isAdmin() && is_a( Yii::app()->mysql, 'KDbConnection') && is_a( Yii::app()->db, 'KDbConnection') ) { 

			$adData = Yii::app()->mysql->getSize();
			$userData = Yii::app()->db->getSize();
			$auditData = Yii::app()->db->getSize( Yii::app()->basePath."/runtime/audit-1.1.10.db" );

			$arr= explode("..", $userData['file']);
			$str = implode(" ", $arr);

	?>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<th class="label">db ads </th>
	    	<?php echo "<td>{$adData['kb']} <code>(Kb)</code></td>";//echo "<td>{$ca['mb']}</td><td>{$ca['kb']}</td>"; ?>
		</tr>
		<tr>
			<th class="label">Ads file</th>
	    	<?php echo "<td>{$adData['file']}";//echo "<td>{$ca['mb']}</td><td>{$ca['kb']}</td>"; ?>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<th class="label">db users </th>
	    	<?php echo "<td>{$userData['kb']} <code>(Kb)</code></td>"; ?>
		</tr>
		<tr>
			<th class="label">User file</th>
	    	<?php echo "<td>{$str}</td>";//echo "<td>{$ca['mb']}</td><td>{$ca['kb']}</td>"; ?>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<th class="label">db audit</th>
	    	<?php echo "<td>{$auditData['kb']} <code>(Kb)</code></td>"; ?>
		</tr>
		<tr>
			<th class="label">Audit file</th>
	    	<?php echo "<td>{$auditData['file']}</td>";//echo "<td>{$ca['mb']}</td><td>{$ca['kb']}</td>"; ?>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
	<?php } ?>
	
	<?php 
		$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
		if ($profileFields) {
			foreach($profileFields as $field) {
				//echo "<pre>"; print_r($profile); die();
			?>
	<tr>
		<th class="label"><?php echo CHtml::encode(UserModule::t($field->title)); ?></th>
    	<td><?php echo (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?></td>
	</tr>
			<?php
			}//$profile->getAttribute($field->varname)
		}
	?>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></th>
    	<td><?php echo CHtml::encode($model->email); ?></td>
	</tr>
	<tr><td colspan="2"><hr></td></tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('create_at')); ?></th>
    	<td><?php echo $model->create_at; ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('lastvisit_at')); ?></th>
    	<td><?php echo $model->lastvisit_at; ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('status')); ?></th>
    	<td><?php echo CHtml::encode(User::itemAlias("UserStatus",$model->status)); ?></td>
	</tr>
</table>
