<?php

class DefaultController extends Controller
{
	
	public $layout='//layouts/column2';
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$id = Yii::app()->user->id;

		/**
		 * @abstract 
		 * 
		 * 	Security
		 *  Only show current user if the user is not admin
		 */
		if( UserModule::isAdmin() ) {
			$criteria = array(
		        'condition'=>'status>'.User::STATUS_BANNED,
		    );
		} else {
			$criteria = array(
		        'condition'=>'status>'.User::STATUS_BANNED.' and id='.$id,
		        //'condition'=>'superuser=1',
		    );
		}

		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Yii::app()->controller->module->user_page_size,
			),
		));

		$this->render('/user/index',array(
			'dataProvider'=>$dataProvider,
		));
	}

}