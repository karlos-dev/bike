<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';
	public $layout = '//layouts/adlayout';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{

		if (Yii::app()->user->isGuest) {

			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];

				// validate user input and redirect to previous page if valid
				if( $model->validate() ) {

					$this->lastViset();

					foreach ( array_keys( Yii::app()->session->toArray() ) as $key ) {
						$_encKey= Yii::app()->user->getStateKeyPrefix().$key;
						unset( Yii::app()->session[ $_encKey ] );
					}

					Yii::log(" --> Removing SessionData LoginController actionLogin()", "audit");


					if ( Yii::app()->user->returnUrl == '/' )
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
			}
			// display the login form
			$this->render('/user/login', array('model'=>$model));
			
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		//print_r($lastVisit); die;
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}

}