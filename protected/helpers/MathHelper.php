<?php

class MathHelper {
	public static function gcd($nums) {

	    function fgcd ($a, $b) {
		    return $b > .01 ? fgcd($b, fmod($a, $b)) : $a; // using fmod
		}

		return array_reduce($nums, 'fgcd');
	}	
}

?>