<?php

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property string $paymentid
 * @property string $paypalid
 * @property string $state
 * @property string $payment_method
 * @property integer $amount
 * @property string $hash
 * @property string $usr_status
 * @property string $usr_email
 * @property string $usr_id
 * @property string $usr_first_name
 * @property string $usr_last_name
 * @property string $createdate
 */
class Payment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments';
	}

	public function scopes()
    {
    	$user = UserModule::user(Yii::app()->user->id);
		$hashStr = substr( UserModule::encrypting($user->email), 0, 10);

        return array(
            'current'=>array(
                'condition'=>"hash='".$hashStr."'",
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paypalid, amount, hash, usr_id', 'required'),
			array('amount', 'numerical', 'integerOnly'=>true),
			array('paypalid, usr_id', 'length', 'max'=>64),
			array('state, payment_method, usr_status', 'length', 'max'=>32),
			array('hash', 'length', 'max'=>20),
			array('usr_email, usr_first_name, usr_last_name', 'length', 'max'=>128),
			array('createdate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('paymentid, paypalid, state, payment_method, amount, hash, usr_status, usr_email, usr_id, usr_first_name, usr_last_name, createdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'paymentid' => 'Paymentid',
			'paypalid' => 'Paypalid',
			'state' => 'State',
			'payment_method' => 'Payment Method',
			'amount' => 'Amount',
			'hash' => 'Hash',
			'usr_status' => 'User Status',
			'usr_email' => 'Email',
			'usr_id' => 'Paypal Id',
			'usr_first_name' => 'First Name',
			'usr_last_name' => 'Last Name',
			'createdate' => 'Created on',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('paymentid',$this->paymentid,true);
		$criteria->compare('paypalid',$this->paypalid,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('payment_method',$this->payment_method,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('hash',$this->hash,true);
		$criteria->compare('usr_status',$this->usr_status,true);
		$criteria->compare('usr_email',$this->usr_email,true);
		$criteria->compare('usr_id',$this->usr_id,true);
		$criteria->compare('usr_first_name',$this->usr_first_name,true);
		$criteria->compare('usr_last_name',$this->usr_last_name,true);
		$criteria->compare('createdate',$this->createdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array( 
				'pageSize'=> 20
			)
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysql;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
