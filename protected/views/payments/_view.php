<?php
/* @var $this PaymentsController */
/* @var $data Payment */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymentid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->paymentid), array('view', 'id'=>$data->paymentid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_method')); ?>:</b>
	<?php echo CHtml::encode($data->payment_method); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amount')); ?>:</b>
	<?php echo CHtml::encode($data->amount); ?>
	<br />

	<?php /*

	<b><?php echo CHtml::encode($data->getAttributeLabel('hash')); ?>:</b>
	<?php echo CHtml::encode($data->hash); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paypalid')); ?>:</b>
	<?php echo CHtml::encode($data->paypalid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_status')); ?>:</b>
	<?php echo CHtml::encode($data->usr_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_email')); ?>:</b>
	<?php echo CHtml::encode($data->usr_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_id')); ?>:</b>
	<?php echo CHtml::encode($data->usr_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_first_name')); ?>:</b>
	<?php echo CHtml::encode($data->usr_first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usr_last_name')); ?>:</b>
	<?php echo CHtml::encode($data->usr_last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

	*/ ?>

</div>