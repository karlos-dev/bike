<?php
/* @var $this PaymentsController */
/* @var $model Payment */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'paypalid'); ?>
		<?php echo $form->textField($model,'paypalid',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'paypalid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payment_method'); ?>
		<?php echo $form->textField($model,'payment_method',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'payment_method'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
		<?php echo $form->textField($model,'amount'); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hash'); ?>
		<?php echo $form->textField($model,'hash',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'hash'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usr_status'); ?>
		<?php echo $form->textField($model,'usr_status',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'usr_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usr_email'); ?>
		<?php echo $form->textField($model,'usr_email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'usr_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usr_id'); ?>
		<?php echo $form->textField($model,'usr_id',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'usr_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usr_first_name'); ?>
		<?php echo $form->textField($model,'usr_first_name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'usr_first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usr_last_name'); ?>
		<?php echo $form->textField($model,'usr_last_name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'usr_last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
		<?php echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->