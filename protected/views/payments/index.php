<?php
/* @var $this PaymentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payments',
);

$this->menu=array(
	array('label'=>'Create Payment', 'url'=>array('create')),
	array('label'=>'Manage Payment', 'url'=>array('admin')),
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pay-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<h3>Payments</h3>

<?php 

	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pay-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		
		//'hash',
		array(
			'name' => 'usr_first_name',
			'type' => 'raw',
			'value' => '$data->usr_first_name',
			'htmlOptions' => array(
		        'style' => 'text-align: left;',
		 	),
		), 
		array(
			'name' => 'usr_last_name',
			'type' => 'raw',
			'value' => '$data->usr_last_name',
			'htmlOptions' => array(
		        'style' => 'text-align: left;',
		    ),
		),
		'state',
		/*
		array(
            'name'=>'active',
            'filter'=>CHtml::dropDownList('Ad[active]', $model->active, array( 'Y'=>'Yes', 'N'=>'No' ), array('empty'=>'All') ),
            'value'=>'( $data->active == "Y") ? "Yes" : "No"',
            'htmlOptions' => array(
		        'style' => 'width: 40px; text-align: center;',
		    ),
        ),
        */

		'payment_method',
		array(
            'name'=>'createdate',
            //'filter'=>CHtml::dropDownList('Ad[class]', $model->class, $this->sizeArr, array('empty'=>'All') ),
            'value'=>' (new DateTime($data->createdate))->format("j F Y") ',
            'htmlOptions' => array(
		        'style' => 'width: 70px; text-align: left; font-size:11px;',
		    ),
        ),
		'amount',
		array(
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode("view"), array("view", "id"=>$data->paymentid))',
			'htmlOptions' => array(
		        'style' => 'text-align: center;',
		 	),
		),
	),
)); ?>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */?>
