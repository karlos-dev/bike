<?php
/* @var $this PaymentsController */
/* @var $model Payment */

$this->breadcrumbs=array(
	'Payments'=>array('index'),
	$model->paymentid,
);

$this->menu=array(
	array('label'=>'List Payment', 'url'=>array('index')),
	array('label'=>'Create Payment', 'url'=>array('create')),
	array('label'=>'Update Payment', 'url'=>array('update', 'id'=>$model->paymentid)),
	array('label'=>'Delete Payment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->paymentid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Payment', 'url'=>array('admin')),
);
?>

<h3>View Payment #<?php echo $model->paymentid; ?></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'paymentid',
		'paypalid',
		'state',
		'payment_method',
		'amount',
		'hash',
		'usr_status',
		'usr_email',
		'usr_id',
		'usr_first_name',
		'usr_last_name',
		'createdate',
	),
)); ?>
