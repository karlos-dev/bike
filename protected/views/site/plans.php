<?php
$name='bikerent';
?>
<div id="wrap">
  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <?php echo "<pre>"; /* echo "&#8413; &#596; &#x27f2;";*/ print_r( Yii::app()->session->toArray() ); /*echo session_id();*/ echo "</pre>"; ?>
  <?php echo $this->renderPartial('header'); ?>
  <!-- end header -->
  <div id="content">
    <div class="container clearfix ">
      <div id="container" class="steps">
        <div class="col1-1 home element">
          <h4><?php echo Yii::t('app','Choose Ad size'); ?>:</h4>
          <table class="plans">
              <tr class="he"><td class="norm"><?php echo Yii::t('app',''); ?></td> <td class="norm"><?php echo Yii::t('app','Size'); ?></td><td><?php echo Yii::t('app','Text'); ?></td><td><?php echo Yii::t('app','Link'); ?></td><td><?php echo Yii::t('app','Background Image'); ?></td><td><?php echo Yii::t('app','Image'); ?></td><td><?php echo Yii::t('app','Ad web'); ?></td><td><?php echo Yii::t('app','Map'); ?></td><td><?php echo Yii::t('app','Analitics'); ?></td> </tr> 
              <tr class=""><td cellspan="8" ></td></tr>
              <tr class="r" data-sz="large"><td class="norm"><?php echo Yii::t('app','Large'); ?></td> <td class="norm">540x220</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td> </tr> 
              <tr class=""><td cellspan="8" ></td></tr>
              <tr class="r" data-sz="medium"><td class="norm"><?php echo Yii::t('app','Medium'); ?></td> <td class="norm">340x140</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td> </tr> 
              <tr class=""><td cellspan="8" ></td></tr>
              <tr class="r" data-sz="regular"><td class="norm"><?php echo Yii::t('app','Regular'); ?></td> <td class="norm">270x120</td><td>Yes</td><td>Yes</td><td>Yes</td><td>Yes</td><td>No</td><td>Yes</td><td>Yes</td> </tr> 
              <tr class=""><td cellspan="8" ></td></tr>
              <tr class="r" data-sz="small"><td class="norm"><?php echo Yii::t('app','Small'); ?></td> <td class="norm">150x90</td><td>Yes</td><td>Yes</td><td>Yes</td><td>No</td><td>No</td><td>No</td><td>Yes</td> </tr> 
              <tr class=""><td cellspan="8" ></td></tr>
              <tr class="r" data-sz="tiny"><td class="norm"><?php echo Yii::t('app','tiny'); ?></td> <td class="norm">80x50</td><td>Yes</td><td>Yes</td><td>No</td><td>No</td><td>No</td><td>No</td><td>Yes</td> </tr> 
          </table>
        </div>
        <div class="col1-1 home element">
          <div class="box">
            <div class="scr">
              <div class="e large"></div>
              <div class="e medium"></div>
              <div class="e regular"></div>
              <div class="e small"></div>
              <div class="e tiny"></div>
            </div>
          </div>
          <div class="opt-box" data-op="large">
            <ul class="">
              <li><h4>Large Ad Features</h4></li>
              <li class="opt">
                <?php echo Yii::t('app','<p>Increase your business </p><h3>SEO visibility</h3>'); ?>
                <p><code><?php echo Yii::t('app','the_name_you_want'); ?>.bikerent.barcelona</code></p>
              </li>
              <li class="opt"><p>Your <b>business will appear in the map search</b></p></li>
              <li class="opt"><p>Include Google Maps in your Ad</p></li>
              <li class="opt"><p>1 hour/day visualization</p></li>
              <li class="opt"><p>Maximun 5 Ads per business</p></li>
              <li class="opt"><p><b>16€</b>/month </p><small>(150€/year)</small></li>
            </ul>
          </div>
          <div class="opt-box" data-op="medium">
            <ul class="">
              <li><h4>Medium Ad Features</h4></li>
              <li class="opt">
                <?php echo Yii::t('app','<p>Increase your business </p><h3>SEO visibility</h3>'); ?>
                <p><code><?php echo Yii::t('app','the_name_you_want'); ?>.bikerent.barcelona</code></p>
              </li>
              <li class="opt"><p>Your <b>business will appear in the map search</b></p></li>
              <li class="opt"><p>Include Google Maps in your Ad</p></li>
              <li class="opt"><p>Maximun 5 Ads per business</p></li>
              <li class="opt"><p><b>12€</b>/month </p><small>(130€/year)</small></li>
            </ul>
          </div>
          <div class="opt-box" data-op="regular">
            <ul class="">
              <li><h4>Regular Ad Features</h4></li>
              <li class="opt">
                <?php echo Yii::t('app','<p>Increase your business </p><h3>SEO visibility</h3>'); ?>
                <p><code><?php echo Yii::t('app','the_name_you_want'); ?>.bikerent.barcelona</code></p>
              </li>
              <li class="opt"><p>Your <b>business will appear in the map search</b></p></li>
              <li class="opt"><p>Include google maps in your Ad</p></li>
              <li class="opt"><p>Maximun 5 Ads per business</p></li>
              <li class="opt"><p><b>8€</b>/month </p><small>(75€/year)</small></li>
            </ul>
          </div>
          <div class="opt-box" data-op="small">
            <ul class="">
              <li><h4>Small Ad Features</h4></li>
              <li class="opt">
                <p>Increase your business </p><h3>SEO visibility</h3>
                <p><code>www.bikerent.barcelona/<?php echo Yii::t('app','the-name-you-want'); ?></code></p>
              </li>
              <li class="opt"><p>More results of your business in <b>Google search</b></p></li>
              <li class="opt"><p>Incredible cheap! </p><small>(0.033€/day)</small></li>
              <li class="opt"><p>Maximun 20 Ads per business</p></li>
              <li class="opt"><p><b>2€</b>/month </p><small>(20€/year)</small></li>
            </ul>
          </div>
           <div class="opt-box" data-op="tiny">
            <ul class="">
              <li><h4>tiny Ad Features</h4></li>
              <li class="opt">
                <p>Increase your business </p><h3>SEO visibility</h3>
                <p><code>www.bikerent.barcelona/<?php echo Yii::t('app','the-name-you-want'); ?></code></p>
              </li>
              <li class="opt"><p>More results of your business in <b>Google search</b></p></li>
              <li class="opt"><p>Insane price! </p><small>(0.033€/day)</small></li>
              <li class="opt"><p>Maximun 30 Ads per business</p></li>
              <li class="opt"><p><b>1€</b>/month </p><small>(10€/year)</small></li>
            </ul>
          </div>
          
        </div>
      </div>
      <div class="opt-actions">
        <a href="/ads"><button>Create Ad</button></a>
      </div>
    </div>
  </div>
  <!-- end content -->
</div>
<!-- end wrap -->
<?php echo $this->renderPartial('footer'); ?>
<!-- BACK TO TOP BUTTON -->
<div id="backtotop">
  <ul>
    <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
  </ul>
</div>

<!--<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
<script src="js/jquery-1.12.3.js" type="text/javascript"></script>
<script src="js/jquery.isotope.min.js" type="text/javascript"></script>
<script src="js/jquery.isotope.perfectmasonry.js"></script>

<script src="js/scrollup.js" type="text/javascript"></script>
<script src="js/preloader.js" type="text/javascript"></script>
<script src="js/navi-slidedown.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
