<footer>
    <div class="container">
      <ul>
        <li><h3 class="pink">BikeRent</h3></li>
        <li><a>Increase your business visibility</a></li>
        <li><a>Link to your website</a></li>
        <li><a>Search Engine results</a></li>
        <li><a>Powerful domain name</a></li>
      </ul>
      <ul>
        <li><h3 class="red">Features</h3></li>
        <li><a>Ad design</a></li>
        <li><a>Ad Website</a></li>
        <li><a>Map Results</a></li>
        <li><a>Ad Statistics</a></li>
        <li><a>Ultra Cheap</a></li>
      </ul>
      <ul>
        <li><h3 class="orange">Ad Groups</h3></li>
        <li><a>Cheapie</a></li>
        <li><a>Classic</a></li>
        <li><a>Electric</a></li>
        <li><a>Fixie</a></li>
        <li><a>Tours</a></li>
      </ul>
      <ul>
        <li><h3 class="green">About</h3></li>
        <li><a>User Area</a></li>
        <li><a>FAQ</a></li>
        <li><a>Contact</a></li>
        <li><a>Newsletter</a></li>
      </ul>
    </div>
</footer>