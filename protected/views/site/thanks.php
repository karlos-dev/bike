<?php

// The name I want to show in the main h1 html tag
$name='bikerent';

// Provide sizes from server to client
$jsonObj = json_encode( $this->sizeArr );
$line = "<script type=\"text/javascript\">\n\n\tvar _sizes='$jsonObj';\n\n</script>\n";

// Ease current user access
$logeduser = UserModule::user( Yii::app()->user->id );

// Form security
$unique_salt=Yii::app()->user->getStateKeyPrefix();

?>
<div id="wrap">
  
  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <?php echo $this->renderPartial('header'); ?>
  <div id="content">
	  <div class="container clearfix">
	  	<div class="board wide">
	  		<div class="good">
	  			<h1>Thanks for your purchase!</h1>
	  		</div>
	  		<div class="list">
				<?php  			
				foreach ($res as $ad) {
					echo "<div class='status'>";
					echo "<div class='title'>" . $ad->title. "</div>";

					$time = strtotime($ad->expiredate) - time();
					$number = floor( $time/ (3600*24*30) );
					if( $number == 0 ) {
						$number = floor( $time/ (3600*24*7) );
						$text = ($number >= 2 ) ? ' weeks':' week';
						if( $number == 0 ) {
							$number = floor( $time/ (3600*24) );
							$text = ($number >= 2 ) ? ' days':' day';
							if( $number == 0 ) {
								$number = floor( $time/ (3600) );
								$text = ($number >= 2 ) ? ' hours':' hour';
							}
						}
					} else
						$text = ($number >= 2 ) ? ' months':' month';

					echo "<p>Links to  <a target='_blank' href=\"http://" . $ad->url . "\">" . $ad->url. "</a></p>";
					//echo "<p>Expire date " . $ad->expiredate. "</p>";
					echo "<p>Expires in " . $number. $text . " <span class='small'> (" . $ad->expiredate . ")</span></p>";
					echo "<p>Tagged as: " . str_replace(",", " ", $ad->tags ) . "</p>";
					echo "<p><a target='_blank' href=\"http://". $_SERVER['HTTP_HOST'] ."/" . $ad->uri . "\">view</a> | <a target='_blank' href=\"http://". $_SERVER['HTTP_HOST'] ."/user/profile\">". strtolower( UserModule::t('Your Profile', 'app') ) . "</a> | <a target='_blank' href=\"http://". $_SERVER['HTTP_HOST'] ."/ads\">".  strtolower( Yii::t('app', 'Create other Ad') ) . "</a></p>";

					// Need to scale image here :)
					
					echo $ad->htmlad;
					echo "</div>";	
				}
				?>
	  		</div>
	  	</div>
	  </div>
  </div>
  <?php echo $this->renderPartial('footer'); ?>
</div>


<div id="backtotop">
  <ul>
    <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
  </ul>
</div>

<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.9.2.min.js" type="text/javascript"></script>
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="js/forms/js/out/jquery.idealforms.js"></script>
<script src="js/preloader.js" type="text/javascript"></script>