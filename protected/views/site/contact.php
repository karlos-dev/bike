<?php
$name='bikerent';
?>
<link href="/css/contact.css" rel="stylesheet" type="text/css" media="screen" />

<div id="wrap">
  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <?php echo $this->renderPartial('header'); ?>

  <!-- end header -->
  <div id="content">
    <div class="container clearfix">
      <div id="container">
        
        <div class="col1-3 home element min-he">
          <div class="images"> 
            <!--
            <img src="" alt="" />
            <div id="map"></div>
            -->
          </div>
          <div class="white-bottom mine grey-area-last">
            <h2><?php echo Yii::t('app','Adverts'); ?></h2>
            <?php echo Yii::t('app','<p class="big inline"></p><h1>Adverts</h1><p class="big inline"> are categorized by size and feature</p>'); ?><br>
            <p class="big"><?php echo Yii::t('app','You select the time the advert will be displayed in the <a href="/" target="_blank">home</a> page'); ?></p>
            <?php echo Yii::t('app','<p class="big">Advert <strong>sizes</strong> you can use:</p>'); ?>
            <ul class="unordered-list floated clearfix">
              <li><?php echo Yii::t('app','Large'); ?> 540x320 pixel Max</li>
              <li><?php echo Yii::t('app','Medium'); ?> 340x180 pixel Max</li>
              <li><?php echo Yii::t('app','Regular'); ?> 270x160 pixel Max</li>
              <li><?php echo Yii::t('app','Small'); ?> 135x90 pixel Max</li>
              <li><?php echo Yii::t('app','tiny'); ?> 75.5x45 pixel Max</li>
            </ul>
            <p class="big bot"><a href="/plans" class="brlink"><?php echo Yii::t('app','Read more'); ?></a></p>
            <!--
            <div class="grey-area last smaller clearfix">
                <p class="small">
                <span class="alignleft"><a href=""><?php echo Yii::t('app','View Ads'); ?></a></span>
                <span class="alignright"><a href="/plans"><?php echo Yii::t('app','Plan Options'); ?></a></span>
                </p>
            </div>
            -->
          </div>
        </div>
        <div class="col1-3 home element min-he">
          <div class="images"> 
            <!--
            <img src="" alt="" />
            <div id="map"></div>
            -->
          </div>
          <div class="white-bottom mine grey-area-last right">
            <h2><?php echo Yii::t('app','Features'); ?></h2>
            <?php echo Yii::t('app','<p class="big inline">Track your </p><h1>bike rent</h1><p class="big inline"> adverts, linked in a powerful domain name: <span class="_link">bikerent.barcelona</span></p><br><p class="big"></p>'); ?>
            <?php echo Yii::t('app','<p class="big inline">Clicks and advert </p><h3>visualization statistics</h3><br><p class="big"></p>'); ?>
            <p class="big inline"><strong><?php echo Yii::t('app','Easy'); ?></strong> </p><h3><?php echo Yii::t('app','Ad management'); ?></h3><p class="big inline"> <?php echo Yii::t('app','in the user area. No configuration nighmares'); ?>.</p>
            <p class="big"><?php echo Yii::t('app','Flexible and simple payments with '); ?><a class="ppal" title="paypal" href="/payments"></a></p>
            <p class="big bot"><a href="/plans" class="brlink"><?php echo Yii::t('app','Read more'); ?></a></p>
            <!--
            <div class="grey-area last smaller clearfix">
                <p class="small">
                <span class="alignleft"><a href="#"><?php echo Yii::t('app','More Information'); ?></a></span>
                <span class="alignright"><a href="/payments"><?php echo Yii::t('app','Payments'); ?></a></span>
                </p>
            </div>
            -->
          </div>
        </div>
        <div class="col1-3 home element min-he">
          <div class="images"></div>
          <div class="white-bottom mine grey-area-last">
            <h2><?php echo Yii::t('app','Plans'); ?></h2>
            <p class="big inline"><?php echo Yii::t('app','The "Almost <h3>FREE</h3><p class="big inline"> ad solution", offers an interesting chance to get visits to your <i>site</i> or <i>business</i> for  <strong>1€/month</strong> <span class="small">( 0.0013€/hour )</span></p>'); ?> <p class="big"></p>
            <p class="big"><?php echo Yii::t('app','Every <span class="high">ad plan</span> can be <span class="high">Monthly</span> paid. <span class="_link">Flexibility</span> is a must for us.</p><p class="big"></p>'); ?>
            <p class="big inline"><?php echo Yii::t('app','<span class="high">Yearly</span> paid <span class="high">ad plans</span> are <h3>price reduced</h3></p>'); ?>
            <p class="big bot"><a href="/features" class="brlink"><?php echo Yii::t('app','Read more'); ?></a></p>
            
            <!--
            <div class="grey-area last smaller clearfix">
                <p class="small">
                <span class="alignleft"><a href="/"><?php echo Yii::t('app','Ad Products'); ?></a></span>
                <span class="alignright"><a href="/plans"><?php echo Yii::t('app','Plans'); ?></a></span>
                </p>
            </div>
            -->
          </div>
        </div>
        <div class="col2-3 home element">
          <div class="images mine"> <img src="/images/clouda.jpg" alt="" /> </div>
            <!--<div id="map"></div>-->
          <div class="white-bottom mine grey-area-last">

            <h2><?php echo Yii::t('app','Contact'); ?></h2>
            <?php echo Yii::t('app','<p class="big inline">Place your </p><h1>bike rent bussines</h1><p class="big inline"> in this site</p>'); ?>
            <p class="big"></p>
            <p class="big inline"></p><h1><?php echo Yii::t('app','Plans for Advertisement'); ?></h1>
            <?php echo Yii::t('app','<p class="big">Drop <strong>here</strong> a line, we will get back to you shortly.</p>'); ?>
            <ul class="unordered-list floated clearfix">
              <li>Weird IT<br />
                22@ Barcelona<br>08018 ES</li>
              <li>E-Mail: info@weird.is</li>
              <li><a href="http://weird.is">@weird</a></li>
            </ul>
            <div class="grey-area last smaller clearfix">
                <p class="small">
                <span class="alignleft"><a href="#"><?php echo Yii::t('app','Anticopyright Assets'); ?></a></span>
                <span class="alignright"><a href="#"><?php echo Yii::t('app','Disclaimer'); ?></a></span>
                </p>
            </div>
          </div>
        </div>
        <div class="element home clearfix col1-3 contact ">
          <div id="contact" class="mine">
            <div class="images mine"> <img src="/images/cloudb.jpg" alt="" /></div>
            <div id="message"></div>
            <form method="post" action="/contact" name="contactform" id="contactform" autocomplete="off">
              <fieldset>
              <div class="alignleft padding-right">
                <label for="name" accesskey="U"><span class="required"><?php echo Yii::t('app','Name'); ?></span></label>
                <input name="name" type="text" id="name" size="30" title="<?php echo Yii::t('app','Name'); ?> *" />
                <label for="email" accesskey="E"><span class="required"><?php echo Yii::t('app','Email'); ?></span></label>
                <input name="email" type="text" id="email" size="30" title="<?php echo Yii::t('app','Email'); ?> *" />
                <label for="phone" accesskey="P"><?php echo Yii::t('app','Phone'); ?></label>
                <input name="phone" type="text" id="phone" size="30" title="<?php echo Yii::t('app','Phone'); ?>" class="third" />
              </div>
              <label for="comments" accesskey="C"><span class="required"><?php echo Yii::t('app','Comment'); ?></span></label>
              <textarea name="comment" id="comment" title="<?php echo Yii::t('app','Comment'); ?> *"></textarea>
              <input type="submit" class="submit" id="submit" value="Submit" />
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end content -->
</div>
<!-- end wrap -->
<?php echo $this->renderPartial('footer'); ?>
<!-- BACK TO TOP BUTTON -->
<div id="backtotop">
  <ul>
    <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
  </ul>
</div>
<!--<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="js/retina.js" type="text/javascript"></script>
<!--<script src="js/jquery.gomap-1.3.2.min.js" type="text/javascript"></script>-->
<script src="js/jquery.isotope.min.js" type="text/javascript"></script>
<script src="js/jquery.ba-bbq.min.js" type="text/javascript"></script>
<script src="js/jquery.isotope.load.js" type="text/javascript"></script>
<script src="js/jquery.isotope.perfectmasonry.js"></script>
<script src="js/jquery.form.js" type="text/javascript"></script>
<script src="js/input.fields.js" type="text/javascript"></script>
<script src="js/responsive-nav.js" type="text/javascript"></script>
<script defer src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="js/image-hover_opacity1.js" type="text/javascript"></script>
<script src="js/scrollup.js" type="text/javascript"></script>
<script src="js/preloader.js" type="text/javascript"></script>
<script src="js/navi-slidedown.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
