<?php

?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"  dir="ltr" lang="en-US"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="milnomada">
<meta name="Description" content="Pintor artista y profesional del tattoo. Arte y tattoo en Barcelona. Tattoo también en en estudio. Vienvenidas todas las consultas y mensajes." />
<link href="/css/reset.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/css/contact.css" rel="stylesheet" type="text/css" media="screen" />
<style type="text/css">
    .quot { opacity: 0; }
</style>
<!--[if !IE]> <link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<!--[if gt IE 6]> <link href="css/styles-ie.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<link href="/css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,600,800' rel='stylesheet' type='text/css' />
<title>Tattoo Barcelona</title>
</head>
<body>
<!-- <div id="wrap"> -->
  <!-- Preloader 
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  -->
    <header class="wrapper">
    <table class="act">
        <tr><td>
            <a href="http://milnomada.com" target="_blank"><div data-text="Web Art" data-pos="right" class="a link"></div></a>
            <a href="/contact" target="_blank"><div data-text="Contact" data-pos="center" class="a mail"></div></a>
            <a href="/about" target="_blank"><div data-text="View Map" data-pos="left" class="a map"></div></a>
        </td></tr>
        <tr><td colspan="3"><div class="text"></div></td></tr>
    </table>
        
    </header>
    <div id="content">

    <ul id="container">
        <div id="contact">
            <div id="message"></div>
            <form method="post" action="contact" name="contactform" id="contactform" autocomplete="off">
                <fieldset>
                <div class="alignleft padding-right">
                <label for="name" accesskey="U"><span class="required">Name</span></label>
                <input name="name" type="text" id="name" size="30" title="Name *">
                <label for="email" accesskey="E"><span class="required">Email</span></label>
                <input name="email" type="text" id="email" size="30" title="Your Email *">
                <label for="phone" accesskey="P">Phone</label>
                <input name="phone" type="text" id="phone" size="30" title="Phone" class="third">
                </div>
                <label for="comment" accesskey="C"><span class="required">Comments</span></label>
                <textarea name="comment" id="comment" title="Comment *"></textarea>
                <input type="submit" class="submit" id="submit" value="Submit">
                </fieldset>
            </form>
            <div class="fback"></div>
            </div>
        </div>
    </ul>
<script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="/js/modernizr.js" type="text/javascript"></script>
<script src="/js/retina.js" type="text/javascript"></script>
<!--

<script src="js/jquery.gomap-1.3.2.min.js" type="text/javascript"></script>
<script src="js/jquery.isotope.min.js" type="text/javascript"></script>
<script src="js/jquery.ba-bbq.min.js" type="text/javascript"></script>
<script src="js/jquery.isotope.load.js" type="text/javascript"></script>
<script src="js/jquery.isotope.perfectmasonry.js"></script>-->

<script src="/js/jquery.form.js" type="text/javascript"></script>
<script src="/js/input.fields.js" type="text/javascript"></script>
<script src="/js/scroll.js" type="text/javascript"></script>

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</body>
</html>
