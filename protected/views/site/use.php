	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><font class="special"><b>VIOLATIONS OF Bikerent.barcelona ACCEPTABLE USE POLICY</b></font></p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">The following constitute violations of this AUP:</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>DMCA Violation and take down procedure:</b> When a DMCA violation has been reported, each customer is notified and is given 24 hours to send us back a proper response or remove the pages/material in violation. After the 24 hour period, the site is suspended until we receive proper correspondence. The second and third violations result in an automatic account suspension and a fourth violation will cause an account removal.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Illegal use:</b> Bikerent.barcelona services may not be used for illegal purposes, or in support of illegal activities. Bikerent.barcelona reserves the right to cooperate with legal authorities and/or injured third parties in the investigation of any suspected crime or civil wrongdoing.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Harm to minors:</b> Use of the Bikerent.barcelona service to harm, or attempt to harm, minors in any way, including, but not limited to child pornography.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Threats:</b> Use of Bikerent.barcelona service to transmit any material (by e-mail, uploading, posting or otherwise) that threatens or encourages bodily harm or destruction of property.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Harassment:</b> Use of Bikerent.barcelona service to transmit any material (by e-mail, uploading, posting or otherwise) that harasses another.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Fraudulent activity:</b> Use of Bikerent.barcelona service to make fraudulent offers to sell or buy products, items, or services, or to advance any type of financial scam such as "pyramid schemes," "ponzi schemes," and "chain letters."</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Forgery or impersonation:</b> Adding, removing or modifying identifying network header information in an effort to deceive or mislead is prohibited. Attempting to impersonate any person by using forged headers or other identifying information is prohibited. The use of anonymous remailers or nicknames does not constitute impersonation.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Unsolicited commercial e-mail / Unsolicited bulk e-mail (SPAM):</b> Use of the Bikerent.barcelona service to transmit any unsolicited commercial or unsolicited bulk e-mail is expressly prohibited. Violations of this type will result in the immediate termination of the offending Bikerent.barcelona account.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Bulk Mailings:</b> Bulk mailings of any kind are not permitted to be sent from our shared (Personal, Business, Reseller) servers. This line item applies to mailings sent within the 1000 emails per hour limit as well.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>E-mail / News Bombing:</b> Malicious intent to impede another person's use of electronic mail services or news will result in the immediate termination of the offending Bikerent.barcelona account.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>E-mail / Message Forging:</b> Forging any message header, in part or whole, of any electronic transmission, originating or passing through the Bikerent.barcelona service is in violation of this AUP.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Unauthorized access:</b> Use of the Bikerent.barcelona service to access, or to attempt to access, the accounts of others, or to penetrate, or attempt to penetrate, security measures of Bikerent.barcelona or another entity's computer software or hardware, electronic communications system, or telecommunications system, whether or not the intrusion results in the corruption or loss of data, is expressly prohibited and the offending Bikerent.barcelona account is subject to immediate termination.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Copyright or trademark infringement:</b> Use of the Bikerent.barcelona service to transmit any material (by e-mail, uploading, posting or otherwise) that infringes any copyright, trademark, patent, trade secret or other proprietary rights of any third party, including, but not limited to, the unauthorized copying of copyrighted material, the digitization and distribution of photographs from magazines, books, or other copyrighted sources, and the unauthorized transmittal of copyrighted software.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Collection of personal data:</b> Use of the Bikerent.barcelona service to collect, or attempt to collect, personal information about third parties without their knowledge or consent.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Free Hosting/Image Hosting Providers:</b> Use of the Bikerent.barcelona service (shared hosting accounts) to provide free hosting or image hosting to the general public is illegal..</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Reselling of Services:</b> Shared services, except for ResellerClass accounts, can only be resold to a single party. Our shared plans were not designed to host several different organizations and parties.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Network disruptions and unfriendly activity:</b> Use of the Bikerent.barcelona service for any activity which affects the ability of other people or systems to use Bikerent.barcelona Services or the Internet. This includes "denial of service" (DOS) attacks against another network host or individual user. Interference with or disruption of other network users, services or equipment is prohibited. It is the Member's responsibility to ensure that their network is configured in a secure manner. A Subscriber may not, through action or inaction, allow others to use their network for illegal or inappropriate actions. A Subscriber may not permit their network, through action or inaction, to be configured in such a way that gives a third party the capability to use their network in an illegal or inappropriate manner. Unauthorized entry and/or use of another company and/or individual's computer system will result in immediate account termination. Bikerent.barcelona will not tolerate any subscriber attempting to access the accounts of others, or penetrate security measures of other systems, whether or not the intrusion results in corruption or loss of data.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Fraud:</b> Involves a knowing misrepresentation or misleading statement, writing or activity made with the intent that the person receiving it will act upon it.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Infringement of Copyright, Patent, Trademark, Trade Secret, or Intellectual Property Right:</b> Distribution and/or posting of copyrighted or the aforementioned infringements will not be tolerated.</p>

<?php 


?>
	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Distribution of Viruses:</b> Intentional distributions of software that attempts to and/or causes damage, harassment, or annoyance to persons, data, and/or computer systems are prohibited. Such an offense will result in the immediate termination of the offending account.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Inappropriate Use of Software:</b> Use of software or any device that would facilitate a continued connection, i.e. pinging, while using Bikerent.barcelona services could result in suspension service.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Verbal abuse of support personnel:</b> Verbal abuse of Bikerent.barcelona personnel is not tolerated. Customers who continue to verbally abuse support personnel may be warned or asked to use a different provider.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><b>Third Party Accountability:</b> Bikerent.barcelona subscribers will be held responsible and accountable for any activity by third parties, using their account, that violates guidelines created within the Acceptable Use Policy.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><font class="special"><b>SECURITY</b></font></p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">You are responsible for any misuse of your account, even if the inappropriate activity was committed by a friend, family member, guest or employee. Therefore, you must take steps to ensure that others do not gain unauthorized access to your account. In addition, you may not use your account to breach security of another account or attempt to gain unauthorized access to another network or server. Your password provides access to your account. It is your responsibility to keep your password secure.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">Sharing your password and account access with unauthorized users is prohibited. You should take care to prevent others from using your account since you will be held responsible for such use. Attempting to obtain another user's account password is strictly prohibited, and may result in termination of service.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">You must adopt adequate security measures to prevent or minimize unauthorized use of your account.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">You may not attempt to circumvent user authentication or security of any host, network or account. This includes, but is not limited to, accessing data not intended for you, logging into or making use of a server or account you are not expressly authorized to access, or probing the security of other networks. Use or distribution of tools designed for compromising security is prohibited. Examples of these tools include, but are not limited to, password guessing programs, cracking tools or network probing tools. You may not attempt to interfere with service to any user, host or network ("denial of service attacks"). This includes, but is not limited to, "flooding" of networks, deliberate attempts to overload a service, and attempts to "crash" a host.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">Users who violate systems or network security may incur criminal or civil liability. Bikerent.barcelona will cooperate fully with investigations of violations of systems or network security at other sites, including cooperating with law enforcement authorities in the investigation of suspected criminal violations.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;"><font class="special"><b>NETWORK PERFORMANCE</b></font></p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">Bikerent.barcelona accounts operate on shared resources. Excessive use or abuse of these shared network resources by one customer may have a negative impact on all other customers. Misuse of network resources in a manner which impairs network performance is prohibited by this policy and may result in termination of your account.</p>

	<p class="pretty" style="text-align: justify; text-indent: 1.5em;">You are prohibited from excessive consumption of resources, including CPU time, memory, disk space and session time. You may not use resource-intensive programs which negatively impact other customers or the performances of Bikerent.barcelona systems or networks. Bikerent.barcelona reserves the right to terminate or limit such activities. 

	</p>
