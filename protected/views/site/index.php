<?php
    require_once( Yii::app()->basePath . '/helpers/MathHelper.php');
?>
<style type="text/css">
  * { box-sizing: border-box; }
  html, body { height: 100%; }
  .ad-box { margin: 0; box-shadow: none; }
  .grid-sizer { width: 20%; }
  .grid { height: 75%; }
  /* clear fix */
  .grid:after {
    content: '';
    display: block;
    clear: both;
  }
  .grid-item {
    float: left;
  }
</style>
<div id="wrap">

    <?php echo $this->renderPartial('header'); ?>
    <div id="content"><div class="grid">

     <?php 
     $sz=array();
     $colorset = ['rgb(245, 245, 245)','rgb(240, 240, 240)','rgb(230, 230, 230)','rgb(210, 210, 210)','rgb(155, 155, 155)','rgb(205, 205, 205)','rgb(125, 125, 125)','rgb(200, 200, 200)', 'rgb(135, 135, 135)','rgb(120, 120, 120)','rgb(230, 230, 230)','rgb(210, 210, 210)','rgb(175, 175, 175)','rgb(165, 165, 165)','rgb(125, 125, 125)','rgb(210, 210, 210)'];
     $sizeset=['medium','small','tiny','regular','large'];
     $txtclass=['add-pre','add-str'];

     $sizes=[340,150,80,270,540];

     foreach ($set as $elem ) { echo '<div class="grid-item" data-hash="'.$elem['hash'].'">'.$elem['htmlad'].'</div>'; } 

     for ( $i=0; $i<0; $i++) {
        $n = mt_rand(0,count($sizeset)-1);
        $cn = mt_rand(0,count($colorset)-1);
        $txtn = mt_rand(0,count($txtclass)-1);

        $c=$colorset[$cn];

        echo '<div class="grid-item"><div class="ad-box '.$sizeset[$n].'">
                <a class="adlnk" href="" target="_blank">
                  <div class="elem-ad" style="animation: animation-5 850ms linear both; background-image: url(&quot;&quot;); background-color: '.$c.'; background-size: 100% 100%; background-position: 0px 0px;"></div>
                  <div class="'.$txtclass[$txtn].' shadow">
                    <h2 style="text-shadow: rgb(238, 238, 238) 2px 2px 6px; color: rgb(0, 0, 0); font-family: Orator;">great electric bikes for a great day</h2>
                  </div>
                </a>
              </div></div>';

        $sz[]=$sizes[$n];
      }
      $w= MathHelper::gcd($sz);
      ?>
     </div>
    </div>
  </div>
  <?php echo $this->renderPartial('footer'); ?>
  <div id="backtotop">
    <ul>
      <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
    </ul>
</div>

<script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="http://isotope.metafizzy.co/isotope.pkgd.js" type="text/javascript"></script>
<script src="/js/jquery.isotope.perfectmasonry.js" type="text/javascript"></script>
<script src="/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="/js/mustache.js" type="text/javascript"></script>
<script src="/node_modules/bounce.js/bounce.min.js" type="text/javascript"></script>
<script type="text/javascript"><?php echo "\n\tvar _cw=10;\n"; ?></script>
<script src="/js/map.js" type="text/javascript"></script>
<script src="/js/bikes.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
