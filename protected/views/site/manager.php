<?php

	/** 
	 * 	Load Ads module libs
	 * 	Render Ad Management views
	 * 
	 * 	The module is run using old fashion php rendering
	 *  Too much to modify and works great
	 * 
	 * @var string $base the application base path
	 */
	error_reporting(E_ALL);
	$base=Yii::app()->basePath;
	echo "<div id='content'>";
    
    require "$base/modules/ad/lib/admin.php" ;

    echo "</div>";
?>