<?php
    $name = "bikerent";
?>

<div id="wrap">
  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <?php echo $this->renderPartial('header'); ?>
  <div id="content">
    <div class="container clearfix">
      <div id="container">
       
        <div class="col1-3 home element">
          <div class="images"> 
            <!--
            <img src="" alt="" />
            <div id="map"></div>
            -->
          </div>
          <div class="white-bottom mine grey-area-last">
            <h2><?php echo Yii::t('app','Flexible'); ?></h2>
            <p class="big inline"></p><?php echo Yii::t('app','<h1>Adverts</h1><p class="big inline"> have plenty of customizable features.</p>'); ?><br>
            <h4><?php echo Yii::t('app','Size'); ?></h4>
            <p class="big inline"><?php echo Yii::t('app','You can choose among 5 different sizes'); ?>. <?php echo Yii::t('app','Every'); ?> </p><h3><?php echo Yii::t('app','Ad'); ?></h3><p class="big inline"> <?php echo Yii::t('app','size in'); ?> </p><h3>bike</h3><h3>rent</h3> <h3>barcelona</h3><p class="big inline"> <?php echo Yii::t('app','has its own features. Choose the plan that better meet your needs.'); ?></p>
            <h4><?php echo Yii::t('app','Design'); ?></h4>
            <p class="big inline"><?php echo Yii::t('app','From just a title and a text, up to insert your own designed HTML code, include a google map or some other features.')." ".Yii::t('app','Your space is yours and therefore can <a href="/terms-of-use">use it</a> as you like.'); ?><br><i><?php echo Yii::t('app','It\'s up to you'); ?></i></p><br>
            
            <h4><?php echo Yii::t('app','Time'); ?></h4>
            
            
            <!--
            <ul class="unordered-list clearfix">
              <li><p class="head">Size</p><p class="sub"> 5 different Add <strong>sizes</strong></p></li>
              <li><p class="head">Url</p><p class="sub"> Every Add comes with a web address like:<br><code>http://bikerent.barcelona/{Add_title}</code> which increases your Add <i>Search Engine Visibility</i> </p></li>
              <li><p class="head">Map</p><p class="sub"> You can insert a google maps marking your business</p></li>
              <li><p class="head">Images</p><p class="sub"> Background and foreground images for your Add</p></li>
              <li><p class="head">Design</p><p class="sub"> Simply insert title and text.<br>Or maybe you have a HTML code to paste.<br>It's <i>up to you</i></p></li>
            </ul>
            -->
            <p class="big inline"><?php echo Yii::t('app','You'); ?> </p><h3><?php echo Yii::t('app','choose'); ?></h3><p class="big inline"><?php echo " ".Yii::t('app','the time the advert will be displayed in the '); echo Yii::t('app','<a href="/" target="_blank">home</a> page.</p>'); ?>
            <p class="big"><a href="/plans" class="brlink"><?php echo Yii::t('app','Read more'); ?></a></p>
            <div class="grey-area last smaller clearfix">
                <p class="small">
                <span class="alignleft"><a href=""><?php echo Yii::t('app','View Ads'); ?></a></span>
                <span class="alignright"><a href="/plans"><?php echo Yii::t('app','Plan Options'); ?></a></span>
                </p>
            </div>
          </div>
        </div>
        <div class="col1-3 home element">
          <div class="images"> 
          </div>
          <div class="white-bottom mine grey-area-last right">
            <h2><?php echo Yii::t('app','Cheap'); ?></h2>
            <p class="big inline"><?php echo Yii::t('app','The "Almost <h3>FREE</h3><p class="big inline"> ad solution", offers an interesting chance to get visits to your <i>site</i> or <i>business</i> for  <strong>1€/month</strong> <span class="small">( 0.0013€/hour )</span></p>'); ?> <p class="big"></p>
            <p class="big"><?php echo Yii::t('app','Every <span class="high">ad plan</span> can be <span class="high">Monthly</span> paid. <span class="_link">Flexibility</span> is a must for us.</p><p class="big"></p>'); ?>
            <p class="big inline"><?php echo Yii::t('app','<span class="high">Yearly</span> paid <span class="high">ad plans</span> are <h3>price reduced</h3></p>'); ?>
            <p class="big"><a href="/plans" class="brlink"><?php echo Yii::t('app',''); ?><?php echo Yii::t('app','Read more'); ?></a></p>
            
            <div class="grey-area last smaller clearfix">
                <p class="small">
                <span class="alignleft"><a href="/"><?php echo Yii::t('app','Ad Products'); ?></a></span>
                <span class="alignright"><a href="/plans"><?php echo Yii::t('app','Plans'); ?></a></span>
                </p>
            </div>
          </div>
        </div>

        <div class="col1-3 home element">
          <div class="images"></div>
          <div class="white-bottom mine grey-area-last">
            <h2><?php echo Yii::t('app','Efficient'); ?></h2>
            <?php echo Yii::t('app','<p class="big inline">ads here are a efficient way to increase your <h3>bike</h3><p class="big inline"> related </p><h3>business accessibility</h3>'); ?>
            <h4><?php echo Yii::t('app','Domain name'); ?></h4>
            <p class="big inline"><?php echo Yii::t('app','See how your'); ?> </p><h3><?php echo Yii::t('app','business grows'); ?></h3><p class="big inline"> <?php echo Yii::t('app','linking your website and '); ?> <span class="high"><?php echo Yii::t('app','using the space'); ?></span>, <?php echo Yii::t('app','that a <h3>powerful domain</h3><p class="big inline"> as <span class="high">http://bikerent.barcelona</span> offers for you.</p></p>'); ?>
            <h4><?php echo Yii::t('app','Monitor'); ?></h4>
            <p class="big inline"><?php echo Yii::t('app','Track the number of clicks on your advert, see how many visits has in the ad page or in the bikerent.barcelona home page'); ?>.</p><br>
            <img class="graf" src="/images/stats.png" title="stats" /><br><p class="big inline">
            <p class="big inline"><?php echo Yii::t('app','Watch live ad details in the'); ?> </p><?php echo Yii::t('app','<h3>visualization statistics</h3><p class="big inline"> section</p>'); ?>
            <h4><?php echo Yii::t('app','Manage'); ?></h4>
            <p class="big inline"><strong><?php echo Yii::t('app','Easy'); ?></strong> </p><h3><?php echo Yii::t('app','Ad management'); ?></h3><p class="big inline"> <?php echo Yii::t('app','in the user area. No configuration nighmares'); ?>.</p>
           
            <p class="big"><a href="/plans" class="brlink"><?php echo Yii::t('app','Read more'); ?></a></p>
            <div class="grey-area last smaller clearfix">
                <p class="small">
                <span class="alignleft"><a href="#"><?php echo Yii::t('app','More Information'); ?></a></span>
                <span class="alignright"><a href="/payments"><?php echo Yii::t('app','Payments'); ?></a></span>
                </p>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end content -->
</div>
  <?php echo $this->renderPartial('footer'); ?>


<!-- BACK TO TOP BUTTON -->
<div id="backtotop">
  <ul>
    <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
  </ul>
</div>
<!--<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="js/modernizr.js" type="text/javascript"></script>
<script src="js/retina.js" type="text/javascript"></script>
<!--<script src="js/jquery.gomap-1.3.2.min.js" type="text/javascript"></script>-->
<script src="js/jquery.isotope.min.js" type="text/javascript"></script>
<script src="js/jquery.ba-bbq.min.js" type="text/javascript"></script>
<script src="js/jquery.isotope.load.js" type="text/javascript"></script>
<script src="js/jquery.isotope.perfectmasonry.js"></script>
<script src="js/jquery.form.js" type="text/javascript"></script>
<script src="js/input.fields.js" type="text/javascript"></script>
<script src="js/responsive-nav.js" type="text/javascript"></script>
<script defer src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="js/image-hover_opacity1.js" type="text/javascript"></script>
<script src="js/scrollup.js" type="text/javascript"></script>
<script src="js/preloader.js" type="text/javascript"></script>
<script src="js/navi-slidedown.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
