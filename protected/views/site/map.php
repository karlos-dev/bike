<?php
    //echo "<pre>$this->id {$this->action->id} \n";
    $cs = Yii::app()->getClientScript(); 
    $key = "AIzaSyBUoZSzzzUnniQXkm_SPyVmzH3h8AsygNg";
    $cs->registerScriptFile( "https://maps.googleapis.com/maps/api/js?key=$key&callback=init", CClientScript::POS_END, array('type'=>'text/javascript') );
?>
<style type="text/css">

  #map    { width: 100%; height: 520px; margin-top: -180px; }
  .iwlink {     
    color: #3070a0;
    font-weight: 400;
    font-family: Arial; 
  }
  .ad-box { box-shadow: none; }
  .adlnk {
      display: block;
      height: 100%;
    }
    
</style>

<?php echo $this->renderPartial('header'); ?>
<div id="content">
    <div id="map">
    </div>
</div>
<?php echo $this->renderPartial('footer'); ?>
<div id="backtotop">
<ul>
  <li><a id="toTop" href="#" onClick="return false">Back to Top</a></li>
</ul>
</div>

<script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="/js/mustache.js" type="text/javascript"></script>
<script src="/node_modules/bounce.js/bounce.min.js" type="text/javascript"></script>
<script src="/js/map.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
