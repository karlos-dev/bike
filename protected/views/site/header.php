<?php 
    $a = $this->action->id;
?>
<header id="wrapper">
<div class="container clearfix">
    <a href="/"><h1 id="logo">BIKERENT</h1><h2>BARCELONA</h2></a>
    <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
    <div id="options" class="clearfix">
        <div id="options" class="clearfix">
            <ul id="main-menu">
                <li><a href="/features" <?php echo ( $a=='features') ? 'class="current"' : ''; ?>><?php echo Yii::t('app','Features'); ?></a>
                <li><a href="/ads" <?php echo ( $a=='ads') ? 'class="current"' : ''; ?>><?php echo Yii::t('app','Create your Ad'); ?></a>
                <li><a href="/map" <?php echo ( $a=='map') ? 'class="current"' : ''; ?>><?php echo Yii::t('app','Rent Map'); ?></a>
                <li><a href="/contact" <?php echo ( $a=='contact') ? 'class="current"' : ''; ?> ><?php echo Yii::t('app','Contact'); ?></a></li>
            </ul>
        </div>
        <?php if ( $a == 'index') { ?>
        <ul id="homepage" class="option-set clearfix" data-option-key="filter">
            <li><a href="#filter=.home" class="selected"><?php echo strtoupper( Yii::t('app','All') ); ?></a>
            <li><a href="#filter=.sic"><?php echo strtoupper( Yii::t('app','Classic') ); ?></a></li>
            <li><a href="#filter=.ric"><?php echo strtoupper( Yii::t('app','Electric') ); ?></a></li>
            <li><a href="#filter=.xie"><?php echo strtoupper( Yii::t('app','Fixie') ); ?></a></li>
            <li><a href="#filter=.pie"><?php echo strtoupper( Yii::t('app','Cheapie') ); ?></a></li>
            <li><a href="/user/login" class="la"><?php echo strtoupper( UserModule::t('Login') ); ?></a></li>
        </ul>
        <?php } ?>
    </div>
</div>
</header>