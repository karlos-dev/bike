<?php
    $name = "bikerent";
    $a = ucfirst( $this->action->id );

    $ipAddress = $_SERVER['REMOTE_ADDR'];
	if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	    $ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
	}

	$ua = $_SERVER['HTTP_USER_AGENT'];

?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"  dir="ltr" lang="en-US"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="">
<meta name="Description" content="Psychedelic Culture, Music Index, Human Evolution Guide and Free Information website." />
<title>Bikerent Barcelona</title>
<link href="/css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="/css/reset.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/css/main.css" rel="stylesheet" type="text/css" media="print" />

<link href="/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
<link href="/css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if gt IE 8]><!--><link href="/css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /><!--<![endif]-->
<!--[if !IE]> <link href="/css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<!--[if gt IE 6]> <link href="/css/styles-ie.css" rel="stylesheet" type="text/css" media="screen" /> <![endif]-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,600,800' rel='stylesheet' type='text/css' />

<link rel="stylesheet" href="/js/forms/css/jquery.idealforms.css">
<link rel="stylesheet" href="/js/sb/jquery.switchButton.css">
<link rel="stylesheet" href="/css/colorPicker.css">

<title><?php echo Yii::t('app', $a); ?> | <?php echo $name;?></title>
</head>
<body class="post">
<?php  echo session_id(); echo " $ipAddress"; echo $content; ?>
<script type="text/javascript">
	<?php echo "\n\tvar _sui='".session_id()."';\n\tvar _sip='".$ipAddress."';\n\tvar _ua='".$ua."';\n\n"; ?>
</script>
</body>
</html>