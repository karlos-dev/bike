<?php /* @var $this Controller */ ?>
<?php $this->beginContent('application.views.layouts.main'); ?>
<style type="text/css">
	ul.operations li { display: inline-block; }
	ul.operations li a { padding: 0.125rem 0.3125rem; }
</style>
<div class="span-24">
	<div id="content" class="">
		<?php
			/*$this->beginWidget('zii.widgets.CPortlet', array(
				'title'=>'Operations',
			));*/
			if( ( $this->id == 'payments' && UserModule::isAdmin() ) || ( $this->id !== 'payments') ) {
				$this->widget('zii.widgets.CMenu', array(
					'items'=>$this->menu,
					'htmlOptions'=>array('class'=>'operations'),
				));
				echo "<hr>";
			}
			//$this->endWidget();
		?>
		
		<?php echo $content; ?>
	</div>
</div>
<!--<div class="span-5 last">
	<div id="sidebar">-->
	
	<!-- </div> sidebar
</div> -->
<?php $this->endContent(); ?>