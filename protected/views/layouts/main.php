<?php 
/* @var $this Controller */ 
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection"><![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<style type="text/css">
		.copyleft {
			font: 16px Arial, sans-serif;
			display: inline-block;
			transform: rotate(180deg);
		}
	</style>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<div class="container" id="page">
		<div id="header">
			<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
		</div><!-- header -->

		<div id="mainmenu">
			<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'Profile',  'url'=>array('/user/profile'), 'visible'=>( !UserModule::isAdmin() && !Yii::app()->user->isGuest ) ),
					array('label'=>'Home',  'url'=>array('/user/profile'),    'visible'=>UserModule::isAdmin()),
					// Here must apply yii rights stuff
					
					array('label'=>'Users',   'url'=>array('/user'), 	   	'visible'=>UserModule::isAdmin()),
					array('label'=>'Ads',     'url'=>array('/site/manager'),'visible'=>UserModule::isAdmin()),
					array('label'=>'MyAds',   'url'=>array('/myads'), 	        'visible'=>( !UserModule::isAdmin() && !Yii::app()->user->isGuest ) ),
					array('label'=>'Payments','url'=>array('/payments'),    'visible'=>!Yii::app()->user->isGuest),
					array('label'=>'Rights',  'url'=>array('/rights'), 		'visible'=>UserModule::isAdmin()),

					array('label'=>'Contact', 'url'=>array('/site/contact')),
					array('label'=>'Login', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest ),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
				),
			)); ?>
		</div> 
		<?php
		/**
		 * 	FIXED
		 * 
		 *  When the user is not logged I change the view in: 
		 *  	login, 
		 *   	activation 
		 *  
		 *  userModule controllers 
		 * 
		 * 	This can be performed in a better way using render and render partial in SiteController.
		 *  Check column1, column2 layouts also
		 *  
		 */ 
		?>
		<?php echo $content; ?>
	</div>
	<div class="clear"></div>
	<div id="footer">
		Copyleft <span class="copyleft">&copy;</span> <?php echo date('Y'); ?> Weird IT 2016<br/>
		No Rights Reserved<br/>
	</div>
</div>
	<!-- <script src="/js/run_prettify.js"></script> -->
</body>
</html>
