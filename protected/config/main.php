<?php
/**
 * General application config
 * @author  Karlos Álvarez <info@milnomada.io>
 * 
 */

$dbName =               "";
$dbConnectionString =   "";
$dbReplica =            "";
$lang = ( isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) : 'en';

date_default_timezone_set('GMT');

$dbFile = dirname(__FILE__).DIRECTORY_SEPARATOR.'..' . DIRECTORY_SEPARATOR .  'runtime/bike-' . COMMIT . '.db';


return array(
	'basePath'=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Bikerent Barcelona',
	'homeUrl'=>'',
	//'runtimePath'=>
	'language'=>$lang,
	'preload'=>array(
		'log',
		'errorHandler', // handle fatal errors
	),

	'aliases' => array(
		'vendor' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.'/vendor',
        'audit' =>  dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.'/vendor/cornernote/yii-audit-module/audit',
    ),

	// autoloading model and component classes
	'import'=>array(
        'application.components.*',
        'application.controllers.*',
        'application.modules.*',
        'application.models.*',
        'application.modules.user.*',
        'application.modules.ad.*',
        'application.modules.ad.models.*',
        'application.modules.user.models.*',
        'application.modules.rights.*',
		'application.modules.rights.components.*', // Correct paths if necessary.
	),

	'modules'=>array(

        'user'=>array(
            'hash' => 'md5',
            'sendActivationMail' => true,
            'loginNotActiv' => false,
            'activeAfterRegister' => false,
            'autoLogin' => true,
            'encryptSession'=>true, // Haha
            'registrationUrl' => array('/user/registration'),
            'recoveryUrl' => array('/user/recovery'),
            'loginUrl' => array('/user/login'),
            'returnUrl' => array('/user/profile'),
            'returnLogoutUrl' => array('/user/login'),
        ),
		'rights' => array(
			'install' => false,
			'debug'=> true,
			'userClass' => 'User',
		),
        'ad' => array(
            'class' => 'AdModule',
            'adClasses' => array(
                'large',
                'medium',
                'regular',
                'small',
                'tiny'
            ),
        ),
		'audit' => array(
            'class' => 'audit.AuditModule', // http://cornernote.github.io/yii-audit-module/#installation
            
            // AuditModule will replace --user_id-- with the actual user_id
            // 'userViewUrl' => array('/user/view', 'id' => '--user_id--'),

            'enableAuditField' => true,	// Set to false if you do not wish to track database audits.
 
            // The ID of the CDbConnection application component. If not set, a SQLite3
            // database will be automatically created in protected/runtime/audit-AuditVersion.db.
            // 'connectionID' => '',
            
            // it is recommended you set this property to be false to improve performance.
            'autoCreateTables' => false, // the DB tables should be created automatically if they do not exist.
 
            // The layout used for module controllers.
            'layout' => 'audit.views.layouts.column1',
 
            'gridViewWidget' => 'bootstrap.widgets.TbGridView',
            'detailViewWidget' => 'zii.widgets.CDetailView',
 
            // Defines the access filters for the module.
            'controllerFilters' => array(
                //'auditAccess' => array('audit.components.AuditAccessFilter'),
            ),

            // A list of users who can access this module in AuditAccessFilter
            'adminUsers' => array('admin'),
 
            // The path to YiiStrap.
            // 'yiiStrapPath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.'/vendor/crisu83/yiistrap',
        ),
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'char00',
        ),
	),

	// application components
	'components'=>array(
        'securityManager' => array(
            //'encryptionKey' => substr( sha1(COMMIT), 0, 16)
        ),
        /*
        'session' => array (
            'sessionName' => 'Site Session',
            'class'=>'CHttpSession',
            'useTransparentSessionID'=>( isset($_POST['PHPSESSID']) ) ? true : false,
            'autoStart' => 'false',
            'cookieMode' => 'only',
            'savePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.'/runtime',
            'timeout' => 300
        ),
        */
		'db'=>array(
            'class'=>'KDbConnection',
            'connectionString'=>'sqlite:'.$dbFile,
            'file'=>$dbFile,
        ),
        /**
         * SELECT sum(round(((data_length + index_length) / 1024 / 1024 / 1024), 2))  as "Size in GB" FROM information_schema.TABLES  WHERE table_schema = "ads";
         * SELECT sum(round(((data_length + index_length) / 1024 / 1024 / 1024), 2))  as "Size in GB", sum(round(((data_length + index_length) / 1024 / 1024 ), 2))  as "Size in MB", sum(round(((data_length + index_length) / 1024 ), 2))  as "Size in Kb" FROM information_schema.TABLES  WHERE table_schema = "ads";
         */
        'mysql'=>array(
            'class'=>'KDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=ads',
            'emulatePrepare' => true,
            'username' => 'char',
            'password' => 'char00',
            'charset' => 'utf8',

            //'enableSchemaCache' => true,
            // Name of the cache component used to store schema information
            //'schemaCache' => 'cache',
            // Duration of schema cache.
            //'schemaCacheDuration' => 86400,
        ),
        
		'urlManager'=>array(            
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(          
				'<action:(features|plans|contact|ads|sads|soon|map)>'=>'site/<action>',  
                '<action:(plans|sads)>/<i:\d+>'=>'site/<action>',  
				// special names
				'terms-of-use'=> 	'site/use',
				// query vars
				'ads/<opt:\w+>'=>	'site/ads',  
				'features/<elem:[\s\S]+>'=>	'site/features',

                // --> For admin area naming url's
                'myads' => 'ad',
                'myads/<controller:\w+>' => 'ad/<controller>',
                'myads/<controller:\w+>/<action:\w+>' => 'ad/<controller>/<action>',
                'myads/<controller:\w+>/<action:\w+>/<id:\w+>' => 'ad/<controller>/<action>',
                // <-- end
                
                'ad/<controller:\w+>/<action:\w+>/<com:\w+>' => 'ad/<controller>/<action>',

                // module navigation
                '<module:\w+>'=>'<module>',  
                '<module:\w+>/<controller:\w+>'=>'<module>/<controller>',  
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',  
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\w+>'=>'<module>/<controller>/<action>',


				// defaults
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',  
                '<controller:\w+>/<action:\w+>/<id:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>/<pos:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>/<ini:\w+>/<end:\w+>'=>'<controller>/<action>',

                // FIX
                // many special characters might be supported here
                '<url:[a-z0-9-\']+>'=>'ad/view',  
			),
		),
		'user'=>array(
			'class'=>'RWebUser', // Allows super users access implicitly.
			'loginUrl'=>array('user/login'),
            'returnUrl' => array('user/profile'),
            'allowAutoLogin' =>true,
		),

		'authManager'=>array(
			'class'=>'RDbAuthManager', // Provides support authorization item sorting.
		),
        /*
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
       	*/
        'errorHandler' => array(
            // path to the AuditErrorHandler class
            'class' => 'audit.components.AuditErrorHandler',
            // set this as you normally would for CErrorHandler
            'errorAction' => 'site/error',
            // Set to false to only track error requests.  Defaults to true.
            'trackAllRequests' => true,
            // Set to false to not handle fatal errors.  Defaults to true.
            'catchFatalErrors' => true,
            // Request keys that we do not want to save in the tracking data.
            'auditRequestIgnoreKeys' => array('PHP_AUTH_PW', 'password'),
 
        ),
        
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, info, audit',
				),
				array(
                    // path to the AuditLogRoute class
                    'class' => 'audit.components.AuditLogRoute',
                    // can be: trace, warning, error, info, profile
                    // can also be anything else you want to pass as a level to `Yii::log()`
                    'levels' => 'error, warning, profile, audit',
                ),                
			),
		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
     
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'admin@psychedelicspace.com',
		'mail-enabled'=>true,
	),


);
