<?php

// This is the database connection configuration.
$dbFile = dirname(__FILE__).DIRECTORY_SEPARATOR.'..' . DIRECTORY_SEPARATOR .  'runtime/bike-' . COMMIT . '.db';
return array(
	'connectionString' => 'sqlite:'. $dbFile,
	//dirname(__FILE__).'/../data/testdrive.db',

	// uncomment the following lines to use a MySQL database
	/*
	'connectionString' => 'mysql:host=localhost;dbname=testdrive',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	*/
);