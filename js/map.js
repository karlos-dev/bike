
/*

var TextToSVG = require(['text-to-svg']).TextToSVG;
var textToSVG = new TextToSVG();
 
var attributes = {fill: 'red', stroke: 'black'};
var options = {x: 0, y: 0, fontSize: 72, attributes: attributes};
 
var svg = textToSVG.getSVG('hello', options);
 
console.log(svg);

*/

var fnav = {

	baseUrl : "http://localhost:1080",
	current : { 
		data:null
	},
	cred : {
		user: 'map',
		pass: 'map?map=true',
		sui:undefined,
		sip:undefined
	},
	request: function (url, fn, method, postdata){
		var mode = false;

		// Prepare request for POST
		var METHOD = 	(method !== undefined) ? method.toUpperCase() : "GET";
		var DATA = 		(METHOD == "POST" && postdata !== undefined) ? postdata : {};

		$.ajax({
            url: url,
            type: METHOD,
            dataType: 'json',
            data: DATA,
            cache:false,
            
            // Append basic http auth credentials
            beforeSend: function (xhr) {
			    xhr.setRequestHeader("Authorization", "Basic " + btoa( fnav.cred.user + ":" + fnav.cred.pass ));
			},
			
            success: function(data){           	
            	if(fn) {
            		fn(data);
            	} else {
            		return data;
            	}
            },
            error: function(data){
                return undefined;
            },
            always: function(){ 
            }        
        });
	},
}

var mapObj = {
	map : undefined,
	geocoder : undefined,
	added : [],
	markers : [],
	points : [],
	bounds : undefined,

	private : {
		key : "AIzaSyBUoZSzzzUnniQXkm_SPyVmzH3h8AsygNg",
		listeners : {
			toggleBounce : function(){
				console.log("Listener toggleBounce");

				if ( this.getAnimation() !== null) {
				    this.setAnimation(null);
				} else {
				    this.setAnimation(google.maps.Animation.BOUNCE);
				}
			}
		}
	},
	init : function() {
		
		mapObj.bounds = new google.maps.LatLngBounds();
		mapObj.map = new google.maps.Map( document.getElementById('map'), {
		    center: {lat: 41.390884, lng: 2.179938 },
		    zoom: 14
		});
		console.log("Map ready");
	},
	getInfoWindow : function( obj ) { 
		return '<div id="content">'+
				'<div id="siteNotice">'+
				'</div>'+
				'<h1 id="firstHeading" class="firstHeading">' + obj.title + '</h1>'+
					'<div id="bodyContent" data-hash="' + obj.hash +'" >'+
					obj.htmlad + // Open in curren page --> .replace('target="_blank"', "") +
					'<p><b>' + obj.address + '</b> (' + obj.zip + ')</p>'+
					'<a class="iwlink" href="http://' + window.location.host + '/' + obj.uri + '" >' + window.location.host + '/' + obj.uri + '</a>' +
				'</div>'+ 
			'</div>';
	},
	addMarker: function(data){
		url = '/images/marker-green-sm.png';
		var image = {
			url: url,
			size: 	new google.maps.Size(32, 32),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(16, 32)

		};
		var point = {lat: parseFloat(data.lat), lng: parseFloat(data.lng)};
		if( point.lat && point.lng && mapObj.added.indexOf( point.lat ) == -1  ) {
			mapObj.added.push( point.lat );

			var infowindow = new google.maps.InfoWindow({
				content: mapObj.getInfoWindow(data)
			});

			var marker = new google.maps.Marker({
			    map: mapObj.map,
			    draggable: true,
			    icon: image,
			    animation: google.maps.Animation.DROP,
			    position: point,
			    title: data.address
			  });

			//marker.addListener('click', mapObj.private.listeners.toggleBounce);
			marker.addListener('click', function() {
				infowindow.open(mapObj.map, marker);
			});

			mapObj.markers.push(marker);
			mapObj.bounds.extend( marker.getPosition() );
		}
	},
	fillMap: function(data){
		var all={}, lweek={},
			unixTimestamp = Date.now(), now,
			lastweek = 1000 * 60 * 60 * 24 * 7,
			olderNew = 0,
			olderUpdate = 0,
			olderNewC = "",
			olderUpdateC = "";

		for (var i in data) {
			if( parseInt(data[i].created) > olderNew ) {
				olderNew = data[i].created;
				olderNewC = data[i].country;
			}
			if( parseInt(data[i].updated) > olderUpdate ) {
				olderUpdate = data[i].updated;
				olderUpdateC = data[i].country;
			}
			if( data[i].count !== undefined ) {
				now = parseInt( (unixTimestamp-lastweek )/1000 );
				console.log(data[i].created + " " + now );

				if( typeof(data[i].country) == 'string' && !lweek[ data[i].country ] ) {
					if( parseInt(data[i].created) > now || parseInt(data[i].updated) > now )  {
						lweek[data[i].country] = parseInt(data[i].count+1);
					}
				} else if( typeof(data[i].country) == 'string' )
					if( parseInt(data[i].created) > now || parseInt(data[i].updated) > now ) {
						lweek[data[i].country] = (parseInt( lweek[data[i].country] ) + parseInt(data[i].count) );
					}

				if( typeof(data[i].country) == 'string' && !all[ data[i].country ] ) {
					all[data[i].country] = parseInt(data[i].count+1);
					console.log( data[i].country + " count: " + all[data[i].country] + " real: " + data[i].count);
					
				}
				else if( typeof(data[i].country) == 'string' ) {
					all[data[i].country] = (parseInt( all[data[i].country] ) + parseInt(data[i].count) );
				}
				
				mapObj.addMarker( data[i] );
			}
		}
		mapObj.map.fitBounds(mapObj.bounds);
		var total = 0;
			str = '<div class="large-3 columns c">';
		for(var j in all) {
			total+=all[j];
			str+='<div><span>' + j + '</span> <span>(' + all[j]  + ')</span></div>';	
		}

		str+='<div><span><b>Total</b></span> <span><b>(' + total  + ')</b></span></div>'
		str+='</div>';
		str += '<div class="large-3 columns b">';
		for(var k in lweek) {
			total+=lweek[k];
			str+='<div><span>' + k + '</span> <span>(' + lweek[k]  + ')</span></div>';	
		}
		str+='</div>';
		
		var olderNewDate = new Date(olderNew * 1000),
			olderUpDate = new Date(olderUpdate * 1000);

		str+='<div class="large-6 columns a">';
		str+='<div><span>Last New Access ' + olderNewDate.toDateString() + '</span> <span>(' + olderNewC + ')</span></div>';	
		str+='<div><span>Last Update Access ' + olderUpDate.toDateString() + '</span> <span>(' + olderUpdateC + ')</span></div>';	
		str+='</div>';

		$('.board').append(str);
		$('.board').append();	
	},
	
	geoCodeStuff: function( data) {
		var obj;
		for(var i in data) {
			obj = data[i];
			if( data[i].lat == undefined || data[i].lat == '' || data[i].lng == undefined || data[i].lng == '' ) {
				mapObj.geoCodeAddress(obj.address);
			} else {
				mapObj.points.push(obj);
			}
		}
		setTimeout(function(){
			fnav.request("/ad/ad/rest/geo", mapObj.callback, 'POST', { data : JSON.stringify( mapObj.points ) } );
		}, 450);
	},
	callback : function(data){
		
		for(var i in mapObj.points) { 
			mapObj.addMarker( mapObj.points[i] );
		}
		mapObj.map.fitBounds(mapObj.bounds);
	},
	geoCodeAddress: function(addr) {
		if( mapObj.geocoder == undefined ) {
			mapObj.geocoder = new google.maps.Geocoder();
		}
		mapObj.geocoder.geocode({'address': addr}, function(results, status) {
			if ( status === google.maps.GeocoderStatus.OK ) {
				mapObj.points.push({ address: addr, lat:results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()});
			} else {
				//alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}
}

function init() {
	mapObj.init();
	fnav.request("/ad/ad/rest/all", mapObj.geoCodeStuff );
}

$(window).on('load', function(){

	fnav.cred.sui = _sui;
	fnav.cred.sip = _sip;

	// Animation on click ad
	anim = new Bounce();
	anim.scale({
		from: { x: 1, y: 1 },
		to: { x: 1.2, y: 1.2 },
		easing: "sway",
		bounces: 8
	});

	$("#content").on('click','.adlnk', function(e) {

		var who = $(this).parent().parent().attr('data-hash');
		anim.applyTo( $(e.currentTarget).find('.elem-ad') ); // callback? --> .then(function(){});

		e.preventDefault();

		fnav.request("/ad/ad/rest/click", function(data){ 
			console.log(data);
		}, 'POST', { hash: who, s: fnav.cred.sui, i: fnav.cred.sip, ua: _ua } );

		// $('.adlnk').trigger('click');
	});
})