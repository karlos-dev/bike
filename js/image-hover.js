jQuery(document).ready(function(){					
    /*
    	 jQuery('.element').bind('mouseover', function() {
            //        jQuery(this).addClass('active');
            jQuery(this).find('img').stop().each(function() {
              jQuery(this).animate({
                'opacity': 1
              }, 200);
          	});
         });
    	 
    	  jQuery('.element').bind('mouseout', function() {
            //        jQuery(this).removeClass('active');
           jQuery(this).find('img').stop().each(function() {
              jQuery(this).animate({
                'opacity': 0.4
              }, 200);
          	});
         });
	*/

    // .element
    // Show hide title subtitle of frames
    // 
    $('.add').bind('mouseenter', function() {
        // $(this).find('.link-box').show();
        $(this).find('.link-box').animate({'opacity':1}, { easing:'easeInSine'} );
    });
    $('.add').bind('mouseleave', function() {
        //$(this).find('.link-box').hide();
        $(this).find('.link-box').animate({'opacity':0}, { easing:'easeOutSine'} );
    });

    jQuery('.element').bind('mouseenter', function() {
        var obj = jQuery(this),
            delay = 75;

        obj.find('.title').stop().each(function() {
            jQuery(this).animate({
            'margin-left': 35,
            'opacity': 1
            }, 250);
        });

        obj.find('.subtitle').stop().each(function() {
            jQuery(this).animate({
            'opacity': 1
            }, 0);

            jQuery(this).delay(delay).animate({
            'margin-left': 35
            }, 250);
        });

        obj.find('.subtitle2').stop().each(function() {
            jQuery(this).animate({
            'opacity': 1
            }, 0);

            jQuery(this).delay(delay).animate({
            'margin-left': 35 + jQuery(this).find('.subtitle2').width()
            }, 250);
        });

    });

    jQuery('.element').bind('mouseleave', function() {
        var obj = jQuery(this);

        obj.find('.title').stop().each(function() {
            jQuery(this).animate({
            'opacity': 0,
            'margin-left': -500
            }, 400);
        });

        obj.find('.subtitle').stop().each(function() {
            jQuery(this).animate({
            'opacity': 0,
            'margin-left': 600
            }, 400);
        });

        obj.find('.subtitle2').stop().each(function() {
            jQuery(this).animate({
            'opacity': 0,
            'margin-left': 600
            }, 400);
        });

    });
});