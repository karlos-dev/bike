/**
 * @author  Karlos Álvarez <info@weird.is>
 *
 * Form for creating Ads js actions
 * 
 * Libs
 * ----------------------------------------------------------------------
 * http://www.laktek.com/2008/10/27/really-simple-color-picker-in-jquery/
 * https://github.com/elclanrs/jq-idealforms
 * https://github.com/olance/jQuery-switchButton
 * 
 */

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

String.prototype.hashCode = function(){
	var hash = 0;
	if (this.length == 0) return hash;
	for (i = 0; i < this.length; i++) {
		char = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}

var sizes = ['large','medium','regular','small','tiny'],
	prices = {
		year:{
			large:110,
			medium:80,
			regular:50,
			small:30,
			tiny:10
		},
		month:{
			large:10,
			medium:7,
			regular:5,
			small:3,
			tiny:1
		}
	},
	
	address = 'Les+Rambles%2C+08002+Barcelona',
	timeout=false,

	_ses = {
		ad : {},
		price : {
			method : 'paypal', // DO NOT TOUCH
			period : 'month',
			ads: []
		},
		/*
			ad object {
				size: 	 string:sizetype,
				htmlad:  string:json,
				cssad: 	 string:json, 
				url: 	 string:url,
				banner:  string:url,
				address: string,
				zip: 	 integer 
			}
		 */
		adset : [], 
		ccardH : 0,
		drag : false,
		disableSession:false,
		//address : 'Les+Rambles%2C+08002+Barcelona',
		current : 'one',
		frame_url : 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.6974309594516!2d2.171654051125883!3d41.380658004273144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a258081714c9%3A0x24bef7ac8842bdc4!2s' + address + '!5e0!3m2!1sen!2ses!4v1459821680461',
		//'<iframe src="' + 
		frame : 'width="305" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>',
		last_image : '',
		last_bg_image : '',
		last_address: '',
		last_code: 0,
		buttons_row: '',
		current_class : 'large',
		title_left :  'add-pre',
		title_right : 'add-str',
		bg_h : '100%',
		bg_w : '100%',
		po_h : '92%',
		po_w : '95%',
		fn : {
			initAd: function() {

				var def_bg="#fff",
					def_font="#000",
					def_sh="#eee";

				var sh = 	{ 'text-shadow' : def_sh +' 2px 2px 6px' },
					font = 	{ 'color': def_font, 'font-family': 'Orator' },
					bg = 	{ 'background-color': def_bg,
							  'background-image':"url()",
							  'background-position':"0 0",
							  'background-size':"100% 100%"
							};

				$('.ad-box .elem-ad').css(bg);
				$('.add-str.shadow h2').css(sh);
				$('.add-pre.shadow h2').css(sh);
				$('.ad-box h2').css(font);

				_ses.ad.font = {}
				_ses.ad.obj=$('.ad-box .elem-ad');

				_ses.ad.font=font;
				_ses.ad.bg=bg;
				_ses.ad.shadow=sh;
				_ses.ad.banner="";
				_ses.ad.img={};
				_ses.ad.img.width=0;
				_ses.ad.img.height=0;
				_ses.ad.tags=[];
				_ses.ad.services=[];

				_ses.ad.frame_width=$('.ad-box').width();
				_ses.ad.frame_height=$('.ad-box').height();

				$.get("/sads", function( data ) {
					var nads=parseInt(data);
					if( nads > 0 )
						_ses.submitted=false;
				});

				//$('div.user-data').css('display','none');

				console.dir(_ses.ad);
			},
			parseable: function(str) {
				// Remove htmlad in case is stored
				var init = str.indexOf('htmlad'), clean;
				if( init !== -1 ) {
					clean = str.substring(0, init-2) + '}';
				} else
					clean = str;
				return clean;
			},
			/**
			 * Edit Ad
			 * User Área
			 */
			loadAd: function(str) {
				// Remove htmlad in case is stored
				var clean = _ses.fn.parseable(str);
				_ses.disableSession=true;
				
				console.log(clean);
				var css = JSON.parse(clean);
				console.log(css); 

				var sh = 	css.shadow,
					font = 	css.font,
					bg = 	css.bg;

				$('.ad-box .elem-ad').css(bg);
				$('.add-str.shadow h2').css(sh);
				$('.add-pre.shadow h2').css(sh);
				$('.ad-box h2').css(font);
				
				_ses.ad.obj=$('.ad-box .elem-ad');
				_ses.ad.bg=bg;
				_ses.ad.font=font;
				_ses.ad.shadow=sh;

				_ses.ad.banner=css.banner;
				_ses.ad.img=css.img;
				_ses.ad.tags=css.tags;
				_ses.ad.services=css.services;

				_ses.ad.frame_width=$('.ad-box').width();
				_ses.ad.frame_height=$('.ad-box').height();

				setTimeout( function(){

					for(var i in css.tags){
						$('input[value='+css.tags[i]+']').trigger('click');
					}

					for(var i in css.services){
						$('input[value='+css.services[i]+']').trigger('click');
					}

		          	$('input[name=title]').val(css.title).trigger('change').trigger('keyup');
		          	$('input[name=website]').val(css.website).trigger('change').trigger('keyup');
		          	$('input[name=zip]').val(zipcode).trigger('change').trigger('keyup');

		          	// $('input[name=image]').val(css.banner).trigger('change');
		          	// $('input[name=image]').sibling('.ideal-file-wrap').find('input').val(css.banner)

		          	// order is important this time
		          	$('input[name=address]').val(addresslocal).trigger('change').trigger('keyup');
		        }, 200);

				console.dir(_ses.ad);
			},
			setAd: function( attr, val ) {

				_ses.ad.obj.css(attr, val);

				//$('.ad-box .elem-ad').css( attr, obj );
				//$('.add-str.shadow h2').css(sh);
				//$('.add-pre.shadow h2').css(sh);
				//$('.ad-box h2').css(font);

				console.dir(_ses.ad);
			},
			displayResumeAd: function(obj) {
				if( obj !== undefined ) {

					var img = $(obj);
					var adbox = img.parent().parent().parent(),

						frw = $(adbox).width(),
						frh = $(adbox).height(),
						orig_frw = img.attr('data-frame-x'),
						orig_frh = img.attr('data-frame-y'),
						posx = img.attr('data-pos-x'),
						posy = img.attr('data-pos-y'),
						ratio = img.attr('data-ratio');

					var cur_posx = (frw * posx)/orig_frw,
						cur_posy = (frh * posy)/orig_frh,
						cur_w = ( frw * ( _ses.ad.img.width ) ) / orig_frw,
						cur_h = ( frh * ( _ses.ad.img.height ) ) / orig_frh,

						h2text = adbox.find('h2'),
						fsz = ( 26* frh ) / orig_frh,
						lhe = ( 30* frh ) / orig_frh;

					h2text.css({
						"font-size": parseFloat(fsz,1)+'px',
						"line-height": parseFloat(lhe,1)+'px',
					});

					img.css({
						"position":"relative",
						"height":cur_h,
						"width":cur_w,
						"left":cur_posx,
						"top":cur_posy
					});
				}
				else {
					$('.status .ad-box img.top-img').each( function(i, e){

						var img = $(e), window_ratio=4;

						if( img.attr('rendered') === undefined ) {

							var adbox = img.parent().parent().parent(),

								frw = $(adbox).width(),
								frh = $(adbox).height(),
								orig_frw = img.attr('data-frame-x'),
								orig_frh = img.attr('data-frame-y'),
								posx = img.attr('data-pos-x'),
								posy = img.attr('data-pos-y'),
								ratio = img.attr('data-ratio');

							$(adbox).animate({ width:frw/window_ratio, height: frh/window_ratio }, 200, function(){

								frw=frw/window_ratio;
								frh=frh/window_ratio;

								console.dir( frw + " " + frh + " " + orig_frw + " " + orig_frh + " " + posx + " " + posy + " " + ratio );

								// Avoid resize ad when prev/next buttons are clicked
								img.attr('rendered', 1);

								var cur_posx = (frw * posx)/orig_frw,
									cur_posy = (frh * posy)/orig_frh,
									cur_w = parseInt( ( frw * ( img.width() ) ) / orig_frw),
									cur_h =  parseInt( ( frh * ( img.height() ) ) / orig_frh),

									// Not need to do this
									//cur_w_comp =  parseInt(( frw * ( img.width()/ratio ) ) / orig_frw),
									//cur_h_comp =  parseInt(( frh * ( img.height()/ratio ) ) / orig_frh),

									h2text = adbox.find('h2'),
									fsz = ( 26* frh ) / orig_frh,
									lhe = ( 30* frh ) / orig_frh;

								h2text.css({
									"font-size": parseFloat(fsz,1)+'px',
									"line-height": parseFloat(lhe,1)+'px',
								});

								img.css({
									"position":"relative",
									"height":cur_h,
									"width":cur_w,
									"left":cur_posx,
									"top":cur_posy
								});

								// Show hidden ad resume elements
								$('.board .status').removeClass('hidden');
							});
						}
					});
				}
			},
			// Show hide ad options depending on ad class
			modifyOptions:function( item ){

				switch(item){
					case 'small': { 
						$('.s2').slideUp();
						$('.s:not(.s2)').slideDown();
						//_ses.last_bg_image = $('.ad-box .elem-ad').css('background-image');
						//_ses.last_image = $('.ad-box .elem-ad img').attr('src');

						//$('.ad-box .elem-ad').css('background-image', "url()");
						$('.ad-box .elem-ad img').attr('src', "");
					} break;

					case 'tiny': { 
						$('.s2').slideUp();
						$('.s').slideUp();
						// _ses.last_bg_image = $('.ad-box .elem-ad').css('background-image');
						// _ses.last_image = $('.ad-box .elem-ad img').attr('src');

						$('.ad-box .elem-ad').css('background-image', "url()");
						$('.ad-box .elem-ad img').attr('src', "");
					} break;

					default:{
						$('.s2').slideDown();
						$('.s').slideDown();
						$('.ad-box .elem-ad').css('background-image', _ses.last_bg_image );
						_ses.fn.displayResumeAd( $('.ad-box .elem-ad img').attr('src', _ses.last_image ) ); //.attr('src', _ses.last_image );

					}
				}
			},
			updateTextClass:function( e ){
				var clas;
				if( $( e.currentTarget ).html() == options.on_label ) {
					clas =_ses.title_right;

					$('.ad-box .shadow').addClass(_ses.title_right);
					$('.ad-box .shadow').removeClass(_ses.title_left);
					
				} else {
					clas =_ses.title_left;

					$('.ad-box .shadow').addClass(_ses.title_left);
					$('.ad-box .shadow').removeClass(_ses.title_right);
					
				}

				$('input[name=imgclass]').val(clas);
			},
			generateMap:function( addr, w, h ){
				// Info --> http://stackoverflow.com/a/36801105/5476782
				var wid = (w == undefined) ? 241 : w,
					hei = (w == undefined) ? 200 : h;

				var embed= "<iframe " + 
	    			"width='" + wid + "' height='"+ hei + "'" +  
	    			"frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q="+   
		        	encodeURIComponent( addr ) +  
		        	"&amp;output=embed'></iframe>";  

				$('.map').html( embed );
				timeout=false;
			},
			formatAddressForMap:function( addr ){
				addr = addr.replace(/ /g, "+");
				//addr = addr.replace(/,/g, "%2C");
				return addr;
			},
			activateStatus: function(){
				var status = $('.status'),
					pre = $('.pre');
				if( ! status.hasClass('active') ) {
	    			status.addClass('active');
	    			pre.addClass('active');
				}
			},
			resumeAd: function(o){
				if( typeof o !== typeof {} )
					return;

				var payment = '<form class="idealforms"><div class="field radio"><p class="group position temp">' +
      							'<label><input name="pos" type="radio" value="month">1 Month</label><label><input name="pos" type="radio" value="year">1 Year</label>' +
    						'</p><span class="error"></span></div></form>',
    				adsize = ( typeof o.size !== typeof undefined ) ? o.size : o.class,
    				price = prices[_ses.price.period][ adsize ];

				console.log(o);

				var key= String(o.title.hashCode()).replace("-", ""),
					resline = '<div data-hash="' + key +  '" class="resume-item"><div class="rem"></div><a href="http://'+window.location.host + '/' + o.uri +'"><span>'
						 + o.title.substring(0, 30) + ".." +'</span></a><span class="size">'+ adsize +'</span>' 
						+ '<div style="text-align:right;padding-right:1.25rem;">' 
						 + payment 
						 + '<span class="price">' + price + '</span><span class="currency">€</span></div>'  
						+ '</div>';

				_ses.price.ads.push( { hash: o.hashkey, size: o.size, price: price, period:_ses.price.period, title:o.title } );

				return resline;
			},
			showResumeStep: function(){
				$('form.ad-data').slideUp();
				$('.ad-select').slideUp();
	    		$('div.user-data').show();

    			$('.mystuff').datepicker({
    					showButtonPanel: true,
						changeMonth: true,
						changeYear: true,
						dateFormat: 'mm/yy',
						showOptions: { direction: "up" },
						showOn: 'button',
						onClose: function(dateText, inst) { 
							var d = new Date(inst.selectedYear, inst.selectedMonth, 1), month = (inst.selectedMonth < 10) ? "0"+inst.selectedMonth : inst.selectedMonth;
							console.dir(inst);
				            $(this).datepicker('setDate', d);

				            $('input[name=expire]').val( month  +"/"+ inst.selectedYear);
				        }
					} 
			    );

    			// DO NOT TOUCH
			    $('form.user-data').idealforms('toggleFields', 'cardholder cardnumber cardcsv expire' );

				var current  = $('.mine.current'),
	    			next = $('.mine.three'),
	    			line="",
	    			resline="",
	    			showPay=true;

	    		$.get("/sads", function( data ) {

					var nads=parseInt(data), found, key, payment, boardHtml;

					$.get('/js/templates/adstatus.mst', function(template){ 

						boardHtml = $('.board').html();

						if( nads > 0 ) { 
							for ( var i=1; i <= nads; i+=1 ) {
								$.get("/sads/"+i, function( data ) {

									found=false;
									o = JSON.parse(data);
									key= String(o.title.hashCode()).replace("-", "");

									o.hashkey = key;
									o.format_address= _ses.fn.formatAddressForMap(o.address);

									if( $('.ad-resume ul').html().indexOf( key ) == -1 ) {

										resline = _ses.fn.resumeAd(o);
										$('.ad-resume ul').append( resline );
									}

									if( boardHtml.indexOf( key ) == -1 && line.indexOf( key ) == -1 ) { 
										line += Mustache.render( template, o );
									}
								});
							}

							setTimeout( function(){
								$('.ad-resume form.idealforms').idealforms();
								//$('.ui-datepicker-calendar').css('display', 'none');
							}, 150);
						}

						$('.pre').slideUp( function() {
							// No repeat + no re-render --> Moved up
							// if( $('.board').html().indexOf( key ) == -1 ) {
							//$('.board .status.height .elem-ad').hide();
							$('.board').append(line);
							// Wait for the line to be available
							setTimeout( function(){
								_ses.fn.displayResumeAd();
								
							}, 150);
							// }
						});

						// Can add another if added one before
						//if( _ses.adset.length > 0 ) {
						//	$('button.other').fadeIn();
						//	showPay=true;
						//} 
						
						if(showPay) {
							// clean adset from crap
							var obj = jQuery.extend(true, {}, _ses.ad );
							delete obj.obj;
							delete obj.htmlad;
							$('input[name=wax]').val( JSON.stringify( obj ) );
						}
						
						$(current).removeClass('current');
						$(next).addClass('current');
					});

				});
			},
			proceedButtonDefault: function () {
				$('.field.buttons').find('img').remove();
				var obj = {
					'color': '#00E009',
					'border': '1px solid #00E009'
				};
				$('.field.buttons button[type=submit]').css( obj );
			},
			proceedButtonLoading: function () {
				var obj = {
					'color': '#fff',
	    			'border': '1px #7D7C7C solid'
				};
				// Before request
				$('.field.buttons').append('<img class="loader" title="Loading" src="/images/gears.gif" alt="Loading, plase wait...">');
	      		$('.field.buttons button[type=submit]').css( obj );
			},
			proceedButtonError: function () {
				$('.field.buttons').find('img').remove();
				var obj = {
					'color': 'red',
					'border': '1px solid red'
				};
				$('.field.buttons button[type=submit]').css( obj ).html('Error 500');
			},
		}
	}, anim, bounce;

$(window).on('load', function(){
	
	$('.home').show();

	if( typeof _ul !== typeof undefined && _ul==true ) {
		console.log('User is registered');
	}

	bounce = new Bounce();
	bounce2 = new Bounce();
	
	// HACK
	_ses.ccardH = $('#container').innerHeight()-400;

	/*
	.translate({
		from: { x: -300, y: 0 },
		to: { x: 0, y: 0 },
		duration: 600,
		stiffness: 4
	})
	*/
	bounce.scale({
		from: { x: 1, y: 1 },
		to: { x: 0.1, y: 0.1 },
		easing: "sway",
		bounces: 8
	})
	.scale({
		from: { x: 1, y: 1 },
		to: {   x: 1.5, y: 1.2 },
		easing: "sway",
		duration: 300,
		delay: 30,
		stiffness: 4,
		bounces: 5
	});

	bounce2.scale({
		from: { x: 1, y: 1 },
		to: {   x: 1.5, y: 1.2 },
		easing: "sway",
		duration: 500,
		delay: 10,
		stiffness: 1,
		bounces: 6
	});

	anim = new Bounce();
	
	anim
	/*
	.scale({
		from: { x: 1, y: 1 },
		to: {   x: 2, y: 2 },
		easing: "sway",
		duration: 500,
		delay: 5,
		stiffness: 1
	})*/
	.scale({
		from: { x: 1, y: 1 },
		to: {   x: 2, y: 2 },
		easing: "sway",
		duration: 700,
		delay: 150,
		stiffness: 1,
		bounces: 6
	})
	/*
	.scale({
		from: { x: 1, y: 1 },
		to: {   x: -0.5, y: -0.5 },
		easing: "sway",
		duration: 300,
		delay: 30,
	});*/
	//.applyTo(document.querySelectorAll(".elem-ad"));

	
	bounce.applyTo( $(".elem-ad") ).then(function() { 
		bounce.remove();
	});

	if( typeof _noInit == typeof undefined ) {
		$('div.user-data').hide();
		
		_ses.fn.initAd();
		_ses.fn.modifyOptions( _ses.current_class );

	} else if( _noInit == 'activate' ) {
		var ad;

		$('.ccard').hide();
		$('.ppal').hide();

        _ses.fn.initAd();

        if( typeof _adobj === typeof 'string' ) {
        	ad = JSON.parse( _ses.fn.parseable(_adobj) );
        	console.log(ad);

        	// Fixing structure
        	ad.size=ad.class;
        }

        $('.ad-resume ul').append( _ses.fn.resumeAd( ad ) );
        var obj = jQuery.extend(true, _ses.price.ads[0], ad );

        console.log(obj);

        $('input[name=paylog]').val( JSON.stringify( _ses.price.ads ) );

        setTimeout( function(){
			$('.ad-resume form.idealforms').idealforms();
		}, 150);
		// _ses.fn.modifyOptions( _ses.current_class );
	} else {
		$('div.user-data').hide();
		_ses.fn.loadAd(cssobj);
	}
	
	var obj,color;

	if( $('#font-co').length ) {
		$('#font-co').colorPicker({ 
			pickerDefault: "ffffff", 
			colors: ["ffffff", "000000", "ccc", "eee", "535353", "dedede", "00E009", "transparent"], 
			transparency: true,
			showHexField: false
		}); 
	}

	if( $('#bg-co').length ) {
		$('#bg-co').colorPicker({ 
			pickerDefault: "ffffff", 
			colors: ["ffffff", "000000", "ccc", "eee", "535353", "dedede", "transparent"], 
			transparency: true,
			showHexField: false
		}); 
	}

	if( $('#sh-co').length ) {
		$('#sh-co').colorPicker({ 
			pickerDefault: "ffffff", 
			colors: ["ffffff", "000000", "ccc", "eee", "535353", "dedede", "transparent"], 
			transparency: true,
			showHexField: false
		}); 
	}
})

$('.move').on('click', function(){

	bounce3 = new Bounce();
	bounce3.scale({
		from: { x: 1, y: 1 },
		to: {   x: 1.3, y: 1.2 },
		easing: "sway",
		duration: 800,
		delay: 10,
		stiffness: 1,
		bounces: 8
	});

	bounce3.applyTo( $('button[type=submit]') ).then(function() { 
		bounce3.remove();
	});
	
});

/**
 * 	Select Ad class
 * 	Fires when a change occurs in the select option class
 * 	ads.php
 */
$('.adclass').change( function(){ 

	var o=$(this);
	// Animate then change class
	
	anim.applyTo( $(".elem-ad") );
	
	//setTimeout( function(){  
		var obj;
		if(_sizes) {
			obj = JSON.parse(_sizes)
		}

		if( $('.ad-box').hasClass( _ses.current_class ) ) {
			$('.ad-box').removeClass(_ses.current_class);
		}

		_ses.current_class = obj[ o.val()];
		$('.ad-box').addClass( obj[ o.val()] );

		_ses.fn.modifyOptions( _ses.current_class );

		_ses.ad.frame_width=  $('.ad-box').width();
		_ses.ad.frame_height= $('.ad-box').height();

	anim.applyTo( $(".elem-ad") );

	//}, 150 );
});

/**
 * 	Input title
 * 	Fires when a user press keyboard on title input
 * 	Step 1/3
 * 	
 * 	ads.php
 */
$("input[name=title]").keyup(function (e) {
    if (e.keyCode == 13) {
        // Do something
    }
    var txt = $('.ad-box h2'),
    	title = $('.status .title'),
    	urlset = $('.status .url-resume ul').first(),
    	any, 
    	url="";

    if( $(this).val().length > 3 ) {

    	$('.url-resume').show();

    	url = "www.bikerent.barcelona/" + $(this).val().replace(/ /g, '-');
    }

    any = $(urlset).children('li.url').first();
    if( any == undefined ) {
    	$(urlset).append('<li class="url">'+url+'</li>');
    } else {
    	$(any).html( url );
    }

    txt.html( $(this).val() );
    title.html( $(this).val() );

    _ses.ad.title=$(this).val();

    bounce2.applyTo( txt );
    bounce.applyTo( title );

    _ses.fn.activateStatus();

});

$("input[name=website]").keyup(function (e) {
    if (e.keyCode == 13) {
        // Do something
    }

    $('.status .website').html( $(this).val() );
    $('.status .website').attr( "href", "http://" + $(this).val() );
    $('.ad-box a.adlnk').attr( "href", "http://" + $(this).val() );

    _ses.ad.website=$(this).val();
    _ses.fn.activateStatus();
});

$("input[name=address]").keyup(function (e) {
    if (e.keyCode == 13) {
        // Do something
    }
    _ses.last_address = $(this).val();
    $('.status .address').html( _ses.last_address );

    _ses.fn.activateStatus();

    setTimeout( function() {
    		// Lock map generation till zip is filled
    		if ( timeout || $('.status .zip').html().length < 5 )
    			return;

    		timeout=true;
    		// address = _ses.fn.formatAddressForMap( _ses.last_address );
    		// console.log( address );
    		console.log( _ses.last_address );
    		// $('.status').css('min-height','210px');
    		
    		// $('.status').addClass('min');
    		_ses.fn.generateMap( _ses.last_address );
    }, 100);
});

$("input[name=zip]").keyup(function (e) {
    if (e.keyCode == 13) {
        // Do something
    }
    
    _ses.last_code = parseInt( $(this).val() );
    $('.status .zip').html( $(this).val() );
    _ses.fn.activateStatus();

    if( $(this).val().length >= 5 ) {
    	
    	if( $("input[name=address]").val() == "" ) 
    		return;

    	$('.status .map').html('<img src="/images/loading.gif" alt="loading..">');
    	setTimeout(function() {
    		// address = _ses.fn.formatAddressForMap( _ses.last_address );
    		// console.log( address );
    		console.log( _ses.last_address );
    		// $('.status').css('min-height','210px');
    		$('.status').addClass('min');
    		_ses.fn.generateMap( _ses.last_address );
    	}, 100);
    }
});


var options = { 
		on_label: "RIGHT",            // Text to be displayed when checked
		off_label: "LEFT",
		checked:true
	},
	options_img = { 
		on_label: "HEIGHT",            // Text to be displayed when checked
		off_label: "WIDTH",
		checked:false 
	},
	options_img_side = { 
		on_label: "Left",            // Text to be displayed when checked
		off_label: "Right",
		checked:false 
	};

$(".txt .sb:checkbox").switchButton(options);
$(".img .sb:checkbox").switchButton(options_img);
$(".img-side .sb:checkbox").switchButton(options_img_side);

/**
 * @updated bike_1.1
 * 
 * Fires when the text switch is clicked
 * 	- Updates text class and 
 * 	- Modifies text position in add preview
 */
$('.txt span.switch-button-label').on('click', function(e){
	// var obj = $(this).parent().find('.sb');
	_ses.fn.updateTextClass(e);
	// console.log( $(obj).val() + ' ' + $(this).hasClass('on') );
});

$(".txt div.switch-button-button").on('click', function(e){
	_ses.fn.updateTextClass(e);
});

/**********************
 * IMG POSITION TOOLS
 ***********************/ 

$('.img span.switch-button-label').on('click', function(e){
	var obj = $(this).parent().find('.sb'),
		thing = '<span class="switch-button-label reset">reset</span>';

	if( ! $(this).parent().find('.reset').length ) { 
		$('.switch-wrapper.img').append(thing);
	}
	
	// HEIGHT
	if( $( e.currentTarget ).html() == options_img.on_label ) {

		if( $(".img-side").hasClass('hidden') ) {
			$(".img-side").removeClass('hidden');
			//$(".img-side .sb:checkbox").switchButton(options_img_side);
		}
		
		opts = { 
			'background-size': "auto 100%",
			'background-position': "0 0"
		}
		_ses.ad.bg['background-position']=opts['background-position'];
		_ses.ad.bg['background-size']=opts['background-size'];
		//$('.ad-box .elem-ad').css('background-size', "auto 100%");
		_ses.ad.obj.css(opts); 
	
	// WIDTH
	} else {
		//$('.ad-box .elem-ad').css('background-size', _ses.bg_w+" auto");
		opts = { 
			'background-size': "100% auto",
			'background-position': "0 0"
			//'background-position': margin+"px " + margin + "px"
		}
		_ses.ad.bg['background-position']=opts['background-position'];
		_ses.ad.bg['background-size']=opts['background-size'];
		_ses.ad.obj.css(opts); 
	}
});

$('.img div.switch-button-button').on('click', function(e){
	var obj = $(this).parent().parent().find('.sb'),
		thing = '<span class="switch-button-label reset">reset</span>',
		opts;

	if( ! $(this).parent().find('.reset').length ) { 
		$('.switch-wrapper.img').append(thing);
	}
	
	// HEIGHT
	if( $( e.currentTarget ).html() == options_img.on_label ) {
		
		if( $(".img-side").hasClass('hidden') ) {
			$(".img-side").removeClass('hidden');
			//$(".img-side .sb:checkbox").switchButton(options_img_side);
		}

		opts = { 
			'background-size': "auto 100%",
			'background-position': "0 0"
		}
		_ses.ad.bg['background-position']=opts['background-position'];
		_ses.ad.bg['background-size']=opts['background-size'];
		//$('.ad-box .elem-ad').css('background-size', "auto 100%");
		_ses.ad.obj.css(opts); 
	
	// WIDTH
	} else {
		//$('.ad-box .elem-ad').css('background-size', _ses.bg_w+" auto");
		opts = { 
			'background-size': "100% auto",
			'background-position': "0 0"
			//'background-position': margin+"px " + margin + "px"
		}
		_ses.ad.bg['background-position']=opts['background-position'];
		_ses.ad.bg['background-size']=opts['background-size'];
		_ses.ad.obj.css(opts); 
	}

	//console.log( $(obj).val() + ' ' + $(this).hasClass('on') );
});

$('.img-side .switch-button-label').on('click', function(e) {

	// right
	if( $( e.currentTarget ).html() == options_img_side.on_label ) {

		//$(".img-side").removeClass('hidden');
		//$(".img-side .sb:checkbox").switchButton(options_img_side);
		
		opts = { 
			//'background-size': "auto 100%",
			'background-position': "center right"
		}
		_ses.ad.bg['background-position']=opts['background-position'];

		// _ses.ad.bg['background-size']=opts['background-size'];
		// $('.ad-box .elem-ad').css('background-size', "auto 100%");
		_ses.ad.obj.css(opts); 
	
	// left
	} else {
		//$('.ad-box .elem-ad').css('background-size', _ses.bg_w+" auto");
		opts = { 
			//'background-size': "100% auto",
			'background-position': "center left"
			//'background-position': margin+"px " + margin + "px"
		}
		_ses.ad.bg['background-position']=opts['background-position'];
		//_ses.ad.bg['background-size']=opts['background-size'];
		_ses.ad.obj.css(opts); 
	}
});

$('.switch-wrapper').on('click', '.reset', function(e){
	//$('.ad-box .elem-ad').css('background-size', _ses.bg_w+" "+_ses.bg_h);
	_ses.ad.obj.css( { 'background-position':"0 0", 'background-size': _ses.bg_w+" "+_ses.bg_h } ); 
	_ses.ad.bg['background-position']="0 0";
	_ses.ad.bg['background-size']=_ses.bg_w+" "+_ses.bg_h;
});

/**
 * Change image position
 * 
 * Fires when background or portrait radio buttons are clicked
 * 
 */
$('.tools').on('click', '.ideal-radio', function(e){

	var elem = $(this).siblings('input').val(),
		grandpa = $(this).parent().parent().parent().parent(),
		opts = {};
	
	var ad_resume_item = $(grandpa).parent().parent().attr('data-hash');

	switch(elem){
		case 'prt': { 
			var x = $('.ad-box .elem-ad').width(),
			 	y = $('.ad-box .elem-ad').height(),
			 	margin=5;

			//alert(x+'x'+y);

			opts = { 
				'background-size': (x-2*margin)+"px "+(y-2*margin)+"px",
				'background-position': margin+"px " + margin + "px"
			}

			_ses.ad.obj.css(opts); 
			_ses.ad.bg['background-position']=opts['background-position'];
			_ses.ad.bg['background-size']=opts['background-size'];

			console.dir(_ses.ad);

		} break;

		case 'bgr': { 
			opts = { 
				'background-size': _ses.bg_w+" "+_ses.bg_h,
				'background-position': "0 0"
			}	
			//$('.ad-box .elem-ad').css(opts); 
			_ses.ad.obj.css(opts); 
			_ses.ad.bg['background-position']=opts['background-position'];
			_ses.ad.bg['background-size']=opts['background-size'];
			console.dir(_ses.ad);
		} break;

		case 'frt': { 
			opts = { 
				'z-index': 2
			}	
			_ses.ad.obj.css(opts); 
			_ses.ad.bg['z-index']=opts['z-index'];

		} break;

		case 'beh': { 
			opts = { 
				'z-index': -1
			}	
			_ses.ad.obj.css(opts); 
			_ses.ad.bg['z-index']=opts['z-index'];
			
		} break;

		case 'month': { 
			_ses.price.period='month';
			console.log("month")
			$('.ad-resume div').each( function(i, e){ 
				var size = $(grandpa).parent().siblings('.size').html();
				var price = prices[_ses.price.period][size];
				$(grandpa).siblings('.price').html(price);

				for ( var i in _ses.price.ads ) {
					if( _ses.price.ads[i].hash == ad_resume_item ) {
						_ses.price.ads[i].price=price;
						_ses.price.ads[i].period=_ses.price.period;
						$('input[name=paylog').val( JSON.stringify(_ses.price.ads) );
						break;
					}
				}
			});

		} break;

		case 'year': { 
			_ses.price.period='year';
			console.log("year")
			$('.ad-resume div').each( function(i, e){ 
				var size = $(grandpa).parent().siblings('.size').html();
				var price = prices[_ses.price.period][size];
				$(grandpa).siblings('.price').html(price);
				for ( var i in _ses.price.ads ) {
					if( _ses.price.ads[i].hash == ad_resume_item ) {
						_ses.price.ads[i].price=price;
						_ses.price.ads[i].period=_ses.price.period;
						$('input[name=paylog').val( JSON.stringify(_ses.price.ads) );
						break;
					}
				}
			});

		} break;

		case 'ppal': { 

			if( _ses.price.method == 'card' ) {
				$('form.user-data').idealforms('toggleFields', 'cardholder cardnumber cardcsv expire' );
				console.log("toggle once: cancel");
			}

			$('#container').innerHeight(_ses.ccardH);

			_ses.price.method = 'paypal';

			console.log(_ses.price.method)

			$('.pmnt.ccard').slideUp();
			$('.pmnt.ppal').slideDown();
			
			//$('form.user-data').idealforms('removeFields', 'cardholder cardnumber' );

		} break;

		case 'ccard': { 

			if( _ses.price.method == 'paypal' ) {
				$('form.user-data').idealforms('toggleFields', 'cardholder cardnumber cardcsv expire' );
				console.log("toggle twice: active");
			}

			var totalW=0;

			$('.ccard').each(function(i, e){
				totalW+=$(e).height();
			});
			//_ses.ccardH = $('#container').innerHeight();

			$('#container').height(( (totalW/2)+_ses.ccardH));

			_ses.price.method = 'card';

			console.log(_ses.price.method);

			$('.pmnt.ppal').slideUp();
			$('.pmnt.ccard').slideDown();
			//$('form.user-data').idealforms('addFields', 'cardholder cardnumber' );

		} break;

		default: { }
	}
});


$('.mine').on('click', function(){
	if( $(this).hasClass('one') ) {
		$('form.idealforms').idealforms('goToStep', 0);
	} else if ( $(this).hasClass('two') ) {
		$('form.idealforms').idealforms('goToStep', 1);
	} else if ( $(this).hasClass('three') ) { 
		$('form.idealforms').idealforms('goToStep', 2);
	}

	$('.mine').removeClass('current');
	$(this).addClass('current');
})

/**
 * 	IDEALFORMS
 * 	
 * 	Form declaration
 * 		Fields
 * 		Error Messages 
 */

$('form.idealforms.ad-data').idealforms({

	silentLoad: true,
    rules: {
        'title': 	'required ajax',

        'address': 	'required address',
        'zip': 	    'required',

        'image': 'extension:jpg:png',
        'website': 'url',

    },

    errors: {
        'title': {
          	ajaxError: 'Ad title invalid or not available',
          	'required': 'Ad must have a title',
        }, 

        'address': {
        	'required': 'Address for the Map Search'
        }
    },

    onValidate: function(invalid, e) { 

    },

    onSubmit: function(invalid, e) {
      	e.preventDefault();

        if (invalid) {

			$('#invalid').show().toggleClass('valid', ! invalid).text(invalid ? (invalid +' invalid fields') : 'All good!');

		} else {
		
			var _last = _ses.adset.pop();
			if( _last ) {
				last = {
					size: _last.size,
					htmlad: _last.htmlad,
					cssad: _last.cssad,
					title: _last.title,
					url: _last.url,
					banner: _last.banner,
					address: _ses.last_address,
					zip: _ses.last_code
				};
				_ses.adset.push( last );
			}
			
			var obj = jQuery.extend(true, {}, _ses.ad );
			delete obj.obj;
			delete obj.htmlad;

			obj.size= _ses.current_class;
			obj.htmlad= $('.board .pre').html();
			obj.cssad= JSON.stringify( obj );
			obj.title= _ses.ad.title;
			obj.hash=String(_ses.ad.title.hashCode()).replace("-", "");
			obj.url= _ses.ad.website;
			obj.banner= _ses.ad.banner;
			obj.address= _ses.last_address;
			obj.zip= _ses.last_code; 

			//OK console.log( $("input[name='tags[]']").val() );
			//OK console.log( $("input[name='services[]']").val() );

			if( !_ses.disableSession )
				$.ajax( {
					type: "POST",
			        url: "/sads",
			        data: { "str" : JSON.stringify( obj ), "saveInSession" : true },
			        success: function (response) {
			        	if( response && parseInt(response) > 0 ) {

			        		// Review HACK
			        		// _ses.submitted=true;
			        		
			        		_ses.fn.showResumeStep();
			        	}
			        	console.log('Response');

			    	}
				});
			else
				$.ajax( {
					type: "POST",
			        url: "/ads",
			        data: { "str" : JSON.stringify( obj ), "saveInSession" : false, "update": true, 'timestamp':_timestamp },
			        success: function (response) {
			        	if( response && parseInt(response) > 0 ) {

			        		// Review HACK
			        		// _ses.submitted=true;
			        		
			        		_ses.fn.showResumeStep();
			        	}
			        	console.log('Response');

			    	}
				});
		}  
    } 
});

/**
 * tag or service options click
 *
 *  - Beware lower or uppercase !!!
 *
 *	Updates _ses.ad object with the clicked element 
 */
$('span.ideal-check').on('click', function(){

	var p = $(this).parent().find('input').attr('name'),
		urlset = $('.status .url-resume ul').first(),
		url = "www.bikerent.barcelona/" + $(this).parent().text().replace(/ /g, '-').toLowerCase() + "/" + _ses.ad.title.replace(/ /g, '-');

	if( p.indexOf('services') !== -1 ) {
		if( $(this).hasClass('checked') ) {
			var arr = [], elem;
			for( var i in _ses.ad.services ) {
				elem=_ses.ad.services.pop();
				if( elem !== $(this).parent().text().toLowerCase() ) {
					arr.push(elem)
				} 
			}
			_ses.ad.services=arr;
			$('.status .services li:contains("' + $(this).parent().text() + '")').remove();
			//$('.status .url-resume ul li:contains("' + $(this).parent().text() + '")').remove();
			//alert('Now is checked, remove service? ' + $(this).parent().text() );

		} else {
			_ses.ad.services.push( $(this).parent().text().toLowerCase() );
			$('.status .services').append('<li >' + $(this).parent().text() + '</li>' );
			// $(urlset).append('<li class="url">'+url+'</li>');
		}
		
	} else if ( p.indexOf('tags') !== -1 ) { 
		if( $(this).hasClass('checked') ) {
			var arr = [], elem;
			for( var i in _ses.ad.services ) {
				elem=_ses.ad.tags.pop();
				if( elem !== $(this).parent().text().toLowerCase() ) {
					arr.push(elem)
				}
			}
			_ses.ad.tags=arr;
			$('.status .tags li:contains("' + $(this).parent().text() + '")').remove();
			$('.status .url-resume ul li:contains("' + $(this).parent().text().toLowerCase() + '")').remove();

			//alert('Now is checked. remove tag? ');
		} else {
			_ses.ad.tags.push( $(this).parent().text().toLowerCase() );
			$('.status .tags').append('<li >' + $(this).parent().text() + '</li>' );
			$(urlset).append('<li class="url">'+url+'</li>');
		}
	}
});


$('form.idealforms.user-data').idealforms({
	silentLoad: true,
    rules: {
        'name':  	'required ajax',
        'email': 	'required email ajax',
        'password': 'required pass',
        'cardholder': 'required',
        'cardnumber': 'required ajax',
        'expire': 'required',
        'cardcsv': 'required min:3 max:3'
    },
    errors: {
        'name': {
        	'required': 'Please, enter your name'
        },
        'email': {
        	ajaxError: 'Already registered email',
        	'required': 'Please, enter your email'
        },
        'password': {
        	'required': 'Password cannot be empty'
        },
        'cardholder': {
        	'required': 'The card holder full name'
        },
        'cardnumber': {
        	'required': 'Enter credit card number',
        	ajaxError: 'Not a valid credit card number',
        },
        'cardcsv': {
        	'required': 'The csv number is required'
        },
        'expire': {
        	'required': 'Expiration date mm/yyyy'
        }
    },
    onValidate: function(invalid, e) { 
    },
    onSubmit: function(invalid, e) {
      	e.preventDefault();
      	_ses.buttons_row = $('.field.buttons').html();

        if (invalid) {
			$('#invalid').show().toggleClass('valid', ! invalid).text(invalid ? (invalid +' invalid fields') : 'All good!');
		} else {

			_ses.fn.proceedButtonLoading();

			$.ajax( {
				type: "POST",
		        url: "/ads",
		        data: $('form.idealforms').serialize(),
		        cache: false,
		        success: function (response) {
		        	console.log("success");
		        	var json = JSON.parse(response);
		        	if( typeof(json) == undefined ) {
		        		//return;
		        	} else {
		        		if( json.status == 'error' ) {
		        			$('.fback').addClass('err');
		        			$('.fback').html('<p>'+'There was an error'+'</p>');
		        		} else {
		        			$('.fback').html('<p>'+json.message+'</p>');
		        			setTimeout( function(){ window.location = json.redir }, 1200 );
		        		}
		        	}
		        	setTimeout( function(){ _ses.fn.proceedButtonDefault(); }, 400 );
		        	
		    	},
		    	error: function(){
		    		console.log("error");
		    		_ses.fn.proceedButtonError();
		    	}
		    	
			});
		}  
    } 
});


/**
 * Card Payment
 * Calendar show/hide
 * 
 *
 */
$('.calen').on('click', function(){
	if( $(this).hasClass('active') ) {
		$('.mystuff').datepicker("hide");
		$(this).removeClass('active');
	}
	else {
		$('.mystuff').datepicker("show");
		$(this).addClass('active');
	}
});

$('.mystuff').on('click', function(e){ 
	e.preventDefault();
	return;
})

$('.next').click(function(){
	$('.next').show();
	$('form.idealforms').idealforms('nextStep');
	/*var current = $('.mine.current');

	_ses.current = (_ses.current == 'one') ? 'two' : (_ses.current == 'two') ? 'three' : _ses.current;
	var next = $('.'+_ses.current);
	$(current).removeClass('current');
	$(next).addClass('current');*/
});

$('.back').click( function() { 

	$('div.user-data').hide();
	$('form.ad-data').slideDown();
	$('.pre').slideDown();
	$('.ad-select').slideDown();
});

$('.prev').click(function(){
	$('.prev').show();
	$('form.idealforms').idealforms('prevStep');

	var current = $('.mine.current');

	$('.pre').slideDown();
	$('.status.height').remove();

	_ses.current = (_ses.current == 'two') ? 'one' : (_ses.current == 'three') ? 'two' : _ses.current;
	var prev = $('.'+_ses.current);

	// Re enable dragging on edit-ad form step:(one)
	if( _ses.current == 'one' )
		_ses.ad.obj.find('.top-img').draggable('enable');

	// Re-set ad original side when coming back from ad checkout step
	//else if( _ses.current == 'two' )
	//	_ses.fn.displayAd();

	$(current).removeClass('current');
	$(prev).addClass('current');
});

/**
 * Step 1 NEXT
 * 
 * Fires when the next button is clicked during step 1
 */
$('.next.step-one').click(function(){
	//$('.next').show();
	//$('form.idealforms').idealforms('nextStep');
	var current = $('.mine.current');
	_ses.current = 'two';
	_ses.ad.obj.find('.top-img').draggable('disable');

	$('.pre').slideDown();

	_ses.ad.img.height =_ses.ad.obj.find('.top-img').css('height');
	_ses.ad.img.width =_ses.ad.obj.find('.top-img').css('width');
	_ses.ad.img.left =_ses.ad.obj.find('.top-img').css('left');
	_ses.ad.img.top =_ses.ad.obj.find('.top-img').css('top');

	// Saving ad css style into form
	var newObject = jQuery.extend(true, {}, _ses.ad );
	console.dir(newObject);
	delete newObject.obj;
	delete newObject.htmlad;
	//$('input[name=cssad]').val( JSON.stringify( newObject ) );
	console.dir(_ses.ad);

	//Updating current 
	var position = _ses.adset.length;
	if( position == 0 && _ses.ad.title !== "" && _ses.ad.title !== undefined )
		_ses.adset.push( {
			// Now
			size: _ses.current_class,
			htmlad: $('.board .pre').html(),
			cssad: JSON.stringify( newObject ),
			title: _ses.ad.title,
			url: _ses.ad.website,
			banner: _ses.ad.banner,
			address: "",
			zip: 0
		} );
	else if( position > 0 ) {
		var _last = _ses.adset.pop();

		if( _last && _ses.ad.title !== "" && _ses.ad.title !== undefined && _last.address !== "" && _last.address !== undefined && _last.zip !== "" && _last.zip !== undefined) {
		
			last = {
				// New
				size: _ses.current_class,
				htmlad: $('.board .pre').html(),
				cssad: JSON.stringify( newObject ),
				title: _ses.ad.title,
				url: _ses.ad.website,
				banner: _ses.ad.banner,
				// From step2
				address: _last.address,
				zip: _last.zip
			};
			_ses.adset.push( last );
		}
	}

	$.get("/sads", function( data ) {

		var nads=parseInt(data), found;
		if( nads>0 && !$('.field.buttons').find('.skip').length ) {

			$( "<div class=\"skip\"><span title=\"Skip current ad design\" class=\"skip-button\">Skip</span></div>" ).insertBefore( "button.prev" );
		}

	});

	var next = $('.two');
	$(current).removeClass('current');
	$(next).addClass('current');
});

/**
 * 	Skip
 * 
 * 	Skip current ad validation. 
 * 	Only visible if any ad is stored in $_session
 */
$('.tools').on('click','.skip-button', function(){ 

	$('.pmnt').css('display', 'block');
	$('.pmnt.ppal').slideUp();
	$('.pmnt.ccard').slideUp();

	_ses.fn.showResumeStep();
});

/**
 * Step 2 NEXT
 * 
 * Fires when the next button is clicked during step 2
 */
$('.next.step-two').click(function(){

	_ses.current = 'three';

	var current  = $('.mine.current'),
		next  = $('.'+_ses.current); 

	// The same as skip button
	$('.pmnt').css('display', 'block');
	$('.ccard').hide();
	$('.ppal').hide();

	$(current).removeClass('current');
	$(next).addClass('current');
});


/**
 * Create other add button
 *
 * Saves ad in session + Redirect to ads on correct save
 */
$('button.other').on('click', function() {
	
	_ses.ad.img.height =_ses.ad.obj.find('.top-img').css('height');
	_ses.ad.img.width =_ses.ad.obj.find('.top-img').css('width');
	_ses.ad.img.left =_ses.ad.obj.find('.top-img').css('left');
	_ses.ad.img.top =_ses.ad.obj.find('.top-img').css('top');

	var newObject = jQuery.extend(true, {}, _ses.ad );
	console.dir(newObject);
	delete newObject.obj;
	delete newObject.htmlad;

	newObject.size= _ses.current_class;
	newObject.htmlad= $('.board .pre').html();
	newObject.cssad= JSON.stringify( newObject );
	newObject.title= _ses.ad.title;
	newObject.url= _ses.ad.website;
	newObject.banner= _ses.ad.banner;
	newObject.address= _ses.last_address;
	newObject.zip= _ses.last_code;

	if( !_ses.disableSession )
		$.ajax( {
			type: "POST",
	        url: "/sads",
	        data: { "str" : JSON.stringify( newObject ), "saveInSession" : true },
	        // cache: false,
		    // contentType: false,
		    // processData: false,
	        success: function (response) {
	        	if( response && parseInt(response) > 0 ) {

	        		setTimeout( function(){ window.location = window.location; }, 1200 );
	        	}
	        	console.log('Response');

	    	}
		});
});


$('.ad-resume').on('click', '.rem', function() {

	var index = $(this).parent().index(),
		o=$(this).parent(), 
		key;
	var title = o.find('a span').html();
	//var last = _ses.adset.pop();

	if ( confirm('Delete Ad '+ title + '?' )) {

	    $.post("/sads/", { 'delete': (parseInt(index)+1) }, function( data ) { 
	    	key= o.attr('data-hash');
	    	o.remove();

	    	$('[data-hash='+ key +']').slideUp( 300, function(){ setTimeout( function(){ $(this).remove(); }, 200); } );

		});
	} 
	/*else if(last) {
	    _ses.adset.push(last);
	}*/
});

/**
 * Fires when the scale option is changed
 * 
 * @param  {[type]} ){ 	alert(       $(this).val() );} [description]
 * @return {[type]}     [description]
 */
$('.scale select').change(function(){ 
	//alert( $(this).val() );
	var ratio = $(this).val();

	var img = _ses.ad.obj.find('.top-img');

	img.css( { "height": Math.round(_ses.ad.img.height/ratio, 1), "width": Math.round(_ses.ad.img.width/ratio, 1) } ); 
	
	img.attr('data-ratio', ratio);

	makeImageDraggable(img);

	_ses.ad.img.ratio=ratio;
});

/**
 * 	@version  bike_0.1
 * 	
 *  Fires In case of image selection in the file input field
 *  ads.php
 */
$('input#bg_picture').change(function(){
	
	var data = new FormData();
	$.each( $(this)[0].files, function(i, file) {
	    data.append('file-'+i, file);
	});

	var filewrap=$(this).siblings('.ideal-file-wrap').first();
	
	data.append('token', _token);
	data.append('timestamp', _timestamp);

	$.ajax( {
		type: "POST",
        url: "/ads",
        data: data,
        cache: false,
	    contentType: false,
	    processData: false,
        success: function (response) {
        	if( parseInt(response[0]) == 1 ) {
        		var res=response.substring(1, response.length);
        		filewrap.find('.ideal-file-filename').css('border-color', '#00e009');

        		_ses.ad.obj.css('background-image', "url(/data/"+ res +")");
        		_ses.ad.bg['background-image']="url(/data/"+ res +")";
        		_ses.last_bg_image= "url(data/"+ res +")";
        		_ses.ad.bgimage='/data/'+res;
        		

        		// $('input[name=upimg]').val('/data/'+res); 

        	}
        	else
        		filewrap.find('.ideal-file-filename').css('border-color', '#cc2a18');

    	}
	});
});

// Disable Ad link while is draggig image
// Experimental

$("a.adlnk").on( "click", function(e) {

	if( _ses.drag ) {
		e.preventDefault();
		e.stopPropagation();
	} 
});


// FIX dupplicated logic
// 
$('input#picture').change(function(){
	
	var data = new FormData();
	$.each( $('#picture')[0].files, function(i, file) {
	    data.append('file-'+i, file);
	});
	var filewrap=$(this).siblings('.ideal-file-wrap').first();

	data.append('token', _token);
	data.append('timestamp', _timestamp);

	$.ajax( {
		type: "POST",
        url: "/ads",
        data: data,
        cache: false,
	    contentType: false,
	    processData: false,
        success: function (response) {
        	if( parseInt(response[0]) == 1 ) {
        		var res=response.substring(1, response.length);

        		filewrap.find('.ideal-file-filename').css('border-color', '#00e009');

        		if( !_ses.ad.obj.find('img').length )
        			_ses.ad.obj.append('<img class="top-img" src="'+ '/data/'+ res + '" />');
        		else {
        			_ses.ad.obj.find('img').remove();
        			_ses.ad.obj.append('<img class="top-img" src="'+ '/data/'+ res + '" />');
        		}
        		_ses.ad.img.src='/data/'+ res;
        		_ses.last_image='/data/'+ res;
        		
        		_ses.ad.banner='/data/'+res;
        		_ses.ad.obj.find('.top-img').on('load', function(e){ 

        			// alert('loaded image');

        			console.dir(e);
        			var wi = e.currentTarget.naturalWidth,
        				he = e.currentTarget.naturalHeight,
        				ratio=$('.scale option').val(),
        				imageO=e.currentTarget;

        			_ses.ad.img.height=he;
        			_ses.ad.img.width=wi;
        			_ses.ad.img.ratio=ratio;
        			
        			$(imageO).attr('data-ratio', ratio);
        			$(imageO).attr('data-frame-x', _ses.ad.frame_width);
        			$(imageO).attr('data-frame-y', _ses.ad.frame_height);

        			$( imageO ).css( { "height": Math.round(wi/ratio, 1), "width": Math.round(he/ratio, 1) } ); 

        			makeImageDraggable( imageO );
        			/*
        			$( imageO ).draggable({
        				//revert: true,
        				start: function( event, ui ) {
        					_ses.drag=true;
        				},
        				drag: function( event, ui ) {
        					console.log("offset" + ui.offset.left + "x" + ui.offset.top );
        					console.log("position" + ui.position.left + "x" + ui.position.top );
        					// var h = Math.floor( event.currentTarget.naturalHeight/$('.scale option').val() );

        					// console.log(Math.floor(ui.offset.top)-h);
        				
        				},
        				stop: function( event, ui ) {
        					console.log("final offset" + ui.offset.left + "x" + ui.offset.top );
        					console.log("final position" + ui.position.left + "x" + ui.position.top );
        					
        					$(imageO).attr('data-pos-x', ui.position.left);
        					$(imageO).attr('data-pos-y', ui.position.top);

        					setTimeout(function(){

        						_ses.drag=false;

        					}, 3000 );

        				}

        			});
					*/
        		});

        		//_ses.ad.obj.find('.top-img').first().draggable();

        		//_ses.ad.obj.css('background-image', "url(/data/"+ res +")");
        		//_ses.ad.bg['background-image']="url(/data/"+ res +")";
        		//_ses.last_image="url(data/"+ res +")";
        		//_ses.ad.banner='/data/'+res;

        		// $('input[name=upimg]').val('/data/'+res); 
        		// alert("Top Image is still not set in <html>");
        	}
        	else
        		filewrap.find('.ideal-file-filename').css('border-color', '#cc2a18');

    	}
	});
});


function makeImageDraggable(img) {
	$( img ).draggable({
		//revert: true,
		start: function( event, ui ) {
			_ses.drag=true;
		},
		drag: function( event, ui ) {
			console.log("offset" + ui.offset.left + "x" + ui.offset.top );
			console.log("position" + ui.position.left + "x" + ui.position.top );
			// var h = Math.floor( event.currentTarget.naturalHeight/$('.scale option').val() );

			// console.log(Math.floor(ui.offset.top)-h);

			/*if( ui.position.left < 0 || Math.floor(ui.offset.top)-h < 0 ) {
				event.stopPropagation();
				return false;
			}*/
		},
		stop: function( event, ui ) {
			console.log("final offset" + ui.offset.left + "x" + ui.offset.top );
			console.log("final position" + ui.position.left + "x" + ui.position.top );
			
			$(img).attr('data-pos-x', ui.position.left);
			$(img).attr('data-pos-y', ui.position.top);

			setTimeout( function(){

				_ses.drag=false;

			}, 2000 );
		}

   });
}

/**
 * 	@version  bike_0.1
 * 	
 *  Fires In case of click in color elements
 *  ads.php
 */
$('.txt input[type=text]').change(function(){
	var val = $(this).val(),
		obj,
		id 	= $(this).attr('id');

	if( id.indexOf('sh') !== -1 ) {
		obj = { 'text-shadow' : val+' 2px 2px 6px' };

		//_ses.ad.obj.css(opts); 
		_ses.ad.shadow=obj;

		$('.add-str.shadow h2').css(obj);
		$('.add-pre.shadow h2').css(obj);

	} else if( id.indexOf('font') !== -1 ) {
		obj = { 'color': val };
		$('.ad-box h2').css(obj);

		_ses.ad.font['color']=obj['color'];
	} else if( id.indexOf('bg') !== -1 ) {
		obj = { 'background-color': val };

		_ses.ad.bg['background-color']=obj['background-color'];
		_ses.ad.obj.css(obj);
	}

	console.dir(_ses.ad);
});

/*
$(".sb.img:checkbox").on('click', function(){
	alert( $(this).val() );
});*/

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/*
$('form.idealforms').submit(function() { 
	$('form.idealforms').append()
	$.ajax({ //FormID - id of the form.
        type: "POST",
        url: "/ads",
        data: $('form.idealforms').serialize(),
        cache: false,
        success: function (response) {


    	}
    });

});
*/





