/**
 * bikes.js
 * 
 * @author  Karlos Álvarez <karlos.alvan@gmail.com>
 * @version bike 1.3.2
 *
 */

// var bounce = new Bounce();

var index = null,
    header = null;

if (typeof jQuery == 'undefined') {
	console.error("jQuery is not defined!");
	exit(1);
}

$.get('/js/templates/index.mst', function(t) {
    index = t;
});

$('#content').append('<ul id="container"></ul>');

/*
$.get('/js/templates/ads.mst', function(template) {

    var map1 = '<iframe id="ifr" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d95777.5288157847!2d2.0787279470992464!3d41.39476881460054!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a49816718e30e5%3A0x44b0fb3d4f47660a!2sBarcelona!5e0!3m2!1sen!2ses!4v1459820708683" width="290" height="192" frameborder="0" style="border:0" allowfullscreen></iframe>',
        map2 = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23945.244378483247!2d2.149972454048684!3d41.39242823483153!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a28d0ae57f29%3A0x72cd495f27c3cc51!2sBarcelona+City+Centre!5e0!3m2!1sen!2ses!4v1459821509519" width="290" height="192" frameborder="0" style="border:0" allowfullscreen></iframe>',
        map3 = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.6974309594516!2d2.171654051125883!3d41.380658004273144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a258081714c9%3A0x24bef7ac8842bdc4!2sLes+Rambles%2C+08002+Barcelona!5e0!3m2!1sen!2ses!4v1459821680461" width="432" height="192" frameborder="0" style="border:0" allowfullscreen></iframe>';


    var rendered = Mustache.render( template, 
        {   "elem" :  [ 
                { "img":0, "tag":"sic", "sizer": "elem9", "class": "elem1" }, 
                { "img":0, "tag":"xie", "sizer": "elem8", 'class': 'elem2','add':'add', 'txt': { t: 'rent a cool bike', 'c':'add-str', 'p':'right' } },  
                { "img":0, "tag":"sic", "sizer": "elem7", 'class': 'elem', 'add':'add', 'txt': { t: 'place your add here', 'c':'add-str' } }, 
                { "img":0, "tag":"sic", "sizer": "elem8", 'class': 'elem8', 
                    'txt': { 't': '{your_name}.bikerent.barcelona <br><br><b>NOW</b>', 'c':'add-not' },
                    'inf': { 'txt1': 'More info' }
                }, 
                { "img":0,      "tag":"sic ric",        "sizer": "elem12", 'class': 'elem5c', 'add':'add', 'txt': { 't':'great</h2><h2>electric bikes</h2><h2>for a great day', 'c':'add-str shadow', 'p':'right' } },
                { "img":"pie",  "tag":"sic",        "sizer": "elem7", 'class': 'elem0' }, 
                { "img":0,      "tag":"sic",        "sizer": "elem8", 'class': 'elem5b', 'add':'add', 'txt': { t: 'A classic<br>&nbsp;&nbsp;bike<br>&nbsp;day?', 'c':'add-pre' } }, 
                { "img":0,      "tag":"pie sic",    "sizer": "elem9", 'class': 'elem9',  'txt': { 't': 'Your little add<br>Almost Free<br>Here', 'c':'add-tit sm' }  },
                { "img":0,      "tag":"pie sic",    "sizer": "elem9", 'class': 'elem9b', 'txt': { 't': 'Your little add<br>Almost Free<br>Here', 'c':'add-tit sm' }  },
                { "img":0,      "tag":"sic pie ric xie",     "sizer": "elem8", 'class': 'elem4', 'title':1, 'inf': { 'txt1': 'Yes, I\'m interested', 'txt2': 'Let\'s see more' } },
                { "img":0,      "tag":"xie",        "sizer": "elem8", 'class': 'elem2', 'txt': { 't':'You can rent a bike here', 'c':'add-str' } }, 
                { "img":0,      "tag":"pie ric",    "sizer": "elem8", 'class': 'elem7', 'map': map3 },


                { "img":2, "tag":"pie", "sizer": "elem10", 'class': 'elem11'  },
                 
                { "img":1, "tag":"sic", "sizer": "elem7", 'class': 'elem'  },
                { "img":0, "tag":"ric", "sizer": "elem8", 'class': 'elem3', 'txt': { t: 'A&nbsp;bike&nbsp;for<br>&nbsp;&nbsp;a&nbsp;race.', 'c':'add-str black tl' } }, 
                { "img":1, "tag":"ric", "sizer": "elem8", 'class': 'elem6', 
                  'txt': { 't': 'Barcelona is awesome by bike!', 'c':'add-tit' },
                  'inf': { 'txt1': 'Classic bike tours?', 'txt2': 'I prefer to go walking..' }

                },

                { "img":0, "tag":"sic", "sizer": "elem7", 'class': 'elem0' },
                { "img":1, "tag":"sic", "sizer": "elem7", 'class': 'elem'  }, 
                { "img":0, "tag":"sic", "sizer": "elem8", 'class': 'elem5', 'add':'add' }, 
                { "img":0,      "tag":"pie",        "sizer": "elem9", 'class': 'elem9b',  'txt': { 't': 'Your little add<br>Almost Free<br>Here', 'c':'add-tit sm' }  },
                { "img":0,      "tag":"pie",        "sizer": "elem9", 'class': 'elem9', 'txt': { 't': 'Your little add<br>Almost Free<br>Here', 'c':'add-tit sm' }  },
                
                { "img":9, "tag":"pie sic", "sizer": "elem9", 'class': 'elem9b', 'txt': { 't': 'Your little add<br>Almost Free<br>Here', 'c':'add-tit sm' }  },
                { "img":0, "tag":"pie sic", "sizer": "elem9", 'class': 'elem9',  'txt': { 't': 'Your little add<br>Almost Free<br>Here', 'c':'add-tit sm' }  },
                { "img":1, "tag":"sic xie", "sizer": "elem7", 'class': 'elem7', 'map': map1 },
                { "img":3, "tag":"sic", "sizer": "elem9", 'class': 'elem1' }
            ] 
        }  
    );
    
   
    // $('#content ul').html(rendered);
    // $('#content').append('<ul id="container"></ul>');
    setTimeout(function(){
        $('iframe').each(function() {
            this.contentWindow.location.reload(true);
        });
        _onload();
    }, 500);
});
*/

$(window).on('load', function() { 
    setTimeout( function(){
        console.log('isotope');

        /** 
         * Is computed as the gratest common divider of the displayed ad withds
         * @type integer
         */
        var colWidth = (typeof _cw !== typeof undefined) ? _cw : $('div#grid div').width();

        // First the columns are set       
        $('.grid').isotope({
            itemSelector: '.grid-item',
            masonry: {
            columnWidth: colWidth
        } });
       
        // Then the colums are aligned
        $('.grid').isotope({
            itemSelector: '.grid-item',
            layoutMode: "perfectMasonry",
            perfectMasonry: {
                liquid: true
            }
        });
    }, 100);
});

