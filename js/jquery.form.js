jQuery(document).ready(function(){

	var done = false, now, action,
		start = 0;

	$('#contactform').submit(function(){

		if(!done) {
			$('#name').css({ "color":"#C10F0F"});
			return done;
		}

		now=Date.now();

		//haha
		if( now < start + (5 * 1000) ) {
			console.error("Too fast login. Less than 5 seconds.");
			return false;
		}

		action = $(this).attr('action');

		// $("#message").slideUp(250,function() {
		$('#message').hide();

		// $('#submit').attr('disabled','disabled');

		$.post( action, {
				name: $('#name').val(),
				email: $('#email').val(),
				comment: $('#comment').val()
			},
			function(data){
				try {
					var obj = JSON.parse(data), word;
				} catch(e) { //console.error("Error in Json"); 

				}		
				if(obj.status !== undefined && obj.status == 'sent') {
					alert("Sent!");
					$('#contact').append('<div class="overlay"></div>');

					$('.fback').html( "You will receive news soon.<br><b>Thank you!</b>" );

				} else {
					try {
						var obj = JSON.parse(data), word;
						if( obj ) {
							for( var i in Object.keys( obj ) ) {

								word = Object.keys( obj )[i];
								//alert( obj[word] );
								$('#'+word).val( obj[word] );
								$('#'+word).css({ "color":"#C10F0F"});

							}
							//$('.fback').html( Object.keys( obj ) );
						}
						else { }
					} catch(e) { //console.error("Error in Json"); 

					}
				}

		}).fail(function() { });
		return false;
	});


	$('#name, #mail').on('focus', function(){ 
		if( !done ) {
			start = Date.now();
			done=true;
		}
	});

});