var sizes = ['large','medium','regular','small','tiny'],
    type='large';

$(window).on('load', function(){
    
    var obj,color;
    $('table.plans tr td').each( function(i, e){ 
        
        console.log(i + $(e).html() );
        if(e !== undefined ) {
            if( $(e).html() == "Yes" ) {
                obj={ "color": "#00E009" };
                color="#00E009";
            }
            else {
                obj={ "color": "#888" };
                color="#888";
            }
            //console.log(obj);
            //$(e).attr( 'bgcolor', color ) ;
            $(e).css( 'color', color ) ;
            //$(e).css( obj ) ;
        }
    });

})


$('.opt-box[data-op=' + type +']').css('display','inline-block');

//var rendered = Mustache.render( header, {} );
$('.plans tr td').on('click', function(){ 
    var p = $(this).parent('tr');

    var sz = $(p).attr('data-sz');
    if( sizes.indexOf(sz) == -1 )
    	return;

    if( $(p).hasClass('selected') ) {
        $(p).removeClass('selected');
        $('.e.'+sz).removeClass('selected').html("");
        setTimeout(function(){
        	
        	$('button').removeClass('selected');

        }, 500);
        
        //$('.opt-box[data-op=' + type +']').css('display','none');
    }
    else {
    	$('.e').removeClass('selected').html("");
    	$(p).siblings().removeClass('selected');
        $(p).addClass('selected');
        $('.e.'+sz).addClass('selected').html(sz);
        setTimeout(function(){
        	
        	$('button').addClass('selected');

        }, 500);
        
        $('.opt-box').css({ 'display':'none'});
        $('.opt-box[data-op=' + sz +']').css('display','inline-block');
    }
});